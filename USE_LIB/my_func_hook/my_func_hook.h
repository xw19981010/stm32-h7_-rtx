#ifndef MY_FUNC_HOOK_H__
#define MY_FUNC_HOOK_H__

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void(*My_Func)(void* owner,void* use_data);

typedef struct My_Func_Hook{
	My_Func func;
	void* data;
	struct My_Func_Hook* next;
}My_Func_Hook;

typedef My_Func_Hook* My_Func_Root;

void my_func_hook_run_all(My_Func_Root root,void* owner);
bool my_func_hook_add(My_Func_Root* proot,My_Func func,void* data);
void my_func_hook_clear_all(My_Func_Root* proot);
bool my_func_hook_remove(My_Func_Root* proot,My_Func func,void* data);

#ifdef __cplusplus
}
#endif


#endif
