#include "my_func_hook.h"


void my_func_hook_run_all(My_Func_Root root,void* owner){
	for(My_Func_Hook* phook=root;phook!=NULL;phook=phook->next){
		phook->func(owner,phook->data);
	}
}

bool my_func_hook_add(My_Func_Root* proot,My_Func func,void* data){
	My_Func_Hook* new_hook=malloc(sizeof(My_Func_Hook));
	if(new_hook==NULL){
		return false;
	}
	new_hook->data=data;
	new_hook->func=func;
	new_hook->next=*proot;
	*proot=new_hook;
	return true;
}

void my_func_hook_clear_all(My_Func_Root* proot){
	while(*proot!=NULL){
		My_Func_Hook* temp=(*proot)->next;
		free(*proot);
		*proot=temp;
	}
}

bool my_func_hook_remove(My_Func_Root* proot,My_Func func,void* data){
	for(My_Func_Hook* phook=*proot,*priv_hook=NULL;phook!=NULL;priv_hook=phook,phook=phook->next){
		if(phook->func==func&&phook->data==data){
			if(priv_hook!=NULL){
				priv_hook->next=phook->next;
			}
			else{
				*proot=phook->next;
			}
			free(phook);
			return true;
		}
	}
	return false;
}

