/*
 * FLAC (Free Lossless Audio Codec) decoder
 * Copyright (c) 2003 Alex Beregszaszi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * @file flac.c
 * FLAC (Free Lossless Audio Codec) decoder
 * @author Alex Beregszaszi
 *
 * For more information on the FLAC format, visit:
 *  http://flac.sourceforge.net/
 *
 * This decoder can be used in 1 of 2 ways: Either raw FLAC data can be fed
 * through, starting from the initial 'fLaC' signature; or by passing the
 * 34-byte streaminfo structure through avctx->extradata[_size] followed
 * by data starting with the 0xFFF8 marker.
 */

#include <inttypes.h>
#include <stdbool.h>
#include <limits.h>
#include "ff.h"
#include "music_device.h"
//#include "arm.h"
#include "golomb.h"
#include "bitstreamf.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef int32_t	s32;
typedef int16_t s16;
typedef int8_t  s8;

#define MAX_BUFFER_OUT		(8*1024)

enum decorrelation_type {
    INDEPENDENT,
    LEFT_SIDE,
    RIGHT_SIDE,
    MID_SIDE,
};


typedef struct FLACContext 
{
	GetBitContext gb;

	int min_blocksize, max_blocksize;	//block的最小/最大采样数
	int min_framesize, max_framesize;	//最小/最大帧大小
	int samplerate, channels;			//采样率和通道数
	int blocksize;  					// last_blocksize
	int bps, curr_bps;
	unsigned long samplenumber;
	unsigned long totalsamples;
	enum decorrelation_type decorrelation;  

	int seektable;
	int seekpoints;

	int bitstream_size;
	int bitstream_index;

	int sample_skip;
	int framesize;

	int *decoded0;  // channel 0
	int *decoded1;  // channel 1
}FLACContext;


#define FFMAX(a,b) ((a) > (b) ? (a) : (b))
#define FFMIN(a,b) ((a) > (b) ? (b) : (a))

static const int sample_rate_table[] ICONST_ATTR =
{ 0, 88200, 176400, 192000,
  8000, 16000, 22050, 24000, 32000, 44100, 48000, 96000,
  0, 0, 0, 0 };

static const int sample_size_table[] ICONST_ATTR = 
{ 0, 8, 12, 0, 16, 20, 24, 0 };

static const int blocksize_table[] ICONST_ATTR = {
     0,    192, 576<<0, 576<<1, 576<<2, 576<<3,      0,      0, 
256<<0, 256<<1, 256<<2, 256<<3, 256<<4, 256<<5, 256<<6, 256<<7 
};

static const uint8_t table_crc8[256] ICONST_ATTR = {
    0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15,
    0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d,
    0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
    0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d,
    0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5,
    0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
    0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85,
    0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd,
    0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
    0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea,
    0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2,
    0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
    0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32,
    0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a,
    0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
    0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a,
    0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c,
    0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
    0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec,
    0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4,
    0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
    0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44,
    0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c,
    0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
    0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b,
    0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63,
    0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
    0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13,
    0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb,
    0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83,
    0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb,
    0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3
};

static int64_t get_utf8(GetBitContext *gb) ICODE_ATTR_FLAC;
static int64_t get_utf8(GetBitContext *gb)
{
    uint64_t val;
    int ones=0, bytes;
    
    while(get_bits1(gb))
        ones++;

    if     (ones==0) bytes=0;
    else if(ones==1) return -1;
    else             bytes= ones - 1;
    
    val= get_bits(gb, 7-ones);
    while(bytes--){
        const int tmp = get_bits(gb, 8);
        
        if((tmp>>6) != 2)
            return -2;
        val<<=6;
        val|= tmp&0x3F;
    }
    return val;
}

static int get_crc8(const uint8_t *buf, int count) ICODE_ATTR_FLAC;
static int get_crc8(const uint8_t *buf, int count)
{
    int crc=0;
    int i;
    
    for(i=0; i<count; i++){
        crc = table_crc8[crc ^ buf[i]];
    }

    return crc;
}

static int decode_residuals(FLACContext *s, int32_t* decoded, int pred_order) ICODE_ATTR_FLAC;
static int decode_residuals(FLACContext *s, int32_t* decoded, int pred_order)
{
    int i, tmp, partition, method_type, rice_order;
    int sample = 0, samples;

    method_type = get_bits(&s->gb, 2);
    if (method_type > 1){
        //fprintf(stderr,"illegal residual coding method %d\n", method_type);
        return -3;
    }
    
    rice_order = get_bits(&s->gb, 4);

    samples= s->blocksize >> rice_order;

    sample= 
    i= pred_order;
    for (partition = 0; partition < (1 << rice_order); partition++)
    {
        tmp = get_bits(&s->gb, method_type == 0 ? 4 : 5);
        if (tmp == (method_type == 0 ? 15 : 31))
        {
            //fprintf(stderr,"fixed len partition\n");
            tmp = get_bits(&s->gb, 5);
            for (; i < samples; i++, sample++)
                decoded[sample] = get_sbits(&s->gb, tmp);
        }
        else
        {
            for (; i < samples; i++, sample++){
                decoded[sample] = get_sr_golomb_flac(&s->gb, tmp, INT_MAX, 0);
            }
        }
        i= 0;
    }

    return 0;
}    

static __inline int32_t get_decode_data(int32_t* decoded,int pred_order,int offset){
	return pred_order<offset?0:decoded[pred_order-offset];
}

static int decode_subframe_fixed(FLACContext *s, int32_t* decoded, int pred_order) ICODE_ATTR_FLAC;
static int decode_subframe_fixed(FLACContext *s, int32_t* decoded, int pred_order)
{
    const int blocksize = s->blocksize;
    int a, b, c, d, i;
        
    /* warm up samples */
    for (i = 0; i < pred_order; i++)
    {
        decoded[i] = get_sbits(&s->gb, s->curr_bps);
    }
    
    if (decode_residuals(s, decoded, pred_order) < 0)
        return -4;

    a = get_decode_data(decoded,pred_order,1);
	b = a - get_decode_data(decoded,pred_order,2);
    c = b - get_decode_data(decoded,pred_order,2) + get_decode_data(decoded,pred_order,3);
    d = c - get_decode_data(decoded,pred_order,2) + 2*get_decode_data(decoded,pred_order,3) - get_decode_data(decoded,pred_order,4);

    switch(pred_order)
    {
        case 0:
            break;
        case 1:
            for (i = pred_order; i < blocksize; i++)
                decoded[i] = a += decoded[i];
            break;
        case 2:
            for (i = pred_order; i < blocksize; i++)
                decoded[i] = a += b += decoded[i];
            break;
        case 3:
            for (i = pred_order; i < blocksize; i++)
                decoded[i] = a += b += c += decoded[i];
            break;
        case 4:
            for (i = pred_order; i < blocksize; i++)
                decoded[i] = a += b += c += d += decoded[i];
            break;
        default:
            return -5;
    }

    return 0;
}


/* level8 用到这个函数最多
 * 之前版本经过验证是不行的
 * 这个函数有arm汇编版,但移植不成功
 * 找了个C语言版本的,效率不高
 */
static int decode_subframe_lpc(FLACContext *s, int32_t* decoded, int pred_order)
{
     int i, j;
     int coeff_prec, qlevel;
     int coeffs[32];
 
     /* warm up samples */
     for (i = 0; i < pred_order; i++) {
         decoded[i] = get_sbits(&s->gb, s->curr_bps);
     }
 
     coeff_prec = get_bits(&s->gb, 4) + 1;
     if (coeff_prec == 16) {
//         av_log(s->avctx, AV_LOG_ERROR, "invalid coeff precision\n");
         return -1;
     }
     qlevel = get_sbits(&s->gb, 5);
     if (qlevel < 0) {
//         av_log(s->avctx, AV_LOG_ERROR, "qlevel %d not supported, maybe buggy stream\n",
//                qlevel);
         return -1;
     }
 
     for (i = 0; i < pred_order; i++) {
        coeffs[i] = get_sbits(&s->gb, coeff_prec);
     }
 
     if (decode_residuals(s, decoded, pred_order) < 0)
         return -1;
 
     if (s->bps > 16) {
         int64_t sum;
         for (i = pred_order; i < s->blocksize; i++) {
             sum = 0;
             for (j = 0; j < pred_order; j++)
                 sum += (int64_t)coeffs[j] * decoded[i-j-1];
             decoded[i] += sum >> qlevel;
         }
     } else {
         for (i = pred_order; i < s->blocksize-1; i += 2) {
             int c;
             int d = decoded[i-pred_order];
             int s0 = 0, s1 = 0;
             for (j = pred_order-1; j > 0; j--) {
                 c = coeffs[j];
                 s0 += c*d;
                 d = decoded[i-j];
                 s1 += c*d;
             }
             c = coeffs[0];
             s0 += c*d;
             d = decoded[i] += s0 >> qlevel;
             s1 += c*d;
             decoded[i+1] += s1 >> qlevel;
         }
         if (i < s->blocksize) {
             int sum = 0;
             for (j = 0; j < pred_order; j++)
                 sum += coeffs[j] * decoded[i-j-1];
             decoded[i] += sum >> qlevel;
         }
     }
 
     return 0;

}

static __inline int decode_subframe(FLACContext *s, int channel, int32_t* decoded)
{
    int type, wasted = 0;
    int i, tmp;
    
    s->curr_bps = s->bps;
    if(channel == 0){
        if(s->decorrelation == RIGHT_SIDE)
            s->curr_bps++;
    }else{
        if(s->decorrelation == LEFT_SIDE || s->decorrelation == MID_SIDE)
            s->curr_bps++;
    }

    if (get_bits1(&s->gb))
    {
        //fprintf(stderr,"invalid subframe padding\n");
        return -9;
    }
    type = get_bits(&s->gb, 6);
//    wasted = get_bits1(&s->gb);
    
//    if (wasted)
//    {
//        while (!get_bits1(&s->gb))
//            wasted++;
//        if (wasted)
//            wasted++;
//        s->curr_bps -= wasted;
//    }
#if 0
    wasted= 16 - av_log2(show_bits(&s->gb, 17));
    skip_bits(&s->gb, wasted+1);
    s->curr_bps -= wasted;
#else
    if (get_bits1(&s->gb))
    {
        wasted = 1;
        while (!get_bits1(&s->gb))
            wasted++;
        s->curr_bps -= wasted;
        //fprintf(stderr,"%d wasted bits\n", wasted);
    }
#endif
//FIXME use av_log2 for types
    if (type == 0)
    {
        //fprintf(stderr,"coding type: constant\n");
        tmp = get_sbits(&s->gb, s->curr_bps);
        for (i = 0; i < s->blocksize; i++)
            decoded[i] = tmp;
    }
    else if (type == 1)
    {
        //fprintf(stderr,"coding type: verbatim\n");
        for (i = 0; i < s->blocksize; i++)
            decoded[i] = get_sbits(&s->gb, s->curr_bps);
    }
    else if ((type >= 8) && (type <= 12))
    {
        //fprintf(stderr,"coding type: fixed\n");
        if (decode_subframe_fixed(s, decoded, type & ~0x8) < 0)
            return -10;
    }
    else if (type >= 32)
    {
        //fprintf(stderr,"coding type: lpc\n");
        if (decode_subframe_lpc(s, decoded, (type & ~0x20)+1) < 0)
            return -11;
    }
    else
    {
        //fprintf(stderr,"Unknown coding type: %d\n",type);
        return -12;
    }
        
    if (wasted)
    {
        int i;
        for (i = 0; i < s->blocksize; i++)
            decoded[i] <<= wasted;
    }

    return 0;
}

static int decode_frame(FLACContext *s) ICODE_ATTR_FLAC;
static int decode_frame(FLACContext *s){
	int blocksize_code, sample_rate_code, sample_size_code, assignment, crc8;
	int decorrelation, bps, blocksize, samplerate;
	int res;
    
    blocksize_code = get_bits(&s->gb, 4);

    sample_rate_code = get_bits(&s->gb, 4);
    
    assignment = get_bits(&s->gb, 4); /* channel assignment */
    if (assignment < 8 && s->channels == assignment+1)
        decorrelation = INDEPENDENT;
    else if (assignment >=8 && assignment < 11 && s->channels == 2)
        decorrelation = LEFT_SIDE + assignment - 8;
    else
    {
        return -13;
    }
        
    sample_size_code = get_bits(&s->gb, 3);
    if(sample_size_code == 0)
        bps= s->bps;
    else if((sample_size_code != 3) && (sample_size_code != 7))
        bps = sample_size_table[sample_size_code];
    else 
    {
        return -14;
    }

    if (get_bits1(&s->gb))
    {
        return -15;
    }

    /* Get the samplenumber of the first sample in this block */
    s->samplenumber=get_utf8(&s->gb);

    /* samplenumber actually contains the frame number for streams
       with a constant block size - so we multiply by blocksize to
       get the actual sample number */
    if (s->min_blocksize == s->max_blocksize) {
        s->samplenumber*=s->min_blocksize;
    }

#if 0    
    if (/*((blocksize_code == 6) || (blocksize_code == 7)) &&*/
        (s->min_blocksize != s->max_blocksize)){
    }else{
    }
#endif

    if (blocksize_code == 0)
        blocksize = s->min_blocksize;
    else if (blocksize_code == 6)
        blocksize = get_bits(&s->gb, 8)+1;
    else if (blocksize_code == 7)
        blocksize = get_bits(&s->gb, 16)+1;
    else 
        blocksize = blocksize_table[blocksize_code];

    if(blocksize > s->max_blocksize){
        return -16;
    }

    if (sample_rate_code == 0){
        samplerate= s->samplerate;
    }else if ((sample_rate_code > 0) && (sample_rate_code < 12))
        samplerate = sample_rate_table[sample_rate_code];
    else if (sample_rate_code == 12)
        samplerate = get_bits(&s->gb, 8) * 1000;
    else if (sample_rate_code == 13)
        samplerate = get_bits(&s->gb, 16);
    else if (sample_rate_code == 14)
        samplerate = get_bits(&s->gb, 16) * 10;
    else{
        return -17;
    }

    skip_bits(&s->gb, 8);
    crc8= get_crc8(s->gb.buffer, get_bits_count(&s->gb)/8);
    if(crc8){
        return -18;
    }
    
    s->blocksize    = blocksize;
    s->samplerate   = samplerate;
    s->bps          = bps;
    s->decorrelation= (enum decorrelation_type)decorrelation;

    /* subframes */
    if ((res=decode_subframe(s, 0, s->decoded0)) < 0){
    	return res-100;
    }


    if (s->channels==2) {
      if ((res=decode_subframe(s, 1, s->decoded1)) < 0){
    	  return res-200;
      }
    }
    
    align_get_bits(&s->gb);

    /* frame footer */
    skip_bits(&s->gb, 16); /* data crc */

    return 0;
} 
//查找下一帧起始地址
//buf:输入数组
//size:数组大小
//fc:flac解码容器
//返回值:-1,没有找到帧标志
//     其他,帧起始偏移量
int flac_seek_frame(uint8_t *buf,uint32_t size,FLACContext * fc)
{
	uint8_t *p;
	uint32_t i;
	uint32_t samplerate;
	uint32_t bps; 
	uint8_t seekok=0;
	p=buf;
	for(i=0;i<size-3;i++,p++)
	{ 
		if(p[0]==0XFF&&((p[1]&0XFC)==0XF8))//找到帧同步字(0XFFF8)
		{
			samplerate=p[2]&0X0F;				//采样率
			if(samplerate==0)samplerate=fc->samplerate;
			else if(samplerate>11)continue;
			else samplerate=sample_rate_table[samplerate]; 
			bps=sample_size_table[(p[3]&0X0F)>>1];//采样深度 16/24位
			if(samplerate==fc->samplerate&&bps==fc->bps)
			{
				seekok=1;
				break;
			}
		}
	}  
	if(seekok)return i;	//帧在数组里面的起始地址偏移量
	else return -1;		//没有找到
}

static int flac_decode_frame(FLACContext *fc, uint8_t *buf, int buf_size){
	int sampleCnt;
	init_get_bits(&fc->gb, buf, buf_size*8);
	skip_bits(&fc->gb, 16); 
	if((sampleCnt=decode_frame(fc))<0)
	{
		fc->bitstream_size=0;
		fc->bitstream_index=0;
		return sampleCnt;
	} 
	fc->framesize = (get_bits_count(&fc->gb)+7)>>3; 
	sampleCnt = fc->blocksize;
	return sampleCnt;
}

static void flac_decorrelation16(FLACContext *fc,int* ch0,int* ch1,int sampleCnt,s16* wavbuf){
	switch(fc->decorrelation){
	case INDEPENDENT :
		if (fc->channels==1) {
			do{
				*(wavbuf ++) = *ch0;
				*(wavbuf ++) = *(ch0 ++);
			}while(-- sampleCnt);
		}
		else{
			do{
				*(wavbuf ++) = *(ch0 ++);
				*(wavbuf ++) = *(ch1 ++);
			}while(-- sampleCnt);
		}
		break;
	case LEFT_SIDE: 
		do{
			*(wavbuf ++) = *ch0;
			*(wavbuf ++) = *(ch0 ++) - *(ch1 ++);
		}while(-- sampleCnt);
		break;
	case RIGHT_SIDE: 
		do{
			*(wavbuf ++) = *(ch0 ++) + *ch1;
			*(wavbuf ++) = *(ch1 ++);
		}while(-- sampleCnt);
		break;
	case MID_SIDE: 
		do{
			int mid, side; 
			mid  = *(ch0 ++);
			side = *(ch1 ++);
			mid -= side>>1;   
			*(wavbuf ++) = (mid + side);
			*(wavbuf ++) = mid;
		}while(-- sampleCnt);
		break;
	default :
		do{
			*(wavbuf ++) = 0;
			*(wavbuf ++) = 0;
		}while(-- sampleCnt);
		break;
	} 
}

static void flac_decorrelation24(FLACContext *fc,int* ch0,int* ch1,int sampleCnt,s32* wavbuf){
	switch(fc->decorrelation){
	case INDEPENDENT :
		if (fc->channels==1) {
			do{
				*(wavbuf ++) = *ch0;
				*(wavbuf ++) = *(ch0 ++);
			}while(-- sampleCnt);
		}
		else{
			do{
				*(wavbuf ++) = *(ch0 ++);
				*(wavbuf ++) = *(ch1 ++);
			}while(-- sampleCnt);
		}
		break;
	case LEFT_SIDE: 
		do{
			*(wavbuf ++) = *ch0;
			*(wavbuf ++) = *(ch0 ++) - *(ch1 ++);
		}while(-- sampleCnt);
		break;
	case RIGHT_SIDE: 
		do{
			*(wavbuf ++) = *(ch0 ++) + *ch1;
			*(wavbuf ++) = *(ch1 ++);
		}while(-- sampleCnt);
		break;
	case MID_SIDE: 
		do{
			int mid, side; 
			mid  = *(ch0 ++);
			side = *(ch1 ++);
			mid -= side>>1;   
			*(wavbuf ++) = (mid + side);
			*(wavbuf ++) = mid;
		}while(-- sampleCnt);
		break;
	default :
		do{
			*(wavbuf ++) = 0;
			*(wavbuf ++) = 0;
		}while(-- sampleCnt);
		break;
	} 
}


//FLAC控制结构体
typedef struct 
{ 
    uint32_t totsec ;				//整首歌时长,单位:秒
    uint32_t cursec ;				//当前播放时长
	
    uint32_t bitrate;	   			//比特率
	uint32_t samplerate;				//采样率
	uint16_t outsamples;				//PCM输出数据量大小
	uint16_t bps;					//位数,比如16bit,24bit,32bit
	
	uint32_t datastart;				//数据帧开始的位置(在文件里面的偏移)
}__flacctrl;

//flaC 标签 
typedef __PACKED_STRUCT
{
    uint8_t id[3];		   	//ID,在文件起始位置,必须是flaC 4个字母 
}FLAC_Tag;

//metadata 数据块头信息结构体 
typedef __PACKED_STRUCT
{
    uint8_t head;		   	//metadata block头
	uint8_t size[3];			//metadata block数据长度	
}MD_Block_Head;

typedef struct{
	FLACContext fc;
	__flacctrl ctrl;
	FIL file;
	uint8_t* inter_buffer;
	uint32_t read_byte;
	uint32_t used_byte;
	
	int used_sample;
	int total_sample;
}Flac_Handle;



//分析FLAC文件
//fx:flac文件指针
//fc:flac解码容器
//返回值:0,分析成功
//    其他,错误代码
static bool flac_init(FIL* fx,__flacctrl* fctrl,FLACContext* fc){
	FLAC_Tag * flactag;
	MD_Block_Head *flacblkh;
	uint8_t *buf; 
	uint8_t endofmetadata=0;			//最后一个metadata标记
	int blocklength; 
	uint32_t br;
	FRESULT res;
	
	buf=malloc(512);	//申请512字节内存
	if(buf==NULL)
		goto fail0;			//内存申请失败 
	res=f_lseek(fx,0);
	if(res!=FR_OK){
		goto fail1;
	}
	res=f_read(fx,buf,4,&br);		//读取4字节 
	if(res!=FR_OK){
		goto fail1;
	}
	
	flactag=(FLAC_Tag*)buf;		//强制转换为flac tag标签
	if(strncmp("fLaC",(char*)flactag->id,4)!=0) {
		printf("文件类型错误\r\n");
		goto fail1;
    } 
    while(!endofmetadata) 
	{
		res=f_read(fx,buf,4,&br);
        if( br<4 || res!=FR_OK)
			break;
		flacblkh=(MD_Block_Head*)buf;
		endofmetadata=flacblkh->head&0X80;	//判断是不是最后一个block?
		blocklength=((uint32_t)flacblkh->size[0]<<16)|((uint16_t)flacblkh->size[1]<<8)|(flacblkh->size[2]);//得到块大小
        if((flacblkh->head&0x7f)==0) 		//head最低7位为0,则表示是STREAMINFO块
        { 
			res=f_read(fx,buf,blocklength,&br);
            if(res!=FR_OK)
				break;  
            fc->min_blocksize=((uint16_t)buf[0]<<8) |buf[1];					//最小块大小
            fc->max_blocksize=((uint16_t)buf[2]<<8) |buf[3];					//最大块大小
            fc->min_framesize=((uint32_t)buf[4]<<16)|((uint16_t)buf[5]<<8)|buf[6];//最小帧大小
            fc->max_framesize=((uint32_t)buf[7]<<16)|((uint16_t)buf[8]<<8)|buf[9];//最大帧大小
            fc->samplerate=((uint32_t)buf[10]<<12)|((uint16_t)buf[11]<<4)|((buf[12]&0xf0)>>4);//采样率
            fc->channels=((buf[12]&0x0e)>>1)+1;							//音频通道数
            fc->bps=((((uint16_t)buf[12]&0x01)<<4)|((buf[13]&0xf0)>>4))+1;	//采样位数16?24?32? 
            fc->totalsamples=((uint32_t)buf[14]<<24)|((uint32_t)buf[15]<<16)|((uint16_t)buf[16]<<8)|buf[17];//一个声道的总采样数
			fctrl->samplerate=fc->samplerate;
			fctrl->totsec=(fc->totalsamples/fc->samplerate);//得到总时间 
        }else 	//忽略其他帧的处理 
		{ 
            if(f_lseek(fx,f_tell(fx)+blocklength)!=FR_OK){ 
				goto fail1;
            }
		}
    } 
	if(fctrl->totsec==0)
		goto fail1;
	fctrl->outsamples=fc->max_blocksize*2;//PCM输出数据量(*2,表示2个声道的数据量)
	fctrl->bps=fc->bps;			//采样位数(16/24/32)
	fctrl->datastart=f_tell(fx);	//FLAC数据帧开始的地址
	fctrl->bitrate=((f_size(fx)-fctrl->datastart)*8)/fctrl->totsec;//得到FLAC的位速
	free(buf);
//	Debug_UnSafe_Printf("\r\n  Blocksize: %d .. %d\r\n", fc->min_blocksize,fc->max_blocksize);
//	Debug_UnSafe_Printf("  Framesize: %d .. %d\r\n",fc->min_framesize,fc->max_framesize);
//	Debug_UnSafe_Printf("  Samplerate: %d\r\n", fc->samplerate);
//	Debug_UnSafe_Printf("  Channels: %d\r\n", fc->channels);
//	Debug_UnSafe_Printf("  Bits per sample: %d\r\n", fc->bps);
//	Debug_UnSafe_Printf("  Metadata length: %d\r\n", fctrl->datastart);
//	Debug_UnSafe_Printf("  Total Samples: %lu\r\n",fc->totalsamples);
//	Debug_UnSafe_Printf("  Duration: %d s\r\n",fctrl->totsec);
//	Debug_UnSafe_Printf("  Bitrate: %d kbps\r\n",fctrl->bitrate); 
	return true;
fail1:
	free(buf);
fail0:
	return false;
} 


static Music_Decoder_Handle Flac_Create(const char* name,void* token){
	Flac_Handle* pHandle=malloc(sizeof(Flac_Handle));
	
	if(pHandle==NULL){
		goto fail0;
	}
	
	if(f_open(&pHandle->file,name,FA_READ)!=FR_OK){
		printf("flac文件打开失败\r\n");
		goto fail1;
	}
	
	if(flac_init(&pHandle->file,&pHandle->ctrl,&pHandle->fc)==false){
		printf("flac文件解析错误\r\n");
		goto fail2;
	}
	
	pHandle->fc.decoded0=malloc(pHandle->fc.max_blocksize*4);
	pHandle->fc.decoded1=malloc(pHandle->fc.max_blocksize*4);
	pHandle->inter_buffer=malloc(pHandle->fc.max_framesize);
	pHandle->total_sample=0;
	pHandle->used_sample=0;
	pHandle->used_byte=0;
	
	if(pHandle->fc.decoded0==NULL||pHandle->fc.decoded1==NULL||pHandle->inter_buffer==NULL){
		printf("flac解码器内存不足\r\n");
		goto fail3;
	}
	
	if(f_lseek(&pHandle->file,pHandle->ctrl.datastart)!=FR_OK){
		printf("flac文件读取错误\r\n");
		goto fail3;
	}
	
	if(f_read(&pHandle->file,pHandle->inter_buffer,pHandle->fc.max_framesize,&pHandle->read_byte)!=FR_OK){
		printf("flac文件读取错误\r\n");
		goto fail3;
	}
	
	return pHandle;
fail3:
	free(pHandle->inter_buffer);
	free(pHandle->fc.decoded0);
	free(pHandle->fc.decoded1);
fail2:
	f_close(&pHandle->file);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

static uint32_t Flac_GetMaxBuf(Music_Decoder_Handle use_handle){
	return MAX_BUFFER_OUT;
}

static bool Flac_File_Buf_Fresh(Flac_Handle* pHandle){
	if(pHandle->used_byte!=0){
		UINT br;
		memmove(pHandle->inter_buffer,&pHandle->inter_buffer[pHandle->used_byte],pHandle->read_byte-pHandle->used_byte);
		pHandle->read_byte-=pHandle->used_byte;
		pHandle->used_byte=0;
		if(f_read(	&pHandle->file,\
					&pHandle->inter_buffer[pHandle->read_byte],\
					pHandle->fc.max_framesize-pHandle->read_byte,\
					&br)!=FR_OK){
			return false;
		}
		pHandle->read_byte+=br;
		if(pHandle->read_byte==0){
			return false;
		}
	}
	return true;
}

static bool Flac_Decode(Flac_Handle* pHandle){
	int fill_sample;
	if(Flac_File_Buf_Fresh(pHandle)==false)
		return false;
	fill_sample=flac_decode_frame(&pHandle->fc,pHandle->inter_buffer,pHandle->read_byte);
	if(fill_sample<0)
		return false;
	pHandle->used_byte+=pHandle->fc.gb.index/8;
	pHandle->total_sample=fill_sample;
	pHandle->used_sample=0;
	return true;
}


static bool Flac_Fill_Buf(Music_Decoder_Handle use_handle,void* buf){
	Flac_Handle* pHandle=use_handle;
	uint32_t target_sample=MAX_BUFFER_OUT/(pHandle->ctrl.bps==16?4:8);
	uint8_t* fill_buf=buf;
	while(target_sample!=0){
		if(pHandle->total_sample-pHandle->used_sample>=target_sample){
			if(pHandle->ctrl.bps==16){
				flac_decorrelation16(&pHandle->fc,\
									pHandle->fc.decoded0+pHandle->used_sample,\
									pHandle->fc.decoded1+pHandle->used_sample,\
									target_sample,\
									(s16*)fill_buf);
			}
			else{
				flac_decorrelation24(&pHandle->fc,\
									pHandle->fc.decoded0+pHandle->used_sample,\
									pHandle->fc.decoded1+pHandle->used_sample,\
									target_sample,\
									(s32*)fill_buf);
			}
			pHandle->used_sample+=target_sample;
			return true;
		}
		else if(pHandle->total_sample!=pHandle->used_sample){
			if(pHandle->ctrl.bps==16){
				flac_decorrelation16(&pHandle->fc,\
									pHandle->fc.decoded0+pHandle->used_sample,\
									pHandle->fc.decoded1+pHandle->used_sample,\
									pHandle->total_sample-pHandle->used_sample,\
									(s16*)fill_buf);
			}
			else{
				flac_decorrelation24(&pHandle->fc,\
									pHandle->fc.decoded0+pHandle->used_sample,\
									pHandle->fc.decoded1+pHandle->used_sample,\
									pHandle->total_sample-pHandle->used_sample,\
									(s32*)fill_buf);
			}
			target_sample-=pHandle->total_sample-pHandle->used_sample;
			fill_buf+=(pHandle->total_sample-pHandle->used_sample)*(pHandle->ctrl.bps==16?4:8);
			pHandle->used_sample=pHandle->total_sample;
		}
		if(Flac_Decode(pHandle)==false){
			return false;
		}
	}
	return true;
}

static uint32_t Flac_GetSamplate(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	return pHandle->fc.samplerate;
}

static void Flac_Delete(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	free(pHandle->inter_buffer);
	free(pHandle->fc.decoded0);
	free(pHandle->fc.decoded1);
	f_close(&pHandle->file);
	free(pHandle);
}

static uint32_t Flac_Get_DataSize(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	return pHandle->ctrl.bps;
}

static uint32_t Flac_GetTotalSec(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	return pHandle->ctrl.totsec;
}

static uint32_t Flac_GetNowPos(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	return (f_tell(&pHandle->file)-pHandle->ctrl.datastart)/pHandle->fc.max_framesize;
}

static uint32_t Flac_GetTotalPos(Music_Decoder_Handle use_handle){
	Flac_Handle* pHandle=use_handle;
	return (f_size(&pHandle->file)-pHandle->ctrl.datastart)/pHandle->fc.max_framesize;
}

static bool Flac_SetPos(Music_Decoder_Handle use_handle,uint32_t pos){
	Flac_Handle* pHandle=use_handle;
	uint32_t buf_ptr=f_tell(&pHandle->file);
	uint32_t br;
	int frame_start;
	
	pos*=pHandle->fc.max_framesize;
	pos+=pHandle->ctrl.datastart;
	if(f_lseek(&pHandle->file,pos)!=FR_OK){
		goto fail0;
	}
	
	if(f_read(&pHandle->file,pHandle->inter_buffer,pHandle->fc.max_framesize,&br)!=FR_OK){
		goto fail0;
	}
	
	frame_start=flac_seek_frame(pHandle->inter_buffer,br,&pHandle->fc);
	if(frame_start<0){
		goto fail0;
	}
	
	if(f_lseek(&pHandle->file,f_tell(&pHandle->file)-pHandle->fc.max_framesize+frame_start)!=FR_OK){
		goto fail0;
	}
	
	if(f_read(&pHandle->file,pHandle->inter_buffer,pHandle->fc.max_framesize,&br)!=FR_OK){
		goto fail0;
	}
	pHandle->read_byte=br;
	pHandle->used_byte=0;
	pHandle->total_sample=0;
	pHandle->used_sample=0;
	return true;
fail0:
	f_lseek(&pHandle->file,buf_ptr);
	return false;
}

static const char*const file_surport[]={
	"flac",NULL
};

const Music_Decoder Flac_Decoder_Device={
	.pCreate=Flac_Create,
	.pDelete=Flac_Delete,
	.pFillBuf=Flac_Fill_Buf,
	.pGetBufMax=Flac_GetMaxBuf,
	.pGetDataSize=Flac_Get_DataSize,
	.pGetNowPos=Flac_GetNowPos,
	.pGetSamplate=Flac_GetSamplate,
	.pGetTotalPos=Flac_GetTotalPos,
	.pGetTotalSec=Flac_GetTotalSec,
	.pSetPos=Flac_SetPos,
	.surport_file=file_surport
};

