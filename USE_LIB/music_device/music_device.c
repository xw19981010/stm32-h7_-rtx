#include "music_device.h"
#include "my_list.h"
#include "hw_music.h"
#include "setting_save.h"
#include "log.h"
#include "dsp/svc_glo.h"

#define MUSIC_BUF_NUM		2
#define MUSIC_MESS_MAX_NUM	20
#define MUSIC_HOOK_TASK_MES_NUM	 20
#define MUSIC_TASK_STK_SIZE		(1024*8)
#define MUSIC_END_TASK_STK_SIZE (1024)

#define FILL_PRIO			1
#define CTRL_PRIO			2

#define SVC_PERSISTEN_MEM_SIZE	(0x558<<2)
#define SVC_SCRATCH_MEM_SIZE	(0xB40<<2)

//操作命令
typedef enum{
	BUF0_FILL_OVER,			//缓冲区0已填充满
	BUF1_FILL_OVER,			//缓冲区1已填充满
	MUSIC_START,			//开始播放
	MUSIC_STOP,				//停止播放
	MUSIC_INSERT,			//播放列表中插入音乐
	MUSIC_REMOVE,			//播放列表移除音乐
	MUSIC_SET_HOOK,			//音乐设置钩子函数
	MUSIC_SET_PTR,			//音乐设置钩子函数调用参数
	MUSIC_SET_POS,			//音乐设置播放进度
	MUSIC_GET_NOWPOS,		//音乐获取当前播放进度
	MUSIC_GET_TOTALPOS,		//音乐获取播放进度全长
	MUSIC_GET_TOTALSEC,		//音乐获取播放总时长
	MUSIC_SET_PRIORITY,		//音乐设置优先级
	MUSIC_GET_BUF_DATA,
	MUSIC_GET_BUF_SIZE,
	MUSIC_GET_DATA_SIZE,
	MUSIC_GET_SAMPLATE,
	MUSIC_HANDLE_CREATE,
	MUSIC_HANDLE_DELETE,
}Music_Cmd;

//音乐句柄
typedef struct{
	const Music_Decoder* pDecoder; //使用的解码器
	bool is_suspend;			   //音频状态:true:暂停；false:播放
	bool is_end;				   //音频是否播放完毕：true:已播放完毕；false:未播放完毕
	Music_Decoder_Handle handle;   //解码器句柄
	int priority;				   //音频优先级
	void* use_ptr;				   //钩子函数调用参数
	void* DMA_Buf[MUSIC_BUF_NUM];  //音频DMA缓冲
	void (*end_hook)(void*);	   //播放结束时自动调用的钩子函数
	uint8_t now_buf;
}Music_Handle_Type;

typedef struct{
	Music_Cmd cmd;				   //命令值
	Music_Handle_Type* handle;	   //执行命令的音乐句柄
	osSemaphoreId_t ret_sem_id;    //用于同步的信号量
	void* param;				   //命令参数
	bool* is_success;
}Music_Message;

typedef struct{
	void (*end_hook)(void*);
	void* ues_data;
}Music_End_Hook_Message;

typedef My_List_Root Decoder_List_Type;
typedef My_List_Root Music_List_Type;

static osRtxThread_t Music_Task_WorkBuf __THREAD;
static __ALIGNED(8) uint8_t Music_Task_Stack[MUSIC_TASK_STK_SIZE];
static const osThreadAttr_t Music_Task_Attr={
	.attr_bits=osThreadDetached,
	.cb_mem=&Music_Task_WorkBuf,
	.cb_size=sizeof(Music_Task_WorkBuf),
	.stack_mem=Music_Task_Stack,
	.stack_size=sizeof(Music_Task_Stack),
	.priority=MUSIC_TASK_PRIO,
	.name="Music Task"
};
static osThreadId_t Music_Task_ID=NULL;

static __ALIGNED(4) uint8_t Music_Message_Buf[osRtxMessageQueueMemSize(MUSIC_MESS_MAX_NUM,sizeof(Music_Message*))];
static osRtxMessageQueue_t Music_Message_Queue_Work_Buf __QUEUE;
static const osMessageQueueAttr_t Music_Message_Attr={
	.mq_mem=Music_Message_Buf,
	.mq_size=sizeof(Music_Message_Buf),
	.cb_mem=&Music_Message_Queue_Work_Buf,
	.cb_size=sizeof(Music_Message_Queue_Work_Buf),
	.attr_bits=0,
	.name="Music Queue"
};
static osMessageQueueId_t Music_Message_Queue_ID=NULL;

static osRtxMutex_t Music_Func_Lock_WorkBuf __MUTEX;
static const osMutexAttr_t Music_Func_Lock_Attr={
	.attr_bits=osMutexPrioInherit|osMutexRobust,
	.cb_mem=&Music_Func_Lock_WorkBuf,
	.cb_size=sizeof(Music_Func_Lock_WorkBuf),
	.name="Music Func Lock"
};
static osMutexId_t Music_Func_Lock_ID=NULL;

static osRtxThread_t Music_Hook_Task_WorkBuf __THREAD;
static __ALIGNED(8) uint8_t Music_Hook_Task_Stack[MUSIC_END_TASK_STK_SIZE];
static const osThreadAttr_t Music_Hook_Task_Attr={
	.attr_bits=osThreadDetached,
	.cb_mem=&Music_Hook_Task_WorkBuf,
	.cb_size=sizeof(Music_Hook_Task_WorkBuf),
	.stack_mem=Music_Hook_Task_Stack,
	.stack_size=sizeof(Music_Hook_Task_Stack),
	.priority=MUSIC_END_HOOK_PRIO,
	.name="Music Hook Task"
};
static osThreadId_t Music_Hook_Task_ID=NULL;

static __ALIGNED(4) uint8_t Music_Hook_Message_Buf[osRtxMessageQueueMemSize(MUSIC_HOOK_TASK_MES_NUM,sizeof(Music_End_Hook_Message))];
static osRtxMessageQueue_t Music_Hook_Message_Queue_Work_Buf __QUEUE;
static const osMessageQueueAttr_t Music_Hook_Message_Attr={
	.mq_mem=Music_Hook_Message_Buf,
	.mq_size=sizeof(Music_Hook_Message_Buf),
	.cb_mem=&Music_Hook_Message_Queue_Work_Buf,
	.cb_size=sizeof(Music_Hook_Message_Queue_Work_Buf),
	.attr_bits=0,
	.name="Music Hook Queue"
};
static osMessageQueueId_t Music_Hook_Message_Queue_ID=NULL;
static __ALIGNED(4) uint8_t SVC_Persistent_Mem[SVC_PERSISTEN_MEM_SIZE]__IN_AXI_RAM;
static __ALIGNED(4) uint8_t SVC_Scratch_Mem[SVC_SCRATCH_MEM_SIZE]__IN_AXI_RAM;
//解码器链表
static Decoder_List_Type Decoder_List;
//音频就绪链表
static Music_List_Type	Music_Ready_List;

//全局资源函数锁
__STATIC_FORCEINLINE void Music_Func_Lock(void){
	osMutexAcquire(Music_Func_Lock_ID,osWaitForever);
}

__STATIC_FORCEINLINE void Music_Func_UnLock(void){
	osMutexRelease(Music_Func_Lock_ID);
}

//硬件启动音频播放
__STATIC_FORCEINLINE void Music_HW_Safe_Start(void){
	HW_Music_Start();
}

//硬件停止音频播放
__STATIC_FORCEINLINE void Music_HW_Safe_Stop(void){
	HW_Music_Stop();
}

//获取音频就绪链表中首个音频
__STATIC_FORCEINLINE Music_Handle_Type* First_Music(Music_List_Type* pRunList){
	return My_List_Get_Item_Num(pRunList)==0?NULL:My_List_Get_Start(pRunList)->data;
}

//在解码器链表中寻找指定的解码器
static const Music_Decoder* Find_Decoder(Decoder_List_Type* pDecoder_List,const char* name){
	TRAVERSE_MY_LIST(pItem,pDecoder_List){
		const Music_Decoder* pDecoder=pItem->data;
		for(const char*const* surport_file=pDecoder->surport_file;*surport_file!=NULL;surport_file++){
			if(strcasecmp(*surport_file,name)==0){
				return pDecoder;
			}
		}
	}
	return NULL;
}

//在解码器链表中移除指定的解码器
static bool Remove_Decoder(Decoder_List_Type* pDecoder_List,const Music_Decoder* pDecoder){
	TRAVERSE_MY_LIST(pItem,pDecoder_List){
		if(pItem->data==(void*)pDecoder){
			My_List_Remove_Item(pItem);
			free(pItem);
			return true;
		}
	}
	return false;
}

//在解码器链表中插入一个解码器
static bool Insert_Decoder(Decoder_List_Type* pDecoder_List,const Music_Decoder* pDecoder){
	My_List_Item* pInsert;
	pInsert=malloc(sizeof(My_List_Item));
	if(pInsert==NULL)
		return false;
	pInsert->data=(void*)pDecoder;
	My_List_Insert_End(pDecoder_List,pInsert);
	return true;
}


static bool svc_volume_gain_set(int16_t gain,void* svc_persis_mem){
	svc_dynamic_param_t svc_param;
	svc_param.mute = 0;             /* disable mute */
    svc_param.target_volume_dB = gain*2; /* volume targeted value */
	
    svc_param.enable_compr = 1;     /* enable compression */
    svc_param.attack_time = 2103207220;
    svc_param.release_time = 2146924480;	
    svc_param.quality = 1;	        /* High quality */
	return svc_setConfig(&svc_param,svc_persis_mem)==SVC_ERROR_NONE;
}

//音频音量软件缩放，仅能处理16bit的音频
//buf:音频缓冲区首地址
//len:缓冲区长度
//gain:增益，单位db
static void svc_gain_process(void* svc_persis_mem,uint16_t* buf,uint32_t buf_len,int16_t gain){
	buffer_t inBuf;
	inBuf.nb_channels=2;
	inBuf.nb_bytes_per_Sample=2;
	inBuf.buffer_size=buf_len/(inBuf.nb_channels*inBuf.nb_bytes_per_Sample);
	inBuf.data_ptr=buf;
	inBuf.mode=INTERLEAVED;
	svc_volume_gain_set(gain,svc_persis_mem);
	svc_process(&inBuf,&inBuf,svc_persis_mem);//仅支持16bit
}

static void svc_lib_init(void* svc_persis_mem,void* svc_scrach_mem){
	svc_static_param_t svc_static_param;
	__HAL_RCC_CRC_CLK_ENABLE();
	svc_reset(svc_persis_mem,svc_scrach_mem);
	svc_static_param.delay_len = 80;
    svc_static_param.joint_stereo =	1;
    svc_setParam(&svc_static_param, svc_scrach_mem);
}

static inline void stream_24bit_to_16bit(uint32_t* buf,uint32_t sample_num){
	uint16_t* pbuf=(uint16_t*)buf;
	for(size_t i=0;i<sample_num;i++){
		pbuf[i]=(buf[i]&0x00ffff00)>>8;
	}
}

static inline void stream_32bit_to_16bit(uint32_t* buf,uint32_t sample_num){
	uint16_t* pbuf=(uint16_t*)buf;
	for(size_t i=0;i<sample_num;i++){
		pbuf[i]=buf[i]>>16;
	}
}

//填充指定的缓冲区
//pHandle：音频句柄
//choose：选择的缓冲区
//返回值：true填充成功
//		  false填充失败（音频播放结束）
static bool Music_Data_Fill(Music_Handle_Type* pHandle,uint8_t choose,void* svc_persis_mem){
	bool res;
	uint32_t data_size=pHandle->pDecoder->pGetDataSize(pHandle->handle);
	uint32_t buf_size=pHandle->pDecoder->pGetBufMax(pHandle->handle);
	res=pHandle->pDecoder->pFillBuf(pHandle->handle,pHandle->DMA_Buf[choose]);
	switch(data_size){
	case 16:
		break;	
	case 24:
		stream_24bit_to_16bit(pHandle->DMA_Buf[choose],buf_size/4);
		data_size=16;
		buf_size>>=1;
		break;
	case 32:
		stream_32bit_to_16bit(pHandle->DMA_Buf[choose],buf_size/4);
		data_size=16;
		buf_size>>=1;
		break;
	}
	svc_gain_process(svc_persis_mem,pHandle->DMA_Buf[choose],buf_size,Common_Setting.soft_volume_gain);
	SCB_CleanInvalidateDCache();
	return res;
}

//用0填充指定的缓冲区
//pHandle：音频句柄
//choose：选择的缓冲区
//返回值：true填充成功
//		  false填充失败（音频播放结束）
static bool Zero_Data_Fill(Music_Handle_Type* pHandle,uint8_t choose,void* svc_persis_mem){
	memset(pHandle->DMA_Buf[choose],0,pHandle->pDecoder->pGetBufMax(pHandle->handle));
	SCB_CleanInvalidateDCache();
	return true;
}

//根据句柄设置硬件参数
//use_handle:句柄
//返回值：true设置成功
//		  fale设置失败
static bool Setting_Music(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	uint32_t data_size=pHandle->pDecoder->pGetDataSize(pHandle->handle);
	uint32_t buf_size=pHandle->pDecoder->pGetBufMax(pHandle->handle);
	if(pHandle->now_buf==1){//需要交换缓冲区地址，保证音频播放顺序正确
		void* temp=pHandle->DMA_Buf[0];
		pHandle->DMA_Buf[0]=pHandle->DMA_Buf[1];
		pHandle->DMA_Buf[1]=temp;
		pHandle->now_buf=0;
	}
	switch(data_size){
	case 16:
		break;
	case 24:
		data_size=16;
		buf_size>>=1;
		break;
	case 32:
		data_size=16;
		buf_size>>=1;
		break;
	}
	return HW_Music_Config(data_size,\
						pHandle->pDecoder->pGetSamplate(pHandle->handle),\
						pHandle->DMA_Buf[0],\
						pHandle->DMA_Buf[1],\
						buf_size);
}

//检查句柄参数硬件是否支持
//use_handle:句柄
//返回值：true支持
//		  fale不支持
static bool Setting_Music_Check(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	uint32_t data_size=pHandle->pDecoder->pGetDataSize(pHandle->handle);
	uint32_t buf_size=pHandle->pDecoder->pGetBufMax(pHandle->handle);
	switch(data_size){
	case 16:
		break;
	case 24:
		data_size=16;
		buf_size>>=1;
		break;
	case 32:
		data_size=16;
		buf_size>>=1;
		break;
	}
	return HW_Muisc_Config_Check_Param(data_size,\
						pHandle->pDecoder->pGetSamplate(pHandle->handle),\
						pHandle->DMA_Buf[0],\
						pHandle->DMA_Buf[1],\
						buf_size);
}

//将一个句柄按照优先级插入到音频就绪链表中
//pHandle:插入的句柄
//pRunList:被插入的链表
//返回值：true插入成功
//		  fale插入失败（内存不足）
static bool Insert_Music_To_RunList(Music_Handle_Type* pHandle,Music_List_Type* pRunList){
	My_List_Item* pItem=malloc(sizeof(My_List_Item));
	if(pItem==NULL)
		return false;
	pItem->data=pHandle;
	TRAVERSE_MY_LIST(item,pRunList){
		if(pHandle->priority>((Music_Handle_Type*)item->data)->priority){
			My_List_Insert_Item_Front(item,pItem);
			return true;
		}
	}
	My_List_Insert_End(pRunList,pItem);
	return true;
}

//将一个句柄从音频就绪链表中移除
//pHandle:待移除的句柄
//pRunList:音频就绪链表
//返回值：true移除成功
//		  fale移除失败（句柄不在链表中）
static bool Remove_Music_To_RunList(Music_Handle_Type* pHandle,Music_List_Type* pRunList){
	TRAVERSE_MY_LIST(item,pRunList){
		if(item->data==pHandle){
			My_List_Remove_Item(item);
			free(item);
			return true;
		}
	}
	return false;
}

//创建一个音频句柄
//pDecoder_List:解码器链表
//pDecoder:指定音频的解码器，可以为NULL，这样将根据后缀名在解码器链表中搜索适合的解码器
//file_name:音频文件的名称，一般为文件名，作为解码器的输入
//priority:音频的播放优先级，数值越大优先级越高，优先级高的会抢占优先级低的音频播放，只有在优先级高的音频播放完毕/被删除，
//			优先级低的音频才会被播放，优先级高的音乐暂停时，优先级低的音乐不会被播放。
//end_hook:音频播放完毕后自动调用的钩子函数,可以为NULL
//use_ptr:钩子函数的参数，可以为NULL
//返回值：Music_Handle 创建的音频句柄，为NULL代表创建失败
static Music_Handle Create_Music_Handle(Decoder_List_Type* pDecoder_List,const Music_Decoder* pDecoder,void* token,const char* file_name,int priority,void(*end_hook)(void*),void* use_ptr){
	Music_Handle_Type* pHandle=malloc(sizeof(Music_Handle_Type));
	if(pHandle==NULL)
		goto fail0;
	if(pDecoder==NULL){
		const char* dot_name;
		dot_name=strrchr(file_name,'.');
		if(dot_name==NULL)
			goto fail1;
		dot_name++;
		Music_Func_Lock();
		pHandle->pDecoder=Find_Decoder(pDecoder_List,dot_name);
		Music_Func_UnLock();
		if(pHandle->pDecoder==NULL)
			goto fail1;
	}
	else{
		pHandle->pDecoder=pDecoder;
	}
	
	pHandle->handle=pHandle->pDecoder->pCreate(file_name,token);
	if(pHandle->handle==NULL)
		goto fail1;
	for(uint32_t i=0;i<MUSIC_BUF_NUM;i++){
		pHandle->DMA_Buf[i]=NULL;
	}
	for(uint32_t i=0;i<MUSIC_BUF_NUM;i++){
		pHandle->DMA_Buf[i]=malloc(pHandle->pDecoder->pGetBufMax(pHandle->handle));
		if(pHandle->DMA_Buf[i]==NULL)
			goto fail2;
	}
	pHandle->is_suspend=true;
	pHandle->is_end=false;
	pHandle->priority=priority;
	pHandle->end_hook=end_hook;
	pHandle->use_ptr=use_ptr;
	pHandle->now_buf=0;
	return pHandle;
fail2:
	for(uint32_t i=0;i<MUSIC_BUF_NUM;i++){
		free(pHandle->DMA_Buf[i]);
	}
	pHandle->pDecoder->pDelete(pHandle->handle);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

//释放一个音乐句柄
//use_handle：释放的音乐句柄
static void Delete_Music_Handle(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	if(pHandle!=NULL){
		pHandle->pDecoder->pDelete(pHandle->handle);
		for(uint32_t i=0;i<MUSIC_BUF_NUM;i++){
			free(pHandle->DMA_Buf[i]);
		}
		free(pHandle);
	}
}

//在DMA中断中调用的函数(hw_music.c)
void Music_Buf0_Over(void){
	static const Music_Message isr_message={
		.cmd=BUF0_FILL_OVER,
		.handle=NULL,
		.is_success=NULL,
		.param=NULL,
		.ret_sem_id=NULL
	};
	const Music_Message* const pmess=&isr_message;
	osMessageQueuePut(Music_Message_Queue_ID,&pmess,FILL_PRIO,0);
}
void Music_Buf1_Over(void){
	static const Music_Message isr_message={
		.cmd=BUF1_FILL_OVER,
		.handle=NULL,
		.is_success=NULL,
		.param=NULL,
		.ret_sem_id=NULL
	};
	const Music_Message* const pmess=&isr_message;
	osMessageQueuePut(Music_Message_Queue_ID,&pmess,FILL_PRIO,0);
}

static void Music_Start_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	if(pmess->is_success!=NULL)
		*pmess->is_success=true;
	if(pmess->handle->is_suspend==true){
		pmess->handle->is_suspend=false;
		if(pmess->handle==First_Music(ready_list)){
			Music_HW_Safe_Start();
		}
	}
}

static void Music_Stop_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	if(pmess->is_success!=NULL)
		*pmess->is_success=true;
	if(pmess->handle->is_suspend==false){
		pmess->handle->is_suspend=true;
		if(pmess->handle==First_Music(ready_list)){
			Music_HW_Safe_Stop();
		}
	}
}

static void Music_Insert_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_Handle_Type* pHandle=First_Music(ready_list);
	bool res=true;
	res&=Insert_Music_To_RunList(pmess->handle,ready_list);
	if(res!=true){
		goto end;
	}
	if(pHandle!=First_Music(ready_list)){
		res&=Setting_Music(First_Music(ready_list));
		if(res!=true){
			goto fail1;
		}
		if(My_List_Get_Item_Num(ready_list)!=0 && First_Music(ready_list)->is_suspend==false){
			Music_HW_Safe_Start();
		}
		goto end;
	}
	else{
		if(Setting_Music_Check(pmess->handle)){
			goto end;
		}
		else
			goto fail1;
	}
fail1:
	Remove_Music_To_RunList(pmess->handle,ready_list);
end:
	if(pmess->is_success!=NULL){
		*pmess->is_success=res;
	}
	return;
}

static void Music_Remove_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_Handle_Type* pHandle;
	bool res=true;
	if(My_List_Get_Item_Num(ready_list)==0){
		res=false;
		goto end;
	}
	pHandle=First_Music(ready_list);
	res&=Remove_Music_To_RunList(pmess->handle,ready_list);
	if(res!=true){
		goto end;
	}
	if(My_List_Get_Item_Num(ready_list)==0){
		Music_HW_Safe_Stop();
		goto end;
	}
	else if(pHandle!=First_Music(ready_list)){
		res=Setting_Music(First_Music(ready_list));
		if(res!=true){
			goto end;
		}
		if(My_List_Get_Item_Num(ready_list)!=0 && First_Music(ready_list)->is_suspend==false){
			Music_HW_Safe_Start();
		}
	}
end:
	if(pmess->is_success!=NULL){
		*pmess->is_success=res;
	}
}

static void Music_Set_Pos_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	bool res=true;
	res&=pmess->handle->pDecoder->pSetPos(pmess->handle->handle,*(uint32_t*)pmess->param);
	if(res==false){
		goto end;
	}
	if(pmess->handle->is_end){
		pmess->handle->is_end=false;
		TRAVERSE_MY_LIST(pItem,ready_list){//避免重复插入
			if(pItem->data==pmess->handle){
				goto end;
			}
		}
		Music_Insert_Use_Process(pmess,ready_list);
	}
end:
	if(pmess->is_success!=NULL){
		*pmess->is_success=res;
	}
}

static void Music_Set_Priority_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_Handle_Type* pHandle=First_Music(ready_list);
	bool res=true;
	if(pHandle->is_end){
		pHandle->priority=*(int*)pmess->param;
		goto end;
	}
	res&=Remove_Music_To_RunList(pmess->handle,ready_list);
	if(res==false){
		goto end;
	}
	pHandle->priority=*(int*)pmess->param;
	res&=Insert_Music_To_RunList(pmess->handle,ready_list);
	if(res==false){
		goto end;
	}
	if(pHandle!=First_Music(ready_list)){
		res&=Setting_Music(First_Music(ready_list));
		if(res==false){
			Remove_Music_To_RunList(pmess->handle,ready_list);
			goto end;
		}
		if(My_List_Get_Item_Num(ready_list)!=0 && First_Music(ready_list)->is_suspend==false){
			Music_HW_Safe_Start();
		}
	}
	else{
		res&=Setting_Music_Check(pmess->handle);
	}
end:
	if(pmess->is_success!=NULL){
		*pmess->is_success=res;
	}
}

static void Music_Get_Buf_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_BufType* pBuf=pmess->param;
	bool res=true;
	uint32_t buf_max_size=pmess->handle->pDecoder->pGetBufMax(pmess->handle->handle);
	pBuf->data_size=pmess->handle->pDecoder->pGetDataSize(pmess->handle->handle);
	switch(pBuf->data_size){
	case 16:
		break;
	case 24:
		buf_max_size>>=1;
		pBuf->data_size=16;
		break;
	case 32:
		buf_max_size>>=1;
		pBuf->data_size=16;
		break;
	}
	
	if(pBuf->buf_size<=buf_max_size){
		memcpy(pBuf->buf_addr,pmess->handle->DMA_Buf[pmess->handle->now_buf],pBuf->buf_size);
	}
	else if(pBuf->buf_size<=2*buf_max_size){
		memcpy(pBuf->buf_addr,pmess->handle->DMA_Buf[pmess->handle->now_buf],buf_max_size);
		memcpy((uint8_t*)pBuf->buf_addr+buf_max_size,pmess->handle->DMA_Buf[(pmess->handle->now_buf+1)%MUSIC_BUF_NUM],pBuf->buf_size-buf_max_size);
	}
	else{
		memcpy(pBuf->buf_addr,pmess->handle->DMA_Buf[pmess->handle->now_buf],buf_max_size);
		memcpy((uint8_t*)pBuf->buf_addr+buf_max_size,pmess->handle->DMA_Buf[(pmess->handle->now_buf+1)%MUSIC_BUF_NUM],buf_max_size);
		pBuf->buf_size=2*buf_max_size;
	}
	if(pmess->is_success!=NULL){
		*pmess->is_success=res;
	}
}

static void Music_Fill_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_Handle_Type* target_handle;
	target_handle= pmess->handle==NULL? First_Music(ready_list):pmess->handle;
	
	if(target_handle==NULL){
		if(pmess->is_success!=NULL)
			*pmess->is_success=false;
		return;
	}
	else{
		if(pmess->is_success!=NULL)
			*pmess->is_success=true;
	}
		
	target_handle->now_buf=pmess->cmd==BUF0_FILL_OVER?1:0;
	if(target_handle->is_end==false){
		if(Music_Data_Fill(target_handle,pmess->cmd==BUF0_FILL_OVER?0:1,SVC_Persistent_Mem)==false){
			target_handle->is_end=true;
		}
	}
	else{
		Zero_Data_Fill(target_handle,pmess->cmd==BUF0_FILL_OVER?0:1,SVC_Persistent_Mem);
		if(target_handle==First_Music(ready_list)){
			void(*end_hook)(void*);
			void* use_ptr;
			Music_HW_Safe_Stop();
			do{
				end_hook=First_Music(ready_list)->end_hook;
				use_ptr=First_Music(ready_list)->use_ptr;
				Remove_Music_To_RunList(First_Music(ready_list),ready_list);
				if(end_hook!=NULL){
					Music_End_Hook_Message buf_mess;
					buf_mess.end_hook=end_hook;
					buf_mess.ues_data=use_ptr;
					if(osMessageQueuePut(Music_Hook_Message_Queue_ID,&buf_mess,0,0)!=osOK){
						printf("音频钩子队列溢出!\r\n");
					}
				}
			}while(First_Music(ready_list)!=NULL&&Setting_Music(First_Music(ready_list))==false);
			if(First_Music(ready_list)!=NULL&&First_Music(ready_list)->is_suspend==false){
				Music_HW_Safe_Start();
			}
		}
	}
}

typedef struct{
	Music_Handle_Type** ppHandle;
	Music_InitType* pInit;
	const char* name;
}Music_Create_Param;

static void Music_Create_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Music_Create_Param* pParam=pmess->param;
	if(pParam->pInit!=NULL)
		*(pParam->ppHandle)=Create_Music_Handle(&Decoder_List,\
												pParam->pInit->pDecoder,\
												pParam->pInit->token,\
												pParam->name,\
												pParam->pInit->priority,\
												pParam->pInit->end_hook,\
												pParam->pInit->use_ptr);
	else
		*(pParam->ppHandle)=Create_Music_Handle(&Decoder_List,NULL,NULL,pParam->name,0,NULL,NULL);
	*pmess->is_success=true;
}

static void Music_Delete_Use_Process(Music_Message* pmess,Music_List_Type* ready_list){
	Delete_Music_Handle(pmess->handle);
	*pmess->is_success=true;
}


//音频处理的主任务，用于自动填充缓冲区、管理音频播放
static __NO_RETURN void Music_Task(void* p){
	Music_Handle_Type* target_handle;
	static Music_Message* pmess;
	while(true){
		osMessageQueueGet(Music_Message_Queue_ID,&pmess,NULL,osWaitForever);
		switch(pmess->cmd){
		case BUF0_FILL_OVER:
		case BUF1_FILL_OVER:
			Music_Fill_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_START:
			Music_Start_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_STOP:
			Music_Stop_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_INSERT:
			Music_Insert_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_REMOVE:
			Music_Remove_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_SET_HOOK:
			pmess->handle->end_hook=pmess->param;
			break;
		case MUSIC_SET_PTR:
			pmess->handle->use_ptr=pmess->param;
			break;
		case MUSIC_SET_POS:
			Music_Set_Pos_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_GET_NOWPOS:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetNowPos(pmess->handle->handle);
			break;
		case MUSIC_GET_TOTALPOS:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetTotalPos(pmess->handle->handle);
			break;
		case MUSIC_GET_TOTALSEC:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetTotalSec(pmess->handle->handle);
			break;
		case MUSIC_SET_PRIORITY:
			Music_Set_Priority_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_GET_BUF_DATA:
			Music_Get_Buf_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_GET_BUF_SIZE:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetBufMax(pmess->handle->handle);
			break;
		case MUSIC_GET_DATA_SIZE:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetDataSize(pmess->handle->handle);
			break;
		case MUSIC_GET_SAMPLATE:
			*(uint32_t*)pmess->param=pmess->handle->pDecoder->pGetSamplate(pmess->handle->handle);
			break;
		case MUSIC_HANDLE_CREATE:
			Music_Create_Use_Process(pmess,&Music_Ready_List);
			break;
		case MUSIC_HANDLE_DELETE:
			Music_Delete_Use_Process(pmess,&Music_Ready_List);
			break;
		default:
			if(pmess->is_success!=NULL){
				pmess->is_success=false;
			}
			break;
		}
		if(pmess->ret_sem_id!=NULL)
			osSemaphoreRelease(pmess->ret_sem_id);
	}
}

//钩子函数运行任务，优先级需要比主任务高
static void Music_End_Hook_Task_Func(void* p){
	static Music_End_Hook_Message recive_mess;
	while(true){
		osMessageQueueGet(Music_Hook_Message_Queue_ID,&recive_mess,NULL,osWaitForever);
		recive_mess.end_hook(recive_mess.ues_data);
	}
}

//向主任务发送一个命令，并等待命令响应
static bool Music_Send_Cmd(Music_Message* mess){
	mess->ret_sem_id=osSemaphoreNew(1,0,NULL);
	if(mess->ret_sem_id==NULL)
		return false;
	if(osMessageQueuePut(Music_Message_Queue_ID,&mess,CTRL_PRIO,osWaitForever)!=osOK){
		osSemaphoreDelete(mess->ret_sem_id);
		return false;
	}
	osSemaphoreAcquire(mess->ret_sem_id,osWaitForever);
	osSemaphoreDelete(mess->ret_sem_id);
	return true;
}

static bool Music_Exec_Cmd(Music_Handle use_handle,Music_Cmd cmd,void* param){
	bool res=true;
	Music_Message buf_mess;
	if(use_handle==NULL){
		return false;
	}
	buf_mess.cmd=cmd;
	buf_mess.handle=use_handle;
	buf_mess.param=param;
	buf_mess.is_success=&res;
	if(Music_Send_Cmd(&buf_mess)==false){
		return false;
	}
	return res;
}


//向就绪链表插入一个音频
static bool Music_Insert(Music_Handle use_handle){
	return Music_Exec_Cmd(use_handle,MUSIC_INSERT,NULL);
}

//就绪链表移除指定音频
static bool Music_Remove(Music_Handle use_handle){
	return Music_Exec_Cmd(use_handle,MUSIC_REMOVE,NULL);
}

//创建一个音频并将其插入到就绪链表中，音频默认为暂停状态，
//name:音频名称，通常为文件名
//pinfo:音乐额外参数，NULL时使用默认参数
Music_Handle Music_Create(const char* name,Music_InitType* pInit){
	Music_Handle_Type* pHandle;
	Music_Create_Param param;
	param.name=name;
	param.pInit=pInit;
	param.ppHandle=&pHandle;
	Music_Exec_Cmd((void*)1,MUSIC_HANDLE_CREATE,&param);
	if(pHandle==NULL){
		return NULL;
	}
	Music_Exec_Cmd(pHandle,BUF0_FILL_OVER,NULL);
	Music_Exec_Cmd(pHandle,BUF1_FILL_OVER,NULL);
	if(Music_Insert(pHandle)==false){
		Music_Exec_Cmd(pHandle,MUSIC_HANDLE_DELETE,NULL);
		return NULL;
	}
	return pHandle;
}

//开始播放音频
//use_handle:音频句柄
void Music_Start(Music_Handle use_handle){
	Music_Exec_Cmd(use_handle,MUSIC_START,NULL);
}

//暂停播放音频
//use_handle:音频句柄
void Music_Stop(Music_Handle use_handle){
	Music_Exec_Cmd(use_handle,MUSIC_STOP,NULL);
}

//从播放链表移除音频并删除
//use_handle:音频句柄
void Music_Delete(Music_Handle use_handle){
	Music_Remove(use_handle);
	Music_Exec_Cmd(use_handle,MUSIC_HANDLE_DELETE,NULL);
}

//获取播放链表当前播放的音乐，为NULL表示无音频
//返回值:音频句柄
Music_Handle Music_Get_Current(void){
	return First_Music(&Music_Ready_List);
}

//设置音频播放结束的钩子函数
//use_handle:句柄
//hook:钩子函数
void Music_Set_EndHook(Music_Handle use_handle,void(*hook)(void*)){
	Music_Exec_Cmd(use_handle,MUSIC_SET_HOOK,hook);
}

//设置音频播放结束的钩子函数调用参数
//use_handle:句柄
//pdata:钩子函数参数
void Music_Set_UsePtr(Music_Handle use_handle,void* pdata){
	Music_Exec_Cmd(use_handle,MUSIC_SET_PTR,pdata);
}

void (*Music_Get_EndHook(Music_Handle use_handle))(void*){
	Music_Handle_Type* pHandle=use_handle;
	return pHandle==NULL?NULL:pHandle->end_hook;
}

void* Music_Get_UsePtr(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	return pHandle==NULL?NULL:pHandle->use_ptr;
}


//在解码器链表上注册一个解码器
//pDecoder:解码器
//返回值:true注册成功
//       false注册失败
bool Music_Decoder_Register(const Music_Decoder* pDecoder){
	bool res;
	Music_Func_Lock();
	res=Insert_Decoder(&Decoder_List,pDecoder);
	Music_Func_UnLock();
	return res;
}

//在解码器链表上移除一个解码器
//pDecoder:解码器
//返回值:true移除成功
//       false移除失败
bool Music_Decoder_UnRegister(const Music_Decoder* pDecoder){
	bool res;
	Music_Func_Lock();
	res=Remove_Decoder(&Decoder_List,pDecoder);
	Music_Func_UnLock();
	return res;
}

void Music_Decoder_Walk(Music_Decoder_Walker walk_func,void* token){
	Music_Func_Lock();
	TRAVERSE_MY_LIST(pItem,&Decoder_List){
		walk_func(pItem->data,token);
	}
	Music_Func_UnLock();
}

//判断音频是否暂停
//use_handle句柄
//返回值：true暂停
//		  false未暂停
bool Music_Is_Suspend(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	return pHandle==NULL||pHandle->is_suspend;
}

//判断音频是否播放完毕
//use_handle句柄
//返回值：true播放完毕
//		  false未播放完毕
bool Music_Is_End(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	return pHandle==NULL||pHandle->is_end;
}

//获取音频优先级
//use_handle句柄
//返回值：int播放优先级
int Music_Get_Priority(Music_Handle use_handle){
	Music_Handle_Type* pHandle=use_handle;
	return pHandle==NULL?0:pHandle->priority;
}

//获取音频总播放进度
//use_handle句柄
//返回值：uint32_t总播放进度
uint32_t Music_Get_TotalPos(Music_Handle use_handle){
	uint32_t total_pos=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_TOTALPOS,&total_pos);
	return total_pos;
}

//获取音频当前播放进度
//use_handle句柄
//返回值：uint32_t当前播放进度
uint32_t Music_Get_NowPos(Music_Handle use_handle){
	uint32_t now_pos=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_NOWPOS,&now_pos);
	return now_pos;
}

//设置音频播放进度
//use_handle：句柄
//pos：设置的播放进度
//返回值：true设置成功
//		  false设置失败
bool Music_Set_Pos(Music_Handle use_handle,uint32_t pos){
	return Music_Exec_Cmd(use_handle,MUSIC_SET_POS,&pos);
}

//获取音频总播放时长，单位：秒
//use_handle句柄
//返回值：uint32_t总秒数
uint32_t Music_Get_TotalSec(Music_Handle use_handle){
	uint32_t total_sec=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_TOTALSEC,&total_sec);
	return total_sec;
}

//设置音频播放优先级
//use_handle：句柄
//priority：设置的优先级
//返回值：true设置成功
//		  false设置失败
bool Music_Set_Priority(Music_Handle use_handle,int priority){
	return Music_Exec_Cmd(use_handle,MUSIC_SET_PRIORITY,&priority);
}

//获取音频缓冲中的数据
//use_handle：句柄
//pBuf:音频缓冲，pBuf->buf_addr为获取数据的缓冲区首地址,pBuf->buf_size为
//		缓冲区长度，pBuf->data_size为数据项的格式（自动获取），函数执行完毕后
//		pBuf->buf_size代表实际获取的数据数量
bool Music_Get_Buf_Data(Music_Handle use_handle,Music_BufType* pBuf){
	return Music_Exec_Cmd(use_handle,MUSIC_GET_BUF_DATA,pBuf);
}


//获取单个音频缓冲区长度
//use_handle：句柄
//返回值：uint32_t 缓冲区长度
uint32_t Music_Get_Buf_Size(Music_Handle use_handle){
	uint32_t buf_size=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_BUF_SIZE,&buf_size);
	return buf_size;
}

//获取音频采样深度,16位或24位，24位音频扩展为32位高8位为0
//use_handle：句柄
//返回值：uint32_t 采样深度
uint32_t Music_Get_Data_Size(Music_Handle use_handle){
	uint32_t data_size=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_DATA_SIZE,&data_size);
	return data_size;
}

//获取音频采样率
//use_handle：句柄
//返回值：uint32_t 采样率
uint32_t Music_Get_Samplate(Music_Handle use_handle){
	uint32_t samplate=0;
	Music_Exec_Cmd(use_handle,MUSIC_GET_SAMPLATE,&samplate);
	return samplate;
}


uint32_t Music_Get_FillBuf_Size(Music_Handle use_handle){
	uint32_t data_size=Music_Get_Data_Size(use_handle);
	switch(data_size){
	case 16:
		return 	Music_Get_Buf_Size(use_handle)*2;
	case 24:
		return 	Music_Get_Buf_Size(use_handle);
	case 32:
		return 	Music_Get_Buf_Size(use_handle);
	default:
		return 0;
	}
}

//设置耳机音量
//l_vol：左耳
//r_vol：右耳
void Music_Set_HP_Vol(uint8_t l_vol,uint8_t r_vol){
	Music_Func_Lock();
	if(l_vol>63)
		l_vol=63;
	if(r_vol>63)
		r_vol=63;
	Common_Setting.hp_l_volume=l_vol;
	Common_Setting.hp_r_volume=r_vol;
	HW_Muisc_Set_HP_Vol(l_vol,r_vol);
	Music_Func_UnLock();
}

//设置喇叭音量
//vol:音量
void Music_Set_SPK_Vol(uint8_t vol){
	Music_Func_Lock();
	if(vol>63)
		vol=63;
	Common_Setting.spk_volume=vol;
	HW_Music_Set_SPK_Vol(vol);
	Music_Func_UnLock();
}

//设置全局音量增益
//gain:增益，单位db
void Music_Set_Volume_Gain(int16_t gain){
	Music_Func_Lock();
	if(gain>MUSIC_GAIN_MAX)
		gain=MUSIC_GAIN_MAX;
	if(gain<MUSIC_GAIN_MIN)
		gain=MUSIC_GAIN_MIN;
	Common_Setting.soft_volume_gain=gain;
	Music_Func_UnLock();
}


//初始化音乐设备
bool Music_Device_Init(void){
	My_List_Init(&Decoder_List);
	My_List_Init(&Music_Ready_List);
	svc_lib_init(SVC_Persistent_Mem,SVC_Scratch_Mem);
	Music_Func_Lock_ID=osMutexNew(&Music_Func_Lock_Attr);
	Music_Task_ID=osThreadNew(Music_Task,NULL,&Music_Task_Attr);
	Music_Message_Queue_ID=osMessageQueueNew(MUSIC_MESS_MAX_NUM,sizeof(Music_Message*),&Music_Message_Attr);
	Music_Hook_Task_ID=osThreadNew(Music_End_Hook_Task_Func,NULL,&Music_Hook_Task_Attr);
	Music_Hook_Message_Queue_ID=osMessageQueueNew(MUSIC_HOOK_TASK_MES_NUM,sizeof(Music_End_Hook_Message),&Music_Hook_Message_Attr);
	if(Music_Message_Queue_ID==NULL||\
		Music_Task_ID==NULL||\
		Music_Func_Lock_ID==NULL||\
		Music_Hook_Task_ID==NULL||\
		Music_Hook_Message_Queue_ID==NULL)
		return false;
	if(HW_Music_Init()==false)
		return false;
	Music_Set_HP_Vol(Common_Setting.hp_l_volume,Common_Setting.hp_r_volume);
	Music_Set_SPK_Vol(Common_Setting.spk_volume);
	return true;
}
