/*

libdemac - A Monkey's Audio decoder

$Id: decoder.c 28632 2010-11-21 17:58:42Z Buschel $

Copyright (C) Dave Chapman 2007

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA

*/

#include <inttypes.h>
#include <string.h>
#include "music_device.h"
#include "parser.h"
#include "predictor.h"
#include "entropy.h"
#include "filter.h"
#include "demac_config.h"
#include <stdlib.h>
#include <stdio.h>
#include "ff.h"

#define APE_FILE_BUF_SZ    			8*1024	//APE解码时,文件buf大小
#define APE_BLOCKS_PER_LOOP			512		//APE解码时,每个循环解码block的个数
#define INPUT_CHUNKSIZE     		(32*1024)

#define FILTER_BUF_64_SIZE		(((64*3 + FILTER_HISTORY_SIZE) * 2)*sizeof(filter_int))
#define FILTER_BUF_256_SIZE		(((256*3 + FILTER_HISTORY_SIZE) * 2)*sizeof(filter_int))
#define FILTER_BUF_1280_SIZE	(((1280*3 + FILTER_HISTORY_SIZE) * 2)*sizeof(filter_int))
#define FILTER_BUF_ALGEN_BYTE	16

typedef struct filter_buf{
	struct{
		struct filter_t filter[2];
		filter_int* filter_buf;
	}filterbuf64,filterbuf256,filterbuf1280;
}filter_buf;

//APE控制结构体
typedef struct { 
    uint32_t totsec ;				//整首歌时长,单位:秒
	uint32_t samplerate;			//采样率
	uint32_t bps;					//位数,比如16bit,24bit,32bit
	uint32_t datastart;				//数据帧开始的位置(在文件里面的偏移)
}__apectrl;

typedef struct{
	filter_buf f_buf;
	int decode_buf[APE_BLOCKS_PER_LOOP];
	__apectrl ctrl;
	uint8_t file_buf[APE_FILE_BUF_SZ];
	FIL file;
	struct ape_ctx_t ape_ctx;
	int first_byte;
	uint32_t curruent_frame;
	int nblocks;
	uint8_t* read_ptr;
	uint32_t bytes_left;
	uint32_t sample_skip;
}APE_Handle;

/* Statically allocate the filter buffers */

#ifdef FILTER256_IRAM
static filter_int filterbuf32[(32*3 + FILTER_HISTORY_SIZE) * 2]   
                  IBSS_ATTR_DEMAC MEM_ALIGN_ATTR; 
                  /* 2432 or 4864 bytes */
static filter_int filterbuf256[(256*3 + FILTER_HISTORY_SIZE) * 2]
                  IBSS_ATTR_DEMAC MEM_ALIGN_ATTR; 
                  /* 5120 or 10240 bytes */
#define FILTERBUF64 filterbuf256
#define FILTERBUF32 filterbuf32
#define FILTERBUF16 filterbuf32
#else
//static filter_int filterbuf64[(64*3 + FILTER_HISTORY_SIZE) * 2]   
//                  IBSS_ATTR_DEMAC MEM_ALIGN_ATTR; 
//                  /* 2816 or 5632 bytes */
//static filter_int filterbuf256[(256*3 + FILTER_HISTORY_SIZE) * 2]
//                  MEM_ALIGN_ATTR; /* 5120 or 10240 bytes */

//#define FILTERBUF64 f_buf->filterbuf64
//#define FILTERBUF32 f_buf->filterbuf64
//#define FILTERBUF16 f_buf->filterbuf64
#endif



///* This is only needed for "insane" files, and no current Rockbox targets
//   can hope to decode them in realtime, except the Gigabeat S (at 528MHz). */
//static filter_int filterbuf1280[(1280*3 + FILTER_HISTORY_SIZE) * 2] 
//                  IBSS_ATTR_DEMAC_INSANEBUF MEM_ALIGN_ATTR;
//                  /* 17408 or 34816 bytes */
extern void *aligned_alloc(size_t align_byte,size_t size);
static int init_frame_decoder(struct ape_ctx_t* ape_ctx,
                        unsigned char* inbuffer, int* firstbyte,
                        int* bytesconsumed,struct filter_buf* f_buf)
{
    init_entropy_decoder(ape_ctx, inbuffer, firstbyte, bytesconsumed);
    init_predictor_decoder(&ape_ctx->predictor);
	
    switch (ape_ctx->compressiontype)
    {
        case 2000:
			if(f_buf->filterbuf64.filter_buf==NULL){
				f_buf->filterbuf64.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_64_SIZE);
				if(f_buf->filterbuf64.filter_buf==NULL)
					goto fail;
			}
            init_filter_16_11(f_buf->filterbuf64.filter,f_buf->filterbuf64.filter_buf);
			free(f_buf->filterbuf256.filter_buf);
			free(f_buf->filterbuf1280.filter_buf);
			f_buf->filterbuf256.filter_buf=NULL;
			f_buf->filterbuf1280.filter_buf=NULL;
            break;
        case 3000:
			if(f_buf->filterbuf64.filter_buf==NULL){
				f_buf->filterbuf64.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_64_SIZE);
				if(f_buf->filterbuf64.filter_buf==NULL)
					goto fail;
			}
            init_filter_64_11(f_buf->filterbuf64.filter,f_buf->filterbuf64.filter_buf);
			free(f_buf->filterbuf256.filter_buf);
			free(f_buf->filterbuf1280.filter_buf);
			f_buf->filterbuf256.filter_buf=NULL;
			f_buf->filterbuf1280.filter_buf=NULL;
            break;
        case 4000:
			if(f_buf->filterbuf64.filter_buf==NULL){
				f_buf->filterbuf64.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_64_SIZE);
				if(f_buf->filterbuf64.filter_buf==NULL)
					goto fail;
			}
			if(f_buf->filterbuf256.filter_buf==NULL){
				f_buf->filterbuf256.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_256_SIZE);
				if(f_buf->filterbuf256.filter_buf==NULL)
					goto fail;
			}
            init_filter_256_13(f_buf->filterbuf256.filter,f_buf->filterbuf256.filter_buf);
            init_filter_32_10(f_buf->filterbuf64.filter,f_buf->filterbuf64.filter_buf);
			free(f_buf->filterbuf1280.filter_buf);
			f_buf->filterbuf1280.filter_buf=NULL;
            break;
        case 5000:
			if(f_buf->filterbuf64.filter_buf==NULL){
				f_buf->filterbuf64.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_64_SIZE);
				if(f_buf->filterbuf64.filter_buf==NULL)
					goto fail;
			}
			if(f_buf->filterbuf256.filter_buf==NULL){
				f_buf->filterbuf256.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_256_SIZE);
				if(f_buf->filterbuf256.filter_buf==NULL)
					goto fail;
			}
			if(f_buf->filterbuf1280.filter_buf==NULL){
				f_buf->filterbuf1280.filter_buf=aligned_alloc(FILTER_BUF_ALGEN_BYTE,FILTER_BUF_1280_SIZE);
				if(f_buf->filterbuf1280.filter_buf==NULL)
					goto fail;
			}
            init_filter_1280_15(f_buf->filterbuf1280.filter,f_buf->filterbuf1280.filter_buf);
            init_filter_256_13(f_buf->filterbuf256.filter,f_buf->filterbuf256.filter_buf);
            init_filter_16_11(f_buf->filterbuf64.filter,f_buf->filterbuf64.filter_buf);
			break;
		default:
			goto fail;
    }
	return 0;
fail:	
	free(f_buf->filterbuf64.filter_buf);
	free(f_buf->filterbuf256.filter_buf);
	free(f_buf->filterbuf1280.filter_buf);
	return -1;
}

static int  decode_chunk(struct ape_ctx_t* ape_ctx,
                                  unsigned char* inbuffer, int* firstbyte,
                                  int* bytesconsumed,
                                  int32_t* decoded0, int32_t* decoded1,
                                  int count,struct filter_buf* f_buf)
{
    uint16_t left;
	uint16_t *abuf=(uint16_t*)decoded1;//利用decode1作音频输出缓冲
	
    if ((ape_ctx->channels==1) || ((ape_ctx->frameflags
        & (APE_FRAMECODE_PSEUDO_STEREO|APE_FRAMECODE_STEREO_SILENCE))
        == APE_FRAMECODE_PSEUDO_STEREO)) {

        entropy_decode(ape_ctx, inbuffer, firstbyte, bytesconsumed,
                       decoded0, NULL, count);

        if (ape_ctx->frameflags & APE_FRAMECODE_MONO_SILENCE) {
            /* We are pure silence, so we're done. */
            return 0;
        }

        switch (ape_ctx->compressiontype)
        {
            case 2000:
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                break;
    
            case 3000:
                apply_filter_64_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                break;
            case 4000:
                apply_filter_32_10(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,0,decoded0,count);
                break;
            case 5000:
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_1280_15(f_buf->filterbuf1280.filter,ape_ctx->fileversion,0,decoded0,count);
				break;
        }

        /* Now apply the predictor decoding */
        predictor_decode_mono(&ape_ctx->predictor,decoded0,count);
		//单声道也做立体声处理
        //if (ape_ctx->channels==2) 
		{
            /* Pseudo-stereo - copy left channel to right channel */
            while (count--)
            {
				*(abuf++)=*decoded0;
				*(abuf++)=*(decoded0++); 
            }
        } 
    } else { /* Stereo */
        entropy_decode(ape_ctx, inbuffer, firstbyte, bytesconsumed,
                       decoded0, decoded1, count);

        if ((ape_ctx->frameflags & APE_FRAMECODE_STEREO_SILENCE)
            == APE_FRAMECODE_STEREO_SILENCE) {
            /* We are pure silence, so we're done. */
            return 0;
        }

        /* Apply filters - compression type 1000 doesn't have any */
        switch (ape_ctx->compressiontype)
        {
            case 2000:
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,1,decoded1,count);
                break;
    
            case 3000:
                apply_filter_64_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_64_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,1,decoded1,count);
                break;

            case 4000:
                apply_filter_32_10(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_32_10(f_buf->filterbuf64.filter,ape_ctx->fileversion,1,decoded1,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,1,decoded1,count);
                break;
    
            case 5000:
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_16_11(f_buf->filterbuf64.filter,ape_ctx->fileversion,1,decoded1,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_256_13(f_buf->filterbuf256.filter,ape_ctx->fileversion,1,decoded1,count);
                apply_filter_1280_15(f_buf->filterbuf1280.filter,ape_ctx->fileversion,0,decoded0,count);
                apply_filter_1280_15(f_buf->filterbuf1280.filter,ape_ctx->fileversion,1,decoded1,count);
				break;
        }

        /* Now apply the predictor decoding */
        predictor_decode_stereo(&ape_ctx->predictor,decoded0,decoded1,count);

        /* Decorrelate and scale to output depth */
        while (count--)
        {
            left = *(decoded1++) - (*decoded0 / 2);
			*(abuf++)=left;
            *(abuf++)=left+*(decoded0++); 
        }
    }
    return 0;
}

static bool Ape_Frame_Decoder_Init(APE_Handle* pHandle){
	int decode_len;
	if(pHandle->curruent_frame==(pHandle->ape_ctx.totalframes-1))
		pHandle->nblocks=pHandle->ape_ctx.finalframeblocks;
	else 
		pHandle->nblocks=pHandle->ape_ctx.blocksperframe; 
	pHandle->ape_ctx.currentframeblocks=pHandle->nblocks;
	pHandle->curruent_frame++;		
	if(init_frame_decoder(&pHandle->ape_ctx,pHandle->read_ptr,&pHandle->first_byte,&decode_len,&pHandle->f_buf)<0)
		return false;
	if(decode_len<0)
		return false;
	pHandle->read_ptr+=decode_len;
	pHandle->bytes_left-=decode_len;
	return true;
}

/* Given an ape_ctx and a sample to seek to, return the file position
   to the frame containing that sample, and the number of samples to
   skip in that frame.
*/

static bool ape_calc_seekpos(struct ape_ctx_t* ape_ctx,
                             uint32_t new_sample,
                             uint32_t* newframe,
                             uint32_t* filepos,
                             uint32_t* samplestoskip)
{
    uint32_t n;

    n = new_sample / ape_ctx->blocksperframe;
    if (n >= ape_ctx->numseekpoints)
    {
        /* We don't have a seekpoint for that frame */
        return false;
    }

    *newframe = n;
    *filepos = ape_ctx->seektable[n];
    *samplestoskip = new_sample - (n * ape_ctx->blocksperframe);

    return true;
}

static inline uint32_t ape_min(uint32_t a,uint32_t b){
	return a>b?b:a;
}

static Music_Decoder_Handle Ape_Create(const char* file_name,void* token){
	APE_Handle* pHandle=malloc(sizeof(APE_Handle));
	uint32_t totalsamples;
	UINT br;
	
	if(pHandle==NULL){
		goto fail0;
	}
	
	pHandle->f_buf.filterbuf64.filter_buf=NULL;
	pHandle->f_buf.filterbuf256.filter_buf=NULL;
	pHandle->f_buf.filterbuf1280.filter_buf=NULL;
	memset(&pHandle->ape_ctx,0,sizeof(pHandle->ape_ctx));
	
	if(f_open(&pHandle->file,file_name,FA_READ)!=FR_OK){
		goto fail0;
	}
	
	if(ape_parseheader(&pHandle->file,&pHandle->ape_ctx)!=0){
		printf("Ape文件解码失败\r\n");
		goto fail1;
	}
	
	if(pHandle->ape_ctx.fileversion<APE_MIN_VERSION||pHandle->ape_ctx.fileversion>APE_MAX_VERSION){
		printf("Ape文件版本不支持\r\n");
		goto fail1;
	}
	
	if(pHandle->ape_ctx.bps!=16){//该库对其他音频位数播放有问题
		printf("Ape文件采样率不支持\r\n");
		goto fail1;
	}
	
	pHandle->ctrl.bps=pHandle->ape_ctx.bps;					//得到采样深度(ape,我们仅支持16位)
	pHandle->ctrl.samplerate=pHandle->ape_ctx.samplerate;	
	if(pHandle->ape_ctx.totalframes>1)
		totalsamples=pHandle->ape_ctx.finalframeblocks+pHandle->ape_ctx.blocksperframe*(pHandle->ape_ctx.totalframes-1);
	else 
		totalsamples=pHandle->ape_ctx.finalframeblocks;
	pHandle->ctrl.totsec=totalsamples/pHandle->ctrl.samplerate;//得到文件总时长 
	pHandle->ctrl.datastart=pHandle->ape_ctx.firstframe;	//得到第一帧的地址	 
	
	pHandle->ape_ctx.numseekpoints=ape_min(pHandle->ape_ctx.totalframes,pHandle->ape_ctx.seektablelength/sizeof(uint32_t));
	if(pHandle->ape_ctx.numseekpoints!=0){
		pHandle->ape_ctx.seektable=malloc(sizeof(uint32_t)*pHandle->ape_ctx.numseekpoints);
		if(pHandle->ape_ctx.seektable==NULL){
			goto fail1;
		}
	}
	else
		pHandle->ape_ctx.seektable=NULL;
	
	
	if(f_lseek(&pHandle->file,pHandle->ape_ctx.seektablefilepos)!=FR_OK){
		goto fail2;
	}
	
	if(f_read(&pHandle->file,pHandle->ape_ctx.seektable,sizeof(uint32_t)*pHandle->ape_ctx.numseekpoints,&br)!=FR_OK){
		goto fail2;
	}
	
	if(f_lseek(&pHandle->file,pHandle->ctrl.datastart)!=FR_OK){
		goto fail2;
	}
	
	if(f_read(&pHandle->file,pHandle->file_buf,APE_FILE_BUF_SZ,&br)!=FR_OK){
		goto fail2;
	}
	
	pHandle->first_byte=3;
	pHandle->curruent_frame=0;
	pHandle->read_ptr=pHandle->file_buf;
	pHandle->bytes_left=br;
	pHandle->sample_skip=0;
	if(Ape_Frame_Decoder_Init(pHandle)==false){
		goto fail1;
	}

	return pHandle;
fail2:
	free(pHandle->ape_ctx.seektable);
fail1:
	f_close(&pHandle->file);
fail0:
	free(pHandle);
	return NULL;
}

static void Ape_Delete(Music_Decoder_Handle use_handle){
	APE_Handle* pHandle=use_handle;
	
	free(pHandle->ape_ctx.seektable);
	free(pHandle->f_buf.filterbuf64.filter_buf);
	free(pHandle->f_buf.filterbuf256.filter_buf);
	free(pHandle->f_buf.filterbuf1280.filter_buf);
	f_close(&pHandle->file);
	free(pHandle);
}

static bool Ape_Fill_Target_Block(Music_Decoder_Handle use_handle,void* buf,uint32_t fill_block_num){
	APE_Handle* pHandle=use_handle;
	int decode_len;
	int need_block;
	int *pbuf=buf;
	
refill:	
	if(pHandle->curruent_frame>=pHandle->ape_ctx.totalframes){
		goto fail0;
	}
	
	if(pHandle->nblocks<=0){
		if(Ape_Frame_Decoder_Init(pHandle)==false){
			goto fail0;
		}
	}
	
	need_block=ape_min(pHandle->nblocks ,fill_block_num);
	
	if(decode_chunk(&pHandle->ape_ctx,
					pHandle->read_ptr,
					&pHandle->first_byte,
					&decode_len,
					pHandle->decode_buf,
					pbuf,
					need_block,
					&pHandle->f_buf)!=0)
	{
		goto fail0;
	}
	if(decode_len<0){
		goto fail0;
	}
	
	pHandle->read_ptr+=decode_len;
	pHandle->bytes_left-=decode_len;
	
	if(pHandle->bytes_left<0||decode_len>4*APE_BLOCKS_PER_LOOP){
		goto fail0;
	}
	if(pHandle->bytes_left<4*APE_BLOCKS_PER_LOOP){
		UINT br;
		memmove(pHandle->file_buf,pHandle->read_ptr,pHandle->bytes_left);
		pHandle->read_ptr=pHandle->file_buf;
		if(f_read(&pHandle->file,&pHandle->file_buf[pHandle->bytes_left],APE_FILE_BUF_SZ-pHandle->bytes_left,&br)!=FR_OK){
			goto fail0;
		}
		pHandle->bytes_left+=br;
	}
	pHandle->nblocks-=need_block;
	fill_block_num-=need_block;
	pbuf+=need_block;
	
	if(fill_block_num!=0){
		goto refill;
	}
	return true;
fail0:
	memset(pbuf,0,4*(APE_BLOCKS_PER_LOOP-(pbuf-(int*)buf)));
	return false;
}

static bool Ape_Fill(Music_Decoder_Handle use_handle,void* buf){
	APE_Handle* pHandle=use_handle;
	bool res;
	while(pHandle->sample_skip!=0){
		uint32_t sample_fill=ape_min(pHandle->sample_skip,APE_BLOCKS_PER_LOOP);
		res=Ape_Fill_Target_Block(use_handle,buf,sample_fill);
		if(res==false){
			return false;
		}
		pHandle->sample_skip-=sample_fill;
	}
	return Ape_Fill_Target_Block(use_handle,buf,APE_BLOCKS_PER_LOOP);
}

static uint32_t Ape_GetMaxBuf(Music_Decoder_Handle use_handle){
	return APE_BLOCKS_PER_LOOP*4;
}

static uint32_t Ape_GetDataSize(Music_Decoder_Handle use_handle){
	return 16;
}

static uint32_t Ape_GetNowPos(Music_Decoder_Handle use_handle){
	APE_Handle* pHandle=use_handle;
	return ((pHandle->curruent_frame==0?0:pHandle->curruent_frame-1)*pHandle->ape_ctx.blocksperframe+pHandle->ape_ctx.blocksdecoded)/APE_BLOCKS_PER_LOOP;
}

static uint32_t Ape_GetSamlate(Music_Decoder_Handle use_handle){
	APE_Handle* pHandle=use_handle;
	return pHandle->ctrl.samplerate;
}

static uint32_t Ape_GetTotalPos(Music_Decoder_Handle use_handle){
	APE_Handle* pHandle=use_handle;
	return ((pHandle->ape_ctx.totalframes-1)*pHandle->ape_ctx.blocksperframe+pHandle->ape_ctx.finalframeblocks)/APE_BLOCKS_PER_LOOP;
}

static uint32_t Ape_GetTotalSec(Music_Decoder_Handle use_handle){
	APE_Handle* pHandle=use_handle;
	return pHandle->ctrl.totsec;
}

static bool Ape_SetPos(Music_Decoder_Handle use_handle,uint32_t pos){
	APE_Handle* pHandle=use_handle;
#if 0
	if(pHandle->ape_ctx.seektable!=NULL){
		uint32_t file_pos;
		pos*=APE_BLOCKS_PER_LOOP;
		if(ape_calc_seekpos(&pHandle->ape_ctx,pos,&pHandle->curruent_frame,&file_pos,&pHandle->sample_skip)==false){
			return false;
		}
		/* APE's bytestream is weird... */
		pHandle->first_byte = 3 - (file_pos & 3);
        file_pos &= ~3;
		if(f_lseek(&pHandle->file,file_pos)==false){
			return false;
		}
		if(f_read(&pHandle->file,pHandle->file_buf,APE_FILE_BUF_SZ,&pHandle->bytes_left)!=FR_OK){
			return false;
		}
		pHandle->read_ptr=pHandle->file_buf;
		pHandle->nblocks=0;
		return true;
	}
#endif
	return false;
}

static const char* const file_surport[]={
	"ape",NULL
};

const Music_Decoder Ape_Decoder_Device={
	.pCreate=Ape_Create,
	.pDelete=Ape_Delete,
	.pFillBuf=Ape_Fill,
	.pGetBufMax=Ape_GetMaxBuf,
	.pGetDataSize=Ape_GetDataSize,
	.pGetNowPos=Ape_GetNowPos,
	.pGetSamplate=Ape_GetSamlate,
	.pGetTotalPos=Ape_GetTotalPos,
	.pGetTotalSec=Ape_GetTotalSec,
	.pSetPos=Ape_SetPos,
	.surport_file=file_surport
};	






















