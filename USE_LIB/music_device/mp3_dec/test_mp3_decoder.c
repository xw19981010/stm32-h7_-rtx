#include "mp3_decoder.h"
#include "helix/mp3dec.h"
#include "fatfs.h"

#define MP3_TITSIZE_MAX		40		//歌曲名字最大长度
#define MP3_ARTSIZE_MAX		40		//歌曲名字最大长度
#define MP3_FILE_BUF_SZ    5*1024	//MP3解码时,文件buf大小
#define MP3_FILL_SIZE		(2*2304)

//ID3V1 标签 
typedef __PACKED_STRUCT 
{
    uint8_t id[3];		   	//ID,TAG三个字母
    uint8_t title[30];		//歌曲名字
    uint8_t artist[30];		//艺术家名字
	uint8_t year[4];			//年代
	uint8_t comment[30];		//备注
	uint8_t genre;			//流派 
}ID3V1_Tag;

//ID3V2 标签头 
typedef __PACKED_STRUCT
{
    uint8_t id[3];		   	//ID
    uint8_t mversion;		//主版本号
    uint8_t sversion;		//子版本号
    uint8_t flags;			//标签头标志
    uint8_t size[4];			//标签信息大小(不包含标签头10字节).所以,标签大小=size+10.
}ID3V2_TagHead;

//ID3V2.3 版本帧头
typedef __PACKED_STRUCT
{
    uint8_t id[4];		   	//帧ID
    uint8_t size[4];			//帧大小
    uint16_t flags;			//帧标志
}ID3V23_FrameHead;

//MP3 Xing帧信息(没有全部列出来,仅列出有用的部分)
typedef __PACKED_STRUCT 
{
    uint8_t id[4];		   	//帧ID,为Xing/Info
    uint8_t flags[4];		//存放标志
    uint8_t frames[4];		//总帧数
	uint8_t fsize[4];		//文件总大小(不包含ID3)
}MP3_FrameXing;
 
//MP3 VBRI帧信息(没有全部列出来,仅列出有用的部分)
typedef __PACKED_STRUCT 
{
    uint8_t id[4];		   	//帧ID,为Xing/Info
	uint8_t version[2];		//版本号
	uint8_t delay[2];		//延迟
	uint8_t quality[2];		//音频质量,0~100,越大质量越好
	uint8_t fsize[4];		//文件总大小
	uint8_t frames[4];		//文件总帧数 
}MP3_FrameVBRI;


//MP3控制结构体
typedef struct
{
    uint8_t title[MP3_TITSIZE_MAX];	//歌曲名字
    uint8_t artist[MP3_ARTSIZE_MAX];	//艺术家名字
    uint32_t totsec ;				//整首歌时长,单位:秒
    uint32_t cursec ;				//当前播放时长
	
    uint32_t bitrate;	   			//比特率
	uint32_t samplerate;				//采样率
	uint16_t outsamples;				//PCM输出数据量大小(以16位为单位),单声道MP3,则等于实际输出*2(方便DAC输出)
	
	uint32_t datastart;				//数据帧开始的位置(在文件里面的偏移)
}__mp3ctrl;

typedef struct{
	__mp3ctrl ctrl;
	uint8_t file_buf[MP3_FILE_BUF_SZ];
	uint8_t* read_ptr;
	uint32_t read_byte;
	FIL file;
	HMP3Decoder pDecoder;
	MP3FrameInfo frame_info;
	
}Mp3_Handle;

#define MIN(a,b) ((a)>(b)?(b):(a))

//解析ID3V1 
//buf:输入数据缓存区(大小固定是128字节)
//pctrl:MP3控制器
//返回值:0,获取正常
//    其他,获取失败
static bool mp3_id3v1_decode(uint8_t* buf,__mp3ctrl *pctrl)
{
	ID3V1_Tag *id3v1tag;
	id3v1tag=(ID3V1_Tag*)buf;
	if (strncmp("TAG",(char*)id3v1tag->id,3)==0){//是MP3 ID3V1 TAG
		if(id3v1tag->title[0])
			strncpy((char*)pctrl->title,(char*)id3v1tag->title,30);
		if(id3v1tag->artist[0])
			strncpy((char*)pctrl->artist,(char*)id3v1tag->artist,30); 
	}
	else 
		return false;
	return true;
}

//解析ID3V2 
//buf:输入数据缓存区
//size:数据大小
//pctrl:MP3控制器
//返回值:0,获取正常
//    其他,获取失败
static bool mp3_id3v2_decode(uint8_t* buf,uint32_t size,__mp3ctrl *pctrl)
{
	ID3V2_TagHead *taghead;
	ID3V23_FrameHead *framehead; 
	uint32_t t;
	uint32_t tagsize;	//tag大小
	uint32_t frame_size;	//帧大小 
	taghead=(ID3V2_TagHead*)buf; 
	if(strncmp("ID3",(const char*)taghead->id,3)==0)//存在ID3?
	{
		tagsize=((uint32_t)taghead->size[0]<<21)|((uint32_t)taghead->size[1]<<14)|((uint16_t)taghead->size[2]<<7)|taghead->size[3];//得到tag 大小
		pctrl->datastart=tagsize;		//得到mp3数据开始的偏移量
		if(tagsize>size)tagsize=size;	//tagsize大于输入bufsize的时候,只处理输入size大小的数据
		if(taghead->mversion<3)
		{
			printf("not supported mversion!\r\n");
			return false;
		}
		t=10;
		while(t<tagsize)
		{
			framehead=(ID3V23_FrameHead*)(buf+t);
			frame_size=((uint32_t)framehead->size[0]<<24)|((uint32_t)framehead->size[1]<<16)|((uint32_t)framehead->size[2]<<8)|framehead->size[3];//得到帧大小
 			if (strncmp("TT2",(char*)framehead->id,3)==0||strncmp("TIT2",(char*)framehead->id,4)==0)//找到歌曲标题帧,不支持unicode格式!!
			{
				strncpy((char*)pctrl->title,(char*)(buf+t+sizeof(ID3V23_FrameHead)+1),MIN(frame_size-1,MP3_TITSIZE_MAX-1));
			}
 			if (strncmp("TP1",(char*)framehead->id,3)==0||strncmp("TPE1",(char*)framehead->id,4)==0)//找到歌曲艺术家帧
			{
				strncpy((char*)pctrl->artist,(char*)(buf+t+sizeof(ID3V23_FrameHead)+1),MIN(frame_size-1,MP3_ARTSIZE_MAX-1));
			}
			t+=frame_size+sizeof(ID3V23_FrameHead);
		} 
	}else pctrl->datastart=0;//不存在ID3,mp3数据是从0开始
	return true;
} 


//获取MP3基本信息
//pname:MP3文件路径
//pctrl:MP3控制信息结构体 
//返回值:0,成功
//    其他,失败
static bool mp3_get_info(const char *pname,__mp3ctrl* pctrl)
{
    HMP3Decoder decoder;
    MP3FrameInfo frame_info;
	MP3_FrameXing* fxing;
	MP3_FrameVBRI* fvbri;
	FIL *fmp3;
	uint8_t *buf;
	uint32_t br;
	uint8_t res;
	int offset=0;
	uint32_t p;
	short samples_per_frame;	//一帧的采样个数
	uint32_t totframes;			//总帧数
	
	fmp3=malloc(sizeof(FIL)); 
	buf=malloc(5*1024);		//申请5K内存 
	decoder=MP3InitDecoder(); 		//MP3解码申请内存
	if(fmp3==NULL||buf==NULL||decoder==NULL){
		goto fail0;
	}
	
		
	if(f_open(fmp3,pname,FA_READ)!=FR_OK){
		goto fail0;
	}
	if(f_read(fmp3,buf,5*1024,&br)!=FR_OK){
		goto fail1;
	}
	
	if(mp3_id3v2_decode(buf,br,pctrl)==false){	//解析ID3V2数据
		goto fail1;
	}
	if(f_lseek(fmp3,f_size(fmp3)-128)!=FR_OK){	//偏移到倒数128的位置
		goto fail1;
	}
	if(f_read(fmp3,buf,128,&br)!=FR_OK){//读取128字节
		goto fail1;
	}
	if(mp3_id3v1_decode(buf,pctrl)==false){	//解析ID3V1数据
		goto fail1;		
	}
	if(f_lseek(fmp3,pctrl->datastart)!=FR_OK){	//偏移到数据开始的地方
		goto fail1;	
	}
	if(f_read(fmp3,(char*)buf,5*1024,&br)!=FR_OK){	//读取5K字节mp3数据
		goto fail1;	
	}
	offset=MP3FindSyncWord(buf,br);	//查找帧同步信息
	if(offset>=0&&MP3GetNextFrameInfo(decoder,&frame_info,&buf[offset])==0)//找到帧同步信息了,且下一阵信息获取正常	
	{ 
		p=offset+4+32;
		fvbri=(MP3_FrameVBRI*)(buf+p);
		if(strncmp("VBRI",(char*)fvbri->id,4)==0)//存在VBRI帧(VBR格式)
		{
			if (frame_info.version==MPEG1)
				samples_per_frame=1152;//MPEG1,layer3每帧采样数等于1152
			else 
				samples_per_frame=576;//MPEG2/MPEG2.5,layer3每帧采样数等于576 
			totframes=((uint32_t)fvbri->frames[0]<<24)|((uint32_t)fvbri->frames[1]<<16)|((uint16_t)fvbri->frames[2]<<8)|fvbri->frames[3];//得到总帧数
			pctrl->totsec=totframes*samples_per_frame/frame_info.samprate;//得到文件总长度
		}else	//不是VBRI帧,尝试是不是Xing帧(VBR格式)
		{  
			if (frame_info.version==MPEG1)	//MPEG1 
			{
				p=frame_info.nChans==2?32:17;
				samples_per_frame = 1152;	//MPEG1,layer3每帧采样数等于1152
			}else
			{
				p=frame_info.nChans==2?17:9;
				samples_per_frame=576;		//MPEG2/MPEG2.5,layer3每帧采样数等于576
			}
			p+=offset+4;
			fxing=(MP3_FrameXing*)(buf+p);
			if(strncmp("Xing",(char*)fxing->id,4)==0||strncmp("Info",(char*)fxing->id,4)==0)//是Xng帧
			{
				if(fxing->flags[3]&0X01)//存在总frame字段
				{
					totframes=((uint32_t)fxing->frames[0]<<24)|((uint32_t)fxing->frames[1]<<16)|((uint16_t)fxing->frames[2]<<8)|fxing->frames[3];//得到总帧数
					pctrl->totsec=totframes*samples_per_frame/frame_info.samprate;//得到文件总长度
				}else	//不存在总frames字段
				{
					pctrl->totsec=f_size(fmp3)/(frame_info.bitrate/8);
				} 
			}else 		//CBR格式,直接计算总播放时间
			{
				pctrl->totsec=f_size(fmp3)/(frame_info.bitrate/8);
			}
		} 
		pctrl->bitrate=frame_info.bitrate;			//得到当前帧的码率
		pctrl->samplerate=frame_info.samprate; 		//得到采样率. 
		if(frame_info.nChans==2)
			pctrl->outsamples=frame_info.outputSamps; //输出PCM数据量大小 
		else 
			pctrl->outsamples=frame_info.outputSamps*2; //输出PCM数据量大小,对于单声道MP3,直接*2,补齐为双声道输出
	}
	else 
		goto fail1;	
	f_close(fmp3);
	free(fmp3);
	free(buf);	
	MP3FreeDecoder(decoder);//释放内存	
	return true;	
fail1:
	f_close(fmp3);
fail0:
	free(fmp3);
	free(buf);	
	MP3FreeDecoder(decoder);//释放内存	
	return false;	
}  

static Music_Decoder_Handle Mp3_Create(const char* name,void* token){
	Mp3_Handle* pHandle;

	pHandle=malloc(sizeof(Mp3_Handle));
	if(pHandle==NULL)
		goto fail0;
	
	memset(&pHandle->ctrl,0,sizeof(__mp3ctrl));
	pHandle->pDecoder=MP3InitDecoder();
	if(pHandle->pDecoder==NULL)
		goto fail1;
	
	if(mp3_get_info(name,&pHandle->ctrl)==false){
		goto fail2;
	}
	
	if(f_open(&pHandle->file,name,FA_READ)!=FR_OK){
		printf("mp3文件打开失败\r\n");
		goto fail2;
	}

	if(f_lseek(&pHandle->file,pHandle->ctrl.datastart)!=FR_OK){
		goto fail3;
	}
	
	if(f_read(&pHandle->file,pHandle->file_buf,MP3_FILE_BUF_SZ,&pHandle->read_byte)!=FR_OK)
		goto fail3; 
	
	pHandle->read_ptr=pHandle->file_buf;
	return pHandle;
fail3:
	f_close(&pHandle->file);
fail2:
	MP3FreeDecoder(pHandle->pDecoder);
fail1:
	free(pHandle);
fail0:
	return NULL;
}

static void Mp3_Delete(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	MP3FreeDecoder(pHandle->pDecoder);
	f_close(&pHandle->file);
	free(pHandle);
}

static uint32_t Mp3_GetSamplerate(Music_Decoder_Handle use_handle){
	return ((Mp3_Handle*)use_handle)->ctrl.samplerate;
}

static uint32_t Mp3_GetDataSize(Music_Decoder_Handle use_handle){
	return 16;
}

static uint32_t Mp3_GetMaxBuf(Music_Decoder_Handle use_handle){
	return MP3_FILL_SIZE;
}

static void single_in_double_out(int16_t* buf,uint32_t max_num){
	for(size_t i=0;i<max_num;i++){
		buf[2*(max_num-i)-2]=buf[i];
		buf[2*(max_num-i)-1]=buf[i];
	}
}


static bool Mp3_FillBuf(Music_Decoder_Handle use_handle,void* buf){
	Mp3_Handle* pHandle=use_handle;
	int offset;
	int err;
refill:
	while((offset=MP3FindSyncWord(pHandle->read_ptr,pHandle->read_byte))<0){
		UINT br;
		memmove(pHandle->file_buf,pHandle->read_ptr,pHandle->read_byte);
		if(f_read(&pHandle->file,pHandle->file_buf+pHandle->read_byte,MP3_FILE_BUF_SZ-pHandle->read_byte,&br)!=FR_OK){
			return false;
		}
		if(br==0){
			return false;
		}
		pHandle->read_byte+=br;
		pHandle->read_ptr=pHandle->file_buf;
	}
	pHandle->read_ptr+=offset;
	pHandle->read_byte-=offset;
	
	err=MP3Decode(pHandle->pDecoder,&pHandle->read_ptr,(int*)&pHandle->read_byte,buf,0);
	if(err!=0){
		printf("decode error:%d\r\n",err);
		pHandle->read_ptr++;
		pHandle->read_byte--;
		goto refill;
	}
	MP3GetLastFrameInfo(pHandle->pDecoder,&pHandle->frame_info);
	if(pHandle->frame_info.nChans!=2){
		single_in_double_out(buf,MP3_FILL_SIZE/4);
	}
	return true;
}

static uint32_t Mp3_GetTotalPos(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return (f_size(&pHandle->file)-pHandle->ctrl.datastart)/MP3_FILL_SIZE;
}

static uint32_t Mp3_GetNowPos(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return (f_tell(&pHandle->file)-pHandle->ctrl.datastart)/MP3_FILL_SIZE;
}

static bool Mp3_SetPos(Music_Decoder_Handle use_handle,uint32_t pos){
	Mp3_Handle* pHandle=use_handle;
	uint32_t raw_pos;
	if(pos>Mp3_GetTotalPos(use_handle)){
		pos=Mp3_GetTotalPos(use_handle);
	}
	pos*=MP3_FILL_SIZE;
	pos+=pHandle->ctrl.datastart;
	
	
	raw_pos=f_tell(&pHandle->file);
	if(f_lseek(&pHandle->file,pos)!=FR_OK){
		return false;
	}
	
	if(f_read(&pHandle->file,pHandle->file_buf,MP3_FILE_BUF_SZ,&pHandle->read_byte)!=FR_OK)
		goto fail1; 
	pHandle->read_ptr=pHandle->file_buf;
	return true;
fail1:	
	f_lseek(&pHandle->file,raw_pos);
fail0:
	return false;
}

static uint32_t Mp3_GetTotalSec(Music_Decoder_Handle use_handle){
	Mp3_Handle* pHandle=use_handle;
	return pHandle->ctrl.totsec;
}

const Music_Decoder Mp3_Decoder_Device={
	.pCreate=Mp3_Create,
	.pDelete=Mp3_Delete,
	.pFillBuf=Mp3_FillBuf,
	.pGetBufMax=Mp3_GetMaxBuf,
	.pGetDataSize=Mp3_GetDataSize,
	.pGetSamplate=Mp3_GetSamplerate,
	.pGetTotalPos=Mp3_GetTotalPos,
	.pGetNowPos=Mp3_GetNowPos,
	.pSetPos=Mp3_SetPos,
	.pGetTotalSec=Mp3_GetTotalSec,
	.surport_file="mp3"
};


