#ifndef MY_TREE_H__
#define MY_TREE_H__

//封装了树相关操作

#include <stddef.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif

struct My_Tree;

typedef struct My_Tree_Node {
	struct My_Tree* pTree;
	struct My_Tree_Node* p_Parent;
	struct My_Tree_Node* p_FirstChild;
	struct My_Tree_Node* p_RightBrother;
	void* data;
	void(*del_call)(struct My_Tree_Node*);
}My_Tree_Node;

typedef struct My_Tree {
	struct My_Tree_Node root_node;
	size_t num;
}My_Tree;

static __inline bool My_Tree_Is_RootNode(const My_Tree_Node* const pNode) {
	return pNode == &(pNode->pTree->root_node);
}

static __inline My_Tree_Node* My_Tree_Get_RootNode(My_Tree* const pTree) {
	return &(pTree->root_node);
}

static __inline void* My_Tree_Node_Get_Data(const My_Tree_Node* const pNode) {
	return pNode->data;
}

static __inline My_Tree_Node* My_Tree_Node_Get_Parent(const My_Tree_Node* const pNode) {
	return pNode->p_Parent;
}

static __inline My_Tree_Node* My_Tree_Node_Get_RightBrother(const My_Tree_Node* const pNode) {
	return pNode->p_RightBrother;
}

static __inline My_Tree_Node* My_Tree_Node_Get_LeftBrother(const My_Tree_Node* const pNode) {
	if (pNode == pNode->p_Parent->p_FirstChild) {
		return NULL;
	}
	else {
		My_Tree_Node* p_LeftBrother = pNode->p_Parent->p_FirstChild;
		while (p_LeftBrother != NULL && p_LeftBrother->p_RightBrother != pNode) {
			p_LeftBrother = p_LeftBrother->p_RightBrother;
		}
		return p_LeftBrother;
	}
}

static __inline My_Tree_Node* My_Tree_Node_Get_FirstChild(const My_Tree_Node* const pNode) {
	return pNode->p_FirstChild;
}

static __inline My_Tree_Node* My_Tree_Node_Get_LastChild(const My_Tree_Node* const pNode) {
	My_Tree_Node* p_LastChild = pNode->p_FirstChild;
	while (p_LastChild != NULL && p_LastChild->p_RightBrother != NULL) {
		p_LastChild = p_LastChild->p_RightBrother;
	}
	return p_LastChild;
}

static __inline size_t My_Tree_Node_Get_ChildNum(const My_Tree_Node* const pNode) {
	if (My_Tree_Is_RootNode(pNode))
		return pNode->pTree->num;
	else {
		extern size_t My_Tree_Node_Traverse_Front(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*));
		return My_Tree_Node_Traverse_Front(pNode->p_FirstChild, NULL);
	}
}

static __inline size_t My_Tree_Get_NodeNum(const My_Tree* const pTree) {
	return pTree->num;
}

//初始化一个空树
void My_Tree_Init(My_Tree* const pTree);
//初始化一个空节点
void My_Tree_Node_Init(My_Tree_Node* const pNode, void* data, void(*del_call)(My_Tree_Node*));
//将一个节点插入到另一个节点右面
bool My_Tree_Node_Insert_NodeRight(My_Tree_Node* const pNode, My_Tree_Node* const pInsert);
//将一个节点插入到另一个节点左面
bool My_Tree_Node_Insert_NodeLeft(My_Tree_Node* const pNode, My_Tree_Node* const pInsert);
//将一个节点插入一个另一个父节点的第一个子节点中
bool My_Tree_Node_Insert_FirstChild(My_Tree_Node* const pParent, My_Tree_Node* const pInsert);
//将一个节点插入一个另一个父节点的最后一个子节点中
bool My_Tree_Node_Insert_LastChild(My_Tree_Node* const pParent, My_Tree_Node* const pInsert);
//移除节点本身及其所有的子节点
bool My_Tree_Node_Remove(My_Tree_Node* const pNode);
//移出树中除根节点外所有节点
void My_Tree_Remove_All(My_Tree* const pTree);
void My_Tree_Move(My_Tree* const pSrcTree, My_Tree* const pDestTree);
//后序遍历该节点下的所有子节点
size_t My_Tree_Node_Traverse_Back(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*));
//前序遍历该节点下的所有子节点
size_t My_Tree_Node_Traverse_Front(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*));
size_t My_Tree_Node_Traverse_Back_L(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*));
size_t My_Tree_Node_Traverse_Front_L(My_Tree_Node* const pNode, void(*call)(My_Tree_Node*));

void My_Tree_Traverse_Back(My_Tree* const pTree, void(*call)(My_Tree_Node*));
void My_Tree_Traverse_Front(My_Tree* const pTree, void(*call)(My_Tree_Node*));
void My_Tree_Traverse_Back_L(My_Tree* const pTree, void(*call)(My_Tree_Node*));
void My_Tree_Traverse_Front_L(My_Tree* const pTree, void(*call)(My_Tree_Node*));
#ifdef __cplusplus
}
#endif

#endif

