#ifndef VISUAL_DEBUG_H_
#define VISUAL_DEBUG_H_
//本文件用来实现调试信息的可视化
//历史调试信息会保存在一个数组中，并且会自动在每条消息
//前添加时间戳
#include "stm32h7xx_sys.h"

#define DEBUG_SHOW 		1

#ifdef __cplusplus
extern "C" {
#endif

//清空日志缓冲
void Debug_Message_Clear(void);

bool Debug_Message_Send(const char* data,uint32_t len);

bool Debug_Message_Send_With_Shell(const char* data,uint32_t len);

bool Debug_Message_Recive(char* mes,uint32_t len,uint32_t* real_len);

void Debug_Error_Out(char ch);

uint32_t Debug_Message_Write(const char* mes,uint32_t len);	
	
bool Visual_Debug_Init(void);
#ifdef __cplusplus
}
#endif

#endif
