#include "visual_debug.h"
#include "hw_time.h"
#include "shell.h"
#include "my_queue.h"
#include "log.h"

#define DEBUG_REC_MAX		(1024)
#define SHELL_TASK_STK		(1024*4)
#define SHELL_BUFFER_SIZE	(1024)

static osRtxThread_t shell_task_work_buf __THREAD;
static __ALIGNED(8) uint8_t shell_task_stack[SHELL_TASK_STK];
static const osThreadAttr_t shell_task_attr={
	.cb_mem=&shell_task_work_buf,
	.cb_size=sizeof(shell_task_work_buf),
	.stack_mem=&shell_task_stack[0],
	.stack_size=sizeof(shell_task_stack),
	.priority=SHELL_TASK_PRIO,
	.attr_bits=osThreadDetached,
	.name="Shell_Task"
};
static osThreadId_t shell_Handle=NULL;

static osRtxMutex_t shell_lock_work_buf __MUTEX;
static const osMutexAttr_t shell_lock_arrt={
	.cb_mem=&shell_lock_work_buf,
	.cb_size=sizeof(shell_lock_work_buf),
	.attr_bits=osMutexPrioInherit|osMutexRobust|osMutexRecursive,
	.name="shell_lock"
};
static osMutexId_t shell_lock_id=NULL;

static uint8_t Debug_Rec_Buf[DEBUG_REC_MAX];
static My_Queue Data_Rec_Queue={
	.buf=Debug_Rec_Buf,
	.buf_size=sizeof(Debug_Rec_Buf),
	.read_ptr=0,
	.write_ptr=0
};

static signed int shell_write(char * buf, unsigned int len);
static signed int shell_read(char * buf, unsigned int len);
static int shell_lock(Shell* pshell);
static int shell_unlock(Shell* pshell);

static Shell shell_handle={
	.read=shell_read,
	.write=shell_write,
	.lock=shell_lock,
	.unlock=shell_unlock
};
static char shell_buffer[SHELL_BUFFER_SIZE];

static void ShellLogWrite(char *buffer, short len);
static Log shell_log={
	.active=true,
	.level= LOG_DEBUG,
	.write=ShellLogWrite
};

static int shell_lock(Shell* pshell){
	osMutexAcquire(shell_lock_id,osWaitForever);
	return 0;
}

static int shell_unlock(Shell* pshell){
	osMutexRelease(shell_lock_id);
	return 0;
}

static void ShellLogWrite(char *buffer, short len){
	Debug_Message_Send_With_Shell(buffer,len);
}

void Debug_Message_Clear(void){
	printf("\033[2J\033[1H");
	fflush(stdout);
}

void Debug_Process_Data(uint8_t data){
	My_Queue_Write(&Data_Rec_Queue,&data,1);
}

uint32_t Debug_Message_Write(const char* mes,uint32_t len){
	return My_Queue_Write(&Data_Rec_Queue,(const uint8_t*)mes,len);
}

bool Debug_Message_Recive(char* mes,uint32_t len,uint32_t* real_len){
	*real_len=0;
	while(len!=0){
		uint32_t buf_len;
		while(My_Queue_Is_Empty(&Data_Rec_Queue)){
			osDelay(100);
		}
		buf_len=My_Queue_Read(&Data_Rec_Queue,(uint8_t*)mes,len);
		(*real_len)+=buf_len;
		mes+=buf_len;
		len-=buf_len;
	}                                                                                                                                                                             
	return true;
}


bool Debug_Message_Send_With_Shell(const char* data,uint32_t len){
	if(shell_Handle==NULL){
		Debug_Send_Data((const uint8_t*)data,len);
	}
	else
		shellWriteEndLine(&shell_handle,(char*)data,len);
	return true;
}

void Debug_Error_Out(char ch){
	Debug_Send_Data((uint8_t*)&ch,1);
}


static signed int shell_write(char * buf, unsigned int len){
	Debug_Send_Data((const uint8_t*)buf,len);
	return len;
}

static signed int shell_read(char * buf, unsigned int len){
	uint32_t real_len;
	Debug_Message_Recive(buf,len,&real_len);
	return real_len;
}

bool Visual_Debug_Init(void){
	shell_lock_id=osMutexNew(&shell_lock_arrt);
	if(shell_lock_id==NULL){
		printf("消息邮箱创建失败\r\n");
		return false;
	}
	shellInit(&shell_handle,shell_buffer,sizeof(shell_buffer));
	shell_Handle=osThreadNew(shellTask,&shell_handle,&shell_task_attr);
	if(shell_Handle==NULL){
		printf("shell创建失败\r\n");
		return false;
	}
	logRegister(&shell_log,&shell_handle);
	return true;
}

