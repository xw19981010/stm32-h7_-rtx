/*------------------------------------------------------------------------------
 * MDK Middleware - Component ::USB:Device:MSC
 * Copyright (c) 2004-2020 Arm Limited (or its affiliates). All rights reserved.
 *------------------------------------------------------------------------------
 * Name:    USBD_User_MSC_0.c
 * Purpose: USB Device Mass Storage Device class (MSC) User module
 * Rev.:    V6.3.4
 *----------------------------------------------------------------------------*/
/**
 * \addtogroup usbd_mscFunctions
 *
 */
 
 
//! [code_USBD_User_MSC]
 
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "rl_usb.h"
#include "stm32h7xx_sys.h"
#include "sdcard_rtos.h"
#include "fatfs.h"
 
static __ALIGNED(32)  uint8_t block_buf[64*512]__IN_AXI_RAM;    // Buffer for block read/write to media
 
// Called during USBD_Initialize to initialize the USB MSC class instance.
void USBD_MSC0_Initialize (void) {
	SDCard_Init();
}
 
 
// \brief Called during USBD_Uninitialize to de-initialize the USB MSC class instance.
void USBD_MSC0_Uninitialize (void) {
  // Add code for de-initialization
}

// Get cache information.
// \param[out]    buffer               cache buffer address.
// \param[out]    size                 cache buffer size.
// \return        true                 operation succeeded.
// \return        false                operation failed.
bool USBD_MSC0_GetCacheInfo (uint32_t *buffer, uint32_t *size) {
  *buffer = (uint32_t)block_buf;        // Local buffer for data
  *size   = sizeof(block_buf);          // Size of local buffer
  return true;
}

// Get media capacity.
// \param[out]    block_count          total number of blocks on media.
// \param[out]    block_size           media block size.
// \return        true                 operation succeeded.
// \return        false                operation failed.
bool USBD_MSC0_GetMediaCapacity (uint32_t *block_count, uint32_t *block_size) {
	HAL_SD_CardInfoTypeDef pinfo;
	HW_SDCard_GetInfo(&pinfo);
	*block_count=pinfo.LogBlockNbr;
	*block_size=pinfo.LogBlockSize;
	return true;
}
 
 
// Read data from media.
// \param[in]     lba                  logical address of first block to read.
// \param[in]     cnt                  number of contiguous blocks to read from media.
// \param[out]    buf                  data buffer for data read from media.
// \return        true                 read succeeded.
// \return        false                read failed.
bool USBD_MSC0_Read (uint32_t lba, uint32_t cnt, uint8_t *buf) {
	return portUSB_SDCard_Read(buf,lba,cnt);
}
 
 
// Write data to media.
// \param[in]     lba                  logical address of first block to write.
// \param[in]     cnt                  number of contiguous blocks to write to media.
// \param[out]    buf                  data buffer containing data to write to media.
// \return        true                 write succeeded.
// \return        false                write failed.
bool USBD_MSC0_Write (uint32_t lba, uint32_t cnt, const uint8_t *buf) {
	return portUSB_SDCard_Write(buf,lba,cnt);
}

 
// Check media presence and write protect status.
//        (if media is not owned by USB it returns that media is not ready)
// \return                             media presence and write protected status
//                bit 1:               write protect bit
//                 - value 1:            media is write protected
//                 - value 0:            media is not write protected
//                bit 0:               media presence bit
//                 - value 1:            media is present
//                 - value 0:            media is not present
uint32_t USBD_MSC0_CheckMedia (void) {
	switch(SDCard_Get_State()){
		case FATFS_WR_NO_USB:
			return USBD_MSC_MEDIA_READY|USBD_MSC_MEDIA_PROTECTED;
		case FATFS_R_USB_R:
			return USBD_MSC_MEDIA_READY|USBD_MSC_MEDIA_PROTECTED;
		case NO_FATFS_USB_WR:
			return USBD_MSC_MEDIA_READY;
		default:
			return USBD_MSC_MEDIA_READY|USBD_MSC_MEDIA_PROTECTED;
	}
}
//! [code_USBD_User_MSC]
