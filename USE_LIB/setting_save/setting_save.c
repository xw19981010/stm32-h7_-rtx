#include "setting_save.h"
#include "cJSON.h"
#include "ff.h"

SettingTypedef Common_Setting={
	.lcd_light= 255,
	.back_pic="0:/picture/background.jpg",
	.hp_r_volume=5,
	.hp_l_volume=5,
	.spk_volume=20,
	.soft_volume_gain=0,
	.fresh_burn=false,
};


const char setting_path[]="0:/SYSTEM/setting.json";

typedef enum{
	SETTING_NONE,		//无错误
	SETTING_MISS,		//没有指定项
	SETTING_VALUE_ERR,	//值设置错误
	SETTING_ADD_ERR		//添加错误
}SETTING_ERR;



static SETTING_ERR json_load_setting(cJSON* const proot,SettingTypedef* const buf_set){
	cJSON*			pItem=NULL;	

	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"lcd_light");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsNumber(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->lcd_light=pItem->valuedouble;
	

	pItem=cJSON_GetObjectItemCaseSensitive(proot,"back_pic");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsString(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->back_pic=calloc(strlen(pItem->valuestring)+1,1);
	strcpy((char*)buf_set->back_pic,pItem->valuestring);
	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"spk_volume");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsNumber(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->spk_volume=pItem->valuedouble;
	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"hp_r_volume");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsNumber(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->hp_r_volume=pItem->valuedouble;
	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"hp_l_volume");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsNumber(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->hp_l_volume=pItem->valuedouble;
	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"soft_volume_gain");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsNumber(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->soft_volume_gain=pItem->valuedouble;
	
	pItem=cJSON_GetObjectItemCaseSensitive(proot,"fresh_burn");
	if(pItem==NULL){
		return SETTING_MISS;
	}
	if(cJSON_IsBool(pItem)==false){
		return SETTING_VALUE_ERR;
	}
	buf_set->fresh_burn=cJSON_IsTrue(pItem);
	
	return SETTING_NONE;
}

static SETTING_ERR json_create_setting(cJSON* const proot,SettingTypedef* const buf_set){
	cJSON* 			pItem=NULL;

	

	pItem=cJSON_AddNumberToObject(proot,"lcd_light",buf_set->lcd_light);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddStringToObject(proot,"back_pic",buf_set->back_pic);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddNumberToObject(proot,"spk_volume",buf_set->spk_volume);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddNumberToObject(proot,"hp_r_volume",buf_set->hp_r_volume);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddNumberToObject(proot,"hp_l_volume",buf_set->hp_l_volume);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddNumberToObject(proot,"soft_volume_gain",buf_set->soft_volume_gain);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	pItem=cJSON_AddBoolToObject(proot,"fresh_burn",buf_set->fresh_burn);
	if(pItem==NULL){
		return SETTING_ADD_ERR;
	}
	
	return SETTING_NONE;
}



bool Create_New_Setting(void){
	FRESULT			res	=FR_OK;
	FIL* 			file=(FIL*)malloc(sizeof(FIL));
	UINT 			bw;
	char*			wbuf=NULL;
	cJSON*			proot=NULL;
	cJSON*			pItem=NULL;
	
	printf("开始重建配置文件。。。\r\n");
	if(file==NULL){
		printf("内存不足！\r\n");
		goto fail0;
	}
	proot=cJSON_CreateObject();
	if(proot==NULL){
		printf("JSON创建错误！\r\n");
		goto fail0;
	}
		
	switch(json_create_setting(proot,&Common_Setting)){
	case SETTING_NONE:
		break;
	case SETTING_MISS:
		printf("配置文件缺少指定项！\r\n");
		goto fail1;
	case SETTING_VALUE_ERR:
		printf("配置值设置错误！\r\n");
		goto fail1;
	case SETTING_ADD_ERR:
		printf("JSON添加错误！\r\n");
		goto fail1;
	default:
		printf("json:%s\r\n",cJSON_GetErrorPtr());
		goto fail1;
	}	
	
	wbuf=cJSON_Print(proot);
	if(wbuf==NULL){
		printf("内存不足！\r\n");
		goto fail1;
	}
	if(f_open(file,setting_path,FA_OPEN_EXISTING)==FR_OK){
		f_close(file);
		f_unlink(setting_path);
	}
	res=f_open(file,setting_path,FA_CREATE_ALWAYS|FA_WRITE);
	if(res!=FR_OK){
		printf("文件创建失败！\r\n");
		goto fail1;
	}
	res=f_write(file,wbuf,strlen(wbuf),&bw);
	if(res!=FR_OK){
		printf("文件写入失败！\r\n");
		goto fail2;
	}
	f_close(file);
	cJSON_Delete(proot);
	free(file);
	free(wbuf);
	printf("重建配置文件成功！\r\n");
	return true;
fail2:
	f_close(file);
fail1:
	cJSON_Delete(proot);
fail0:
	free(file);
	free(wbuf);
	return false;
}

bool Load_Setting(void){
	FIL* 			file=(FIL*)malloc(sizeof(FIL));
	FRESULT			res	=FR_OK;
	char*			rbuf=NULL;
	UINT 			br;
	cJSON*			proot=NULL;
	cJSON*			pItem=NULL;
	SettingTypedef* buf_set=(SettingTypedef*)malloc(sizeof(SettingTypedef));
	
	if(file==NULL||buf_set==NULL){
		printf("内存不足！\r\n");
		goto fail0;
	}
	res=f_open(file,setting_path,FA_READ);
	if(res!=FR_OK){
		printf("配置文件打开失败！\r\n");
		goto fail0;
	}
	printf(	"配置文件打开成功！\r\n"
			"正在加载。。。\r\n");
	
	rbuf=malloc(f_size(file)+1);
	if(rbuf==NULL){
		printf("内存不足！\r\n");
		goto fail1;
	}
	
	res=f_read(file,rbuf,f_size(file),&br);
	if(res!=FR_OK){
		printf("配置文件读取失败！\r\n");
		goto fail1;
	}
	rbuf[f_size(file)]='\0';
	proot=cJSON_Parse(rbuf);
	if(proot==NULL){
		printf("JSON解析错误：%s\r\n",cJSON_GetErrorPtr());
		goto fail1;
	}
	
	switch(json_load_setting(proot,buf_set)){
	case SETTING_NONE:
		break;
	case SETTING_MISS:
		printf("配置文件缺少指定项！\r\n");
		goto fail2;
	case SETTING_VALUE_ERR:
		printf("配置值设置错误！\r\n");
		goto fail2;
	case SETTING_ADD_ERR:
		printf("JSON添加错误！\r\n");
		goto fail2;
	default:
		printf("json:%s\r\n",cJSON_GetErrorPtr());
		goto fail2;
	}	
	
	memcpy(&Common_Setting,buf_set,sizeof(SettingTypedef));
	cJSON_Delete(proot);
	f_close(file);
	free(rbuf);
	free(file);
	free(buf_set);
	printf("配置文件加载成功！\r\n");
	return true;
fail2:
	cJSON_Delete(proot);
fail1:
	f_close(file);
fail0:	
	free(rbuf);
	free(file);
	free(buf_set);
	printf("配置文件加载失败，重建中。。。\r\n");
	return Create_New_Setting();
}

















