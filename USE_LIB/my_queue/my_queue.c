#include "my_queue.h"

void My_Queue_Init(My_Queue* pqueue,uint8_t* buf,size_t buf_size){
	pqueue->buf=buf;
	pqueue->buf_size=buf_size;
	pqueue->read_ptr=0;
	pqueue->write_ptr=0;
}

size_t My_Queue_Write(My_Queue* pqueue,const uint8_t* buf,size_t size){
	for(size_t i=0;i<size;i++){
		if(My_Queue_Is_Full(pqueue)){
			return i;
		}
		pqueue->buf[pqueue->write_ptr]=buf[i];
		pqueue->write_ptr=(pqueue->write_ptr+1)%pqueue->buf_size;
	}
	return size;
}

size_t My_Queue_Read(My_Queue* pqueue,uint8_t* buf,uint32_t size){
	for(size_t i=0;i<size;i++){
		if(My_Queue_Is_Empty(pqueue)){
			return i;
		}
		buf[i]=pqueue->buf[pqueue->read_ptr];
		pqueue->read_ptr=(pqueue->read_ptr+1)%pqueue->buf_size;
	}
	return size;
}

