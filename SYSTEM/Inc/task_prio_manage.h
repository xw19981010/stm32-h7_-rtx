#ifndef TASK_RPIO_MANAGE_H__
#define TASK_RPIO_MANAGE_H__

#define START_TASK_PRIO			osPriorityHigh7
#define MUSIC_END_HOOK_PRIO		osPriorityHigh6
#define MUSIC_TASK_PRIO			osPriorityHigh5


#define FRESH_TASK_PRIO			osPriorityAboveNormal7

#define SHELL_TASK_PRIO			osPriorityNormal5
#define LVGL_TASK_RPIO			osPriorityNormal4
#define LUA___TASK_PRIO			osPriorityNormal3




#endif
