
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'STM32H7_RTX5' 
 * Target:  'STM32H7_RTX5' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32h7xx.h"

/* ARM::CMSIS:RTOS2:Keil RTX5:Source:5.5.3 */
#define RTE_CMSIS_RTOS2                 /* CMSIS-RTOS2 */
        #define RTE_CMSIS_RTOS2_RTX5            /* CMSIS-RTOS2 Keil RTX5 */
        #define RTE_CMSIS_RTOS2_RTX5_SOURCE     /* CMSIS-RTOS2 Keil RTX5 Source */
/* Keil.MDK-Pro::USB:CORE:Release:6.15.4 */
#define RTE_USB_Core                    /* USB Core */
          #define RTE_USB_Core_Release            /* USB Core Release Version */
/* Keil.MDK-Pro::USB:Device:6.15.4 */
#define RTE_USB_Device_0                /* USB Device 0 */

/* Keil.MDK-Pro::USB:Device:MSC:6.15.4 */
#define RTE_USB_Device_MSC_0            /* USB Device MSC instance 0 */

/* Keil::CMSIS Driver:USB Device:1.5.0 */
#define RTE_Drivers_USBD0               /* Driver USBD0 */
        #define RTE_Drivers_USBD1               /* Driver USBD1 */
/* Keil::Device:STM32Cube Framework:STM32CubeMX:2.0.0 */
#define RTE_DEVICE_FRAMEWORK_CUBE_MX
/* Keil::Device:STM32Cube HAL:ADC:1.9.0 */
#define RTE_DEVICE_HAL_ADC
/* Keil::Device:STM32Cube HAL:Common:1.9.0 */
#define RTE_DEVICE_HAL_COMMON
/* Keil::Device:STM32Cube HAL:Cortex:1.9.0 */
#define RTE_DEVICE_HAL_CORTEX
/* Keil::Device:STM32Cube HAL:DMA2D:1.9.0 */
#define RTE_DEVICE_HAL_DMA2D
/* Keil::Device:STM32Cube HAL:DMA:1.9.0 */
#define RTE_DEVICE_HAL_DMA
/* Keil::Device:STM32Cube HAL:GPIO:1.9.0 */
#define RTE_DEVICE_HAL_GPIO
/* Keil::Device:STM32Cube HAL:IRDA:1.9.0 */
#define RTE_DEVICE_HAL_IRDA
/* Keil::Device:STM32Cube HAL:JPEG:1.9.0 */
#define RTE_DEVICE_HAL_JPEG
/* Keil::Device:STM32Cube HAL:LTDC:1.9.0 */
#define RTE_DEVICE_HAL_LTDC
/* Keil::Device:STM32Cube HAL:MDMA:1.9.0 */
#define RTE_DEVICE_HAL_MDMA
/* Keil::Device:STM32Cube HAL:PCD:1.9.0 */
#define RTE_DEVICE_HAL_PCD
/* Keil::Device:STM32Cube HAL:PWR:1.9.0 */
#define RTE_DEVICE_HAL_PWR
/* Keil::Device:STM32Cube HAL:QSPI:1.9.0 */
#define RTE_DEVICE_HAL_QSPI
/* Keil::Device:STM32Cube HAL:RCC:1.9.0 */
#define RTE_DEVICE_HAL_RCC
/* Keil::Device:STM32Cube HAL:RNG:1.9.0 */
#define RTE_DEVICE_HAL_RNG
/* Keil::Device:STM32Cube HAL:RTC:1.9.0 */
#define RTE_DEVICE_HAL_RTC
/* Keil::Device:STM32Cube HAL:SAI:1.9.0 */
#define RTE_DEVICE_HAL_SAI
/* Keil::Device:STM32Cube HAL:SD:1.9.0 */
#define RTE_DEVICE_HAL_SD
/* Keil::Device:STM32Cube HAL:SDRAM:1.9.0 */
#define RTE_DEVICE_HAL_SDRAM
/* Keil::Device:STM32Cube HAL:SPI:1.9.0 */
#define RTE_DEVICE_HAL_SPI
/* Keil::Device:STM32Cube HAL:Smartcard:1.9.0 */
#define RTE_DEVICE_HAL_SMARTCARD
/* Keil::Device:STM32Cube HAL:TIM:1.9.0 */
#define RTE_DEVICE_HAL_TIM
/* Keil::Device:STM32Cube HAL:UART:1.9.0 */
#define RTE_DEVICE_HAL_UART
/* Keil::Device:STM32Cube HAL:USART:1.9.0 */
#define RTE_DEVICE_HAL_USART
/* Keil::Device:Startup:1.9.0 */
#define RTE_DEVICE_STARTUP_STM32H7XX    /* Device Startup for STM32H7 */


#endif /* RTE_COMPONENTS_H */
