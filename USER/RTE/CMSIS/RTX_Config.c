/*
 * Copyright (c) 2013-2021 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 * $Revision:   V5.1.1
 *
 * Project:     CMSIS-RTOS RTX
 * Title:       RTX Configuration
 *
 * -----------------------------------------------------------------------------
 */
 
#include "cmsis_compiler.h"
#include "rtx_os.h"
#include "rtx_evr.h"
#include "tlsf.h"
#include "cmsis_os2.h"
// OS Idle Thread
__WEAK __NO_RETURN void osRtxIdleThread (void *argument) {
  (void)argument;
  extern void vApplicationIdleHook(void);
  extern void delay_xms(volatile uint32_t nms);
  extern void delay_us(volatile uint32_t nus);
  for (;;) {
	  vApplicationIdleHook();
	  delay_xms(1);
  }
}
 
// OS Error Callback function
__WEAK uint32_t osRtxErrorNotify (uint32_t code, void *object_id) {
  (void)object_id;
  extern void Debug_UnSafe_Printf(const char* fmt,...);
  switch (code) {
    case osRtxErrorStackOverflow:  
      // Stack overflow detected for thread (thread_id=object_id)
	  Debug_UnSafe_Printf("Stack overflow detected for thread:%s,0x%08x\r\n",osThreadGetName(object_id),(uint32_t)object_id);
      break;
    case osRtxErrorISRQueueOverflow:
      // ISR Queue overflow detected when inserting object (object_id)
	  Debug_UnSafe_Printf("ISR Queue overflow detected when inserting object:0x%08x\r\n",(uint32_t)object_id);
      break;
    case osRtxErrorTimerQueueOverflow:
      // User Timer Callback Queue overflow detected for timer (timer_id=object_id)
	  Debug_UnSafe_Printf("User Timer Callback Queue overflow detected for timer:0x%08x\r\n",(uint32_t)object_id);
      break;
    case osRtxErrorClibSpace:
      // Standard C/C++ library libspace not available: increase OS_THREAD_LIBSPACE_NUM
	  Debug_UnSafe_Printf("Standard C/C++ library libspace not available: increase OS_THREAD_LIBSPACE_NUM:0x%08x\r\n",(uint32_t)object_id);
      break;
    case osRtxErrorClibMutex:
      // Standard C/C++ library mutex initialization failed
	  Debug_UnSafe_Printf("Standard C/C++ library mutex initialization failed:0x%08x\r\n",(uint32_t)object_id);
      break;
    default:
      // Reserved
	  Debug_UnSafe_Printf("osRtxErrorNotify: Unknown fault\r\n");
      break;
  }
  for (;;) {}
//return 0U;
}

//  ==== Library functions ====

/// Initialize Memory Pool with variable block size.
/// \param[in]  mem             pointer to memory pool.
/// \param[in]  size            size of a memory pool in bytes.
/// \return 1 - success, 0 - failure.
uint32_t osRtxMemoryInit (void *mem, uint32_t size) {
	
  // Check parameters
  //lint -e{923} "cast from pointer to unsigned int" [MISRA Note 7]
  if ((mem == NULL) || (((uint32_t)mem & 7U) != 0U) || ((size & 7U) != 0U)){
    EvrRtxMemoryInit(mem, size, 0U);
    //lint -e{904} "Return statement before end of function" [MISRA Note 1]
    return 0U;
  }
  
  if(tlsf_create_with_pool(mem,size)==NULL){
	  EvrRtxMemoryInit(mem, size, 0U);
	  return 0U;
  }

  EvrRtxMemoryInit(mem, size, 1U);
  return 1U;
}

/// Allocate a memory block from a Memory Pool.
/// \param[in]  mem             pointer to memory pool.
/// \param[in]  size            size of a memory block in bytes.
/// \param[in]  type            memory block type: 0 - generic, 1 - control block
/// \return allocated memory block or NULL in case of no memory is available.
void *osRtxMemoryAlloc (void *mem, uint32_t size, uint32_t type) {
	
  void* ptr;
  // Check parameters
  if ((mem == NULL) || (size == 0U)) {
    EvrRtxMemoryAlloc(mem, size, type, NULL);
    //lint -e{904} "Return statement before end of function" [MISRA Note 1]
    return NULL;
  }

  ptr=tlsf_malloc(mem,size);
  
  if(ptr==NULL){
	EvrRtxMemoryAlloc(mem, size, type, NULL);
	return ptr;
  }
  
  EvrRtxMemoryAlloc(mem, size, type, ptr);

  return ptr;
}

/// Return an allocated memory block back to a Memory Pool.
/// \param[in]  mem             pointer to memory pool.
/// \param[in]  block           memory block to be returned to the memory pool.
/// \return 1 - success, 0 - failure.
uint32_t osRtxMemoryFree (void *mem, void *block) {

  // Check parameters
  if ((mem == NULL) || (block == NULL)) {
    EvrRtxMemoryFree(mem, block, 0U);
    //lint -e{904} "Return statement before end of function" [MISRA Note 1]
    return 0U;
  }
  
  tlsf_free(mem,block);
  EvrRtxMemoryFree(mem, block, 1U);

  return 1U;
}


