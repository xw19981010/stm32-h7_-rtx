#ifndef LVGL_TASK_H__
#define LVGL_TASK_H__

#include "stm32h7xx_sys.h"
#include "lvgl/lvgl.h"


bool LVGL_Task_Create(void);
bool LVGL_Protect_Run(void(*func)(void*),void* use_data,bool wait_response);


#endif
