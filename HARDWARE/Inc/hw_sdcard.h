#ifndef HW_SDCARD_H__
#define HW_SDCARD_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
bool HW_SDCard_Init(void);
bool HW_SDCard_GetInfo(HAL_SD_CardInfoTypeDef *cardinfo);	
bool HW_SDCard_ReadDisk(uint8_t*buf,uint32_t sector,uint32_t cnt); 	
bool HW_SDCard_WriteDisk(uint8_t*buf,uint32_t sector,uint32_t cnt);
bool HW_SDCard_ReadDisk_DMA(uint8_t* buf,uint32_t sector,uint32_t cnt);
bool HW_SDCard_WriteDisk_DMA(uint8_t *buf,uint32_t sector,uint32_t cnt);
bool HW_SDCard_Transe_IsOver(void);
extern void HAL_SD_TxCpltCallback(SD_HandleTypeDef *hsd);
extern void HAL_SD_RxCpltCallback(SD_HandleTypeDef *hsd);
#ifdef __cplusplus
}
#endif
#endif
