#ifndef HW_RAND_H__
#define HW_RAND_H__

#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif
bool HW_Rand_Init(void);
uint32_t RNG_Get_RandomNum(void);
#ifdef __cplusplus
}
#endif
#endif
