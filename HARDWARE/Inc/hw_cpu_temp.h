#ifndef HW_CPU_TEMP_H__
#define HW_CPU_TEMP_H__

#include "stm32h7xx_sys.h"


#ifdef __cplusplus
 extern "C" {
#endif
void HW_CPU_Temp_Init(void);
double Get_CPUTemprate(void);

#ifdef __cplusplus
}
#endif

#endif
