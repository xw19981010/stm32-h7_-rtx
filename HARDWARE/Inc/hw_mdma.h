#ifndef HW_MDMA_H__
#define HW_MDMA_H__


#include "stm32h7xx_sys.h"
#ifdef __cplusplus
 extern "C" {
#endif

bool HW_MDMA_Init(uint8_t channel,MDMA_InitTypeDef* init);
bool HW_MDMA_DeInit(uint8_t channel);
bool HW_MDMA_Set_CallBack(uint8_t channel,void(*callback)(MDMA_HandleTypeDef*));
bool HW_MDMA_Start_Transe(uint8_t channel,const void* psrc,void* pdest,uint32_t size);
MDMA_HandleTypeDef* HW_MDMA_Get_Handle(uint8_t channel);

#ifdef __cplusplus
}
#endif
#endif
