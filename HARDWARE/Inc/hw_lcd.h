#ifndef HW_LCD_H__
#define HW_LCD_H__

#include "stm32h7xx_sys.h"
#define LCD_Width     800				
#define LCD_Height    480				
#ifdef __cplusplus
 extern "C" {
#endif
void HW_LCD_Init(LTDC_HandleTypeDef* hltdc);
void HW_LCD_Layer_Config(LTDC_HandleTypeDef* hltdc,void* Frame_Buf,uint32_t color_format);
void LCD_BK_Ctrl(bool on);

#ifdef __cplusplus
}
#endif
#endif
