#include "hw_music.h"
#include "iic_software.h"


#define WM8978_ADDR     0X1A	//WM8978的器件地址,固定为0X1A 

#define WM8978_IIC_SCL_PORT		GPIOE
#define WM8978_IIC_SCL_PIN		GPIO_PIN_4
#define WM8978_IIC_SDA_PORT		GPIOF
#define WM8978_IIC_SDA_PIN		GPIO_PIN_6

#define WM8978_IIC_Start()		IIC_Start_General(WM8978_IIC_SCL_PORT,WM8978_IIC_SDA_PORT,\
									WM8978_IIC_SCL_PIN,WM8978_IIC_SDA_PIN)
#define WM8978_IIC_Stop()		IIC_Stop_General(WM8978_IIC_SCL_PORT,WM8978_IIC_SDA_PORT,\
									WM8978_IIC_SCL_PIN,WM8978_IIC_SDA_PIN)

#define WM8978_IIC_Send_Byte(DATA)	IIC_Send_Byte_General(WM8978_IIC_SCL_PORT,WM8978_IIC_SDA_PORT,\
									WM8978_IIC_SCL_PIN,WM8978_IIC_SDA_PIN,DATA)


//WM8978寄存器值缓存区(总共58个寄存器,0~57),占用116字节内存
//因为WM8978的IIC操作不支持读操作,所以在本地保存所有寄存器值
//写WM8978寄存器时,同步更新到本地寄存器值,读寄存器时,直接返回本地保存的寄存器值.
//注意:WM8978的寄存器值是9位的,所以要用uint16_t来存储. 
static uint16_t WM8978_REGVAL_TBL[58]=
{
	0X0000,0X0000,0X0000,0X0000,0X0050,0X0000,0X0140,0X0000,
	0X0000,0X0000,0X0000,0X00FF,0X00FF,0X0000,0X0100,0X00FF,
	0X00FF,0X0000,0X012C,0X002C,0X002C,0X002C,0X002C,0X0000,
	0X0032,0X0000,0X0000,0X0000,0X0000,0X0000,0X0000,0X0000,
	0X0038,0X000B,0X0032,0X0000,0X0008,0X000C,0X0093,0X00E9,
	0X0000,0X0000,0X0000,0X0000,0X0003,0X0010,0X0010,0X0100,
	0X0100,0X0002,0X0001,0X0001,0X0039,0X0039,0X0039,0X0039,
	0X0001,0X0001
}; 
static SAI_HandleTypeDef SAI1B_Handler;        //SAI1 Block B句柄
static DMA_HandleTypeDef SAI1_TXDMA_Handler;   //DMA发送句柄


//WM8978写寄存器
//reg:寄存器地址
//val:要写入寄存器的值 
//返回值:true,成功;
//    	 false,失败
static bool WM8978_Write_Reg(uint8_t reg,uint16_t val)
{ 
	bool res=true;
	WM8978_IIC_Start(); 
	res&=WM8978_IIC_Send_Byte((WM8978_ADDR<<1)|0);
	res&=WM8978_IIC_Send_Byte((reg<<1)|((val>>8)&0X01));
	res&=WM8978_IIC_Send_Byte(val&0XFF);
    WM8978_IIC_Stop();
	WM8978_REGVAL_TBL[reg]=val;	//保存寄存器值到本地
	return res;	
}  

/**
	* @brief  从cash中读回读回wm8978寄存器
	* @param  _ucRegAddr ： 寄存器地址
	* @retval 寄存器值
	*/
static uint16_t WM8978_Read_Reg(uint8_t _ucRegAddr)
{
	return WM8978_REGVAL_TBL[_ucRegAddr];
}


static void WM8978_GPIO_Init(void){
	GPIO_InitTypeDef GPIO_InitStructure;
	
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	
	GPIO_InitStructure.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Pin=GPIO_PIN_4;
	GPIO_InitStructure.Pull=GPIO_PULLUP;
	GPIO_InitStructure.Speed=GPIO_SPEED_FREQ_LOW;
	
	HAL_GPIO_Init(GPIOE,&GPIO_InitStructure);
	
	GPIO_InitStructure.Pin=GPIO_PIN_6;
	HAL_GPIO_Init(GPIOF,&GPIO_InitStructure);
}

//WM8978初始化
//返回值:true,初始化正常
//    其他,错误代码
static bool WM8978_Init(void)
{ 
	uint8_t i=0;
	WM8978_GPIO_Init();         //初始化IIC接口
	while(WM8978_Write_Reg(0,0)!=true){
		delay_ms(100);
		i++;
//		printf("WM8978第%d次尝试复位失败！\r\n",i);
		if(i>10){
			return false;
		}
	}
	//以下为通用设置
	WM8978_Write_Reg(1,0X1B);	//R1,MICEN设置为1(MIC使能),BIASEN设置为1(模拟器工作),VMIDSEL[1:0]设置为:11(5K)
	WM8978_Write_Reg(2,0X1B0);	//R2,ROUT1,LOUT1输出使能(耳机可以工作),BOOSTENR,BOOSTENL使能
	WM8978_Write_Reg(3,0X6C);	//R3,LOUT2,ROUT2输出使能(喇叭工作),RMIX,LMIX使能	
	WM8978_Write_Reg(6,0);		//R6,MCLK由外部提供
	WM8978_Write_Reg(43,1<<4);	//R43,INVROUT2反向,驱动喇叭
	WM8978_Write_Reg(47,1<<8);	//R47设置,PGABOOSTL,左通道MIC获得20倍增益
	WM8978_Write_Reg(48,1<<8);	//R48设置,PGABOOSTR,右通道MIC获得20倍增益
	WM8978_Write_Reg(49,1<<1);	//R49,TSDEN,开启过热保护 
	WM8978_Write_Reg(49,1<<2);	//R49,SPEAKER BOOST,1.5x 
	WM8978_Write_Reg(10,1<<3);	//R10,SOFTMUTE关闭,128x采样,最佳SNR 
	WM8978_Write_Reg(14,1<<3);	//R14,ADC 128x采样率
	return true;
} 

//WM8978 DAC/ADC配置
//adcen:adc使能(1)/关闭(0)
//dacen:dac使能(1)/关闭(0)
static void WM8978_ADDA_Cfg(uint8_t dacen,uint8_t adcen)
{
	uint16_t regval;
	regval=WM8978_Read_Reg(3);	//读取R3
	if(dacen)regval|=3<<0;		//R3最低2个位设置为1,开启DACR&DACL
	else regval&=~(3<<0);		//R3最低2个位清零,关闭DACR&DACL.
	WM8978_Write_Reg(3,regval);	//设置R3
	regval=WM8978_Read_Reg(2);	//读取R2
	if(adcen)regval|=3<<0;		//R2最低2个位设置为1,开启ADCR&ADCL
	else regval&=~(3<<0);		//R2最低2个位清零,关闭ADCR&ADCL.
	WM8978_Write_Reg(2,regval);	//设置R2	
}

//WM8978 L2/R2(也就是Line In)增益设置(L2/R2-->ADC输入部分的增益)
//gain:0~7,0表示通道禁止,1~7,对应-12dB~6dB,3dB/Step
static void WM8978_LINEIN_Gain(uint8_t gain)
{
	uint16_t regval;
	gain&=0X07;
	regval=WM8978_Read_Reg(47);	//读取R47
	regval&=~(7<<4);			//清除原来的设置 
 	WM8978_Write_Reg(47,regval|gain<<4);//设置R47
	regval=WM8978_Read_Reg(48);	//读取R48
	regval&=~(7<<4);			//清除原来的设置 
 	WM8978_Write_Reg(48,regval|gain<<4);//设置R48
} 
//WM8978 AUXR,AUXL(PWM音频部分)增益设置(AUXR/L-->ADC输入部分的增益)
//gain:0~7,0表示通道禁止,1~7,对应-12dB~6dB,3dB/Step
static void WM8978_AUX_Gain(uint8_t gain)
{
	uint16_t regval;
	gain&=0X07;
	regval=WM8978_Read_Reg(47);	//读取R47
	regval&=~(7<<0);			//清除原来的设置 
 	WM8978_Write_Reg(47,regval|gain<<0);//设置R47
	regval=WM8978_Read_Reg(48);	//读取R48
	regval&=~(7<<0);			//清除原来的设置 
 	WM8978_Write_Reg(48,regval|gain<<0);//设置R48
} 
//WM8978 输入通道配置 
//micen:MIC开启(1)/关闭(0)
//lineinen:Line In开启(1)/关闭(0)
//auxen:aux开启(1)/关闭(0) 
static void WM8978_Input_Cfg(uint8_t micen,uint8_t lineinen,uint8_t auxen)
{
	uint16_t regval;  
	regval=WM8978_Read_Reg(2);	//读取R2
	if(micen)regval|=3<<2;		//开启INPPGAENR,INPPGAENL(MIC的PGA放大)
	else regval&=~(3<<2);		//关闭INPPGAENR,INPPGAENL.
 	WM8978_Write_Reg(2,regval);	//设置R2 
	
	regval=WM8978_Read_Reg(44);	//读取R44
	if(micen)regval|=3<<4|3<<0;	//开启LIN2INPPGA,LIP2INPGA,RIN2INPPGA,RIP2INPGA.
	else regval&=~(3<<4|3<<0);	//关闭LIN2INPPGA,LIP2INPGA,RIN2INPPGA,RIP2INPGA.
	WM8978_Write_Reg(44,regval);//设置R44
	
	if(lineinen)WM8978_LINEIN_Gain(5);//LINE IN 0dB增益
	else WM8978_LINEIN_Gain(0);	//关闭LINE IN
	if(auxen)WM8978_AUX_Gain(7);//AUX 6dB增益
	else WM8978_AUX_Gain(0);	//关闭AUX输入  
}
//WM8978 输出配置 
//dacen:DAC输出(放音)开启(1)/关闭(0)
//bpsen:Bypass输出(录音,包括MIC,LINE IN,AUX等)开启(1)/关闭(0) 
static void WM8978_Output_Cfg(uint8_t dacen,uint8_t bpsen)
{
	uint16_t regval=0;
	if(dacen)regval|=1<<0;	//DAC输出使能
	if(bpsen)
	{
		regval|=1<<1;		//BYPASS使能
		regval|=5<<2;		//0dB增益
	} 
	WM8978_Write_Reg(50,regval);//R50设置
	WM8978_Write_Reg(51,regval);//R51设置 
}
//WM8978 MIC增益设置(不包括BOOST的20dB,MIC-->ADC输入部分的增益)
//gain:0~63,对应-12dB~35.25dB,0.75dB/Step
static void WM8978_MIC_Gain(uint8_t gain)
{
	gain&=0X3F;
	WM8978_Write_Reg(45,gain);		//R45,左通道PGA设置 
	WM8978_Write_Reg(46,gain|1<<8);	//R46,右通道PGA设置
}
 
//设置I2S工作模式
//fmt:0,LSB(右对齐);1,MSB(左对齐);2,飞利浦标准I2S;3,PCM/DSP;
//len:0,16位;1,20位;2,24位;3,32位;  
static void WM8978_I2S_Cfg(uint8_t fmt,uint8_t len)
{
	fmt&=0X03;
	len&=0X03;//限定范围
	WM8978_Write_Reg(4,(fmt<<3)|(len<<5));	//R4,WM8978工作模式设置	
}	

//设置耳机左右声道音量
//voll:左声道音量(0~63)
//volr:右声道音量(0~63)
static void WM8978_HPvol_Set(uint8_t voll,uint8_t volr)
{
	voll&=0X3F;
	volr&=0X3F;//限定范围
	if(voll==0)voll|=1<<6;//音量为0时,直接mute
	if(volr==0)volr|=1<<6;//音量为0时,直接mute 
	WM8978_Write_Reg(52,voll);			//R52,耳机左声道音量设置
	WM8978_Write_Reg(53,volr|(1<<8));	//R53,耳机右声道音量设置,同步更新(HPVU=1)
}
//设置喇叭音量
//voll:左声道音量(0~63) 
static void WM8978_SPKvol_Set(uint8_t volx)
{ 
	volx&=0X3F;//限定范围
	if(volx==0)volx|=1<<6;//音量为0时,直接mute 
 	WM8978_Write_Reg(54,volx);			//R54,喇叭左声道音量设置
	WM8978_Write_Reg(55,volx|(1<<8));	//R55,喇叭右声道音量设置,同步更新(SPKVU=1)	
}
//设置3D环绕声
//depth:0~15(3D强度,0最弱,15最强)
static void WM8978_3D_Set(uint8_t depth)
{ 
	depth&=0XF;//限定范围 
 	WM8978_Write_Reg(41,depth);	//R41,3D环绕设置 	
}
//设置EQ/3D作用方向
//dir:0,在ADC起作用
//    1,在DAC起作用(默认)
static void WM8978_EQ_3D_Dir(uint8_t dir)
{
	uint16_t regval; 
	regval=WM8978_Read_Reg(0X12);
	if(dir)regval|=1<<8;
	else regval&=~(1<<8); 
 	WM8978_Write_Reg(18,regval);//R18,EQ1的第9位控制EQ/3D方向
}

//设置EQ1
//cfreq:截止频率,0~3,分别对应:80/105/135/175Hz
//gain:增益,0~24,对应-12~+12dB
static void WM8978_EQ1_Set(uint8_t cfreq,uint8_t gain)
{ 
	uint16_t regval;
	cfreq&=0X3;//限定范围 
	if(gain>24)gain=24;
	gain=24-gain;
	regval=WM8978_Read_Reg(18);
	regval&=0X100;
	regval|=cfreq<<5;	//设置截止频率 
	regval|=gain;		//设置增益	
 	WM8978_Write_Reg(18,regval);//R18,EQ1设置 	
}
//设置EQ2
//cfreq:中心频率,0~3,分别对应:230/300/385/500Hz
//gain:增益,0~24,对应-12~+12dB
static void WM8978_EQ2_Set(uint8_t cfreq,uint8_t gain)
{ 
	uint16_t regval=0;
	cfreq&=0X3;//限定范围 
	if(gain>24)gain=24;
	gain=24-gain; 
	regval|=cfreq<<5;	//设置截止频率 
	regval|=gain;		//设置增益	
 	WM8978_Write_Reg(19,regval);//R19,EQ2设置 	
}
//设置EQ3
//cfreq:中心频率,0~3,分别对应:650/850/1100/1400Hz
//gain:增益,0~24,对应-12~+12dB
static void WM8978_EQ3_Set(uint8_t cfreq,uint8_t gain)
{ 
	uint16_t regval=0;
	cfreq&=0X3;//限定范围 
	if(gain>24)gain=24;
	gain=24-gain; 
	regval|=cfreq<<5;	//设置截止频率 
	regval|=gain;		//设置增益	
 	WM8978_Write_Reg(20,regval);//R20,EQ3设置 	
}
//设置EQ4
//cfreq:中心频率,0~3,分别对应:1800/2400/3200/4100Hz
//gain:增益,0~24,对应-12~+12dB
static void WM8978_EQ4_Set(uint8_t cfreq,uint8_t gain)
{ 
	uint16_t regval=0;
	cfreq&=0X3;//限定范围 
	if(gain>24)gain=24;
	gain=24-gain; 
	regval|=cfreq<<5;	//设置截止频率 
	regval|=gain;		//设置增益	
 	WM8978_Write_Reg(21,regval);//R21,EQ4设置 	
}
//设置EQ5
//cfreq:中心频率,0~3,分别对应:5300/6900/9000/11700Hz
//gain:增益,0~24,对应-12~+12dB
static void WM8978_EQ5_Set(uint8_t cfreq,uint8_t gain)
{ 
	uint16_t regval=0;
	cfreq&=0X3;//限定范围 
	if(gain>24)gain=24;
	gain=24-gain; 
	regval|=cfreq<<5;	//设置截止频率 
	regval|=gain;		//设置增益	
 	WM8978_Write_Reg(22,regval);//R22,EQ5设置 	
}

static void SAIA_GPIO_Init(void){
	 GPIO_InitTypeDef GPIO_Initure;
    __HAL_RCC_SAI1_CLK_ENABLE();                //使能SAI1时钟
    __HAL_RCC_GPIOE_CLK_ENABLE();               //使能GPIOE时钟
    __HAL_RCC_GPIOF_CLK_ENABLE();
	
    //初始化PF7、8、9
    GPIO_Initure.Pin=GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;  
    GPIO_Initure.Mode=GPIO_MODE_AF_PP;          //推挽复用
    GPIO_Initure.Pull=GPIO_PULLUP;              //上拉
    GPIO_Initure.Speed=GPIO_SPEED_LOW;    //高速
    GPIO_Initure.Alternate=GPIO_AF6_SAI1;       //复用为SAI   
    HAL_GPIO_Init(GPIOF,&GPIO_Initure);         //初始化
	
	GPIO_Initure.Pin=GPIO_PIN_3;
	HAL_GPIO_Init(GPIOE,&GPIO_Initure);         //初始化
}

static void SAI1_Basic_Init(void){
	SAIA_GPIO_Init();
	SAI1B_Handler.Instance=SAI1_Block_B;                     //SAI1 Bock B
	SAI1B_Handler.Init.AudioMode=SAI_MODEMASTER_TX;          //设置SAI1工作模式
	SAI1B_Handler.Init.Synchro=SAI_ASYNCHRONOUS;             //音频模块异步
	SAI1B_Handler.Init.OutputDrive=SAI_OUTPUTDRIVE_ENABLE;   //立即驱动音频模块输出
	SAI1B_Handler.Init.NoDivider=SAI_MASTERDIVIDER_ENABLE;   //使能主时钟分频器(MCKDIV)
	SAI1B_Handler.Init.FIFOThreshold=SAI_FIFOTHRESHOLD_1QF;  //设置FIFO阈值,1/4 FIFO
	SAI1B_Handler.Init.MonoStereoMode=SAI_STEREOMODE;        //立体声模式
	SAI1B_Handler.Init.Protocol=SAI_FREE_PROTOCOL;           //设置SAI1协议为:自由协议(支持I2S/LSB/MSB/TDM/PCM/DSP等协议)
	SAI1B_Handler.Init.DataSize=SAI_DATASIZE_16;             //设置数据大小
	SAI1B_Handler.Init.FirstBit=SAI_FIRSTBIT_MSB;            //数据MSB位优先
	SAI1B_Handler.Init.ClockStrobing=SAI_CLOCKSTROBING_RISINGEDGE;//数据在时钟的上升/下降沿选通
	SAI1B_Handler.Init.AudioFrequency=SAI_AUDIO_FREQUENCY_44K;
	//帧设置
	SAI1B_Handler.FrameInit.FrameLength=64;                  //设置帧长度为64,左通道32个SCK,右通道32个SCK.
	SAI1B_Handler.FrameInit.ActiveFrameLength=32;            //设置帧同步有效电平长度,在I2S模式下=1/2帧长.
	SAI1B_Handler.FrameInit.FSDefinition=SAI_FS_CHANNEL_IDENTIFICATION;//FS信号为SOF信号+通道识别信号
	SAI1B_Handler.FrameInit.FSPolarity=SAI_FS_ACTIVE_LOW;    //FS低电平有效(下降沿)
	SAI1B_Handler.FrameInit.FSOffset=SAI_FS_BEFOREFIRSTBIT;  //在slot0的第一位的前一位使能FS,以匹配飞利浦标准	

	//SLOT设置
	SAI1B_Handler.SlotInit.FirstBitOffset=0;                 //slot偏移(FBOFF)为0
	SAI1B_Handler.SlotInit.SlotSize=SAI_SLOTSIZE_32B;        //slot大小为32位
	SAI1B_Handler.SlotInit.SlotNumber=2;                     //slot数为2个    
	SAI1B_Handler.SlotInit.SlotActive=SAI_SLOTACTIVE_0|SAI_SLOTACTIVE_1;//使能slot0和slot1

	HAL_SAI_Init(&SAI1B_Handler);                            //初始化SAI
	__HAL_SAI_DISABLE(&SAI1B_Handler);
}


static bool SAI1_Config(uint32_t samplerate,uint32_t datasize){
	RCC_PeriphCLKInitTypeDef RCC_PeriphStruct={0};
	switch(samplerate){
		case SAI_AUDIO_FREQUENCY_11K:
		case SAI_AUDIO_FREQUENCY_22K:
		case SAI_AUDIO_FREQUENCY_44K:
			RCC_PeriphStruct.PLL3.PLL3N=406;
			RCC_PeriphStruct.PLL3.PLL3P=6;
			RCC_PeriphStruct.PLL3.PLL3R=10;
			break;
		case SAI_AUDIO_FREQUENCY_8K:
		case SAI_AUDIO_FREQUENCY_16K:
		case SAI_AUDIO_FREQUENCY_32K:
		case SAI_AUDIO_FREQUENCY_48K:
		case SAI_AUDIO_FREQUENCY_96K:
		case SAI_AUDIO_FREQUENCY_192K:
			RCC_PeriphStruct.PLL3.PLL3N=344;
			RCC_PeriphStruct.PLL3.PLL3P=7;
			RCC_PeriphStruct.PLL3.PLL3R=8;
			break;
		default:
			printf("采样率%d不支持\r\n",samplerate);
			return false;
	}
	RCC_PeriphStruct.PLL3.PLL3M=25;
	RCC_PeriphStruct.PLL3.PLL3Q=4;
	
	RCC_PeriphStruct.PLL3.PLL3FRACN=0;
	RCC_PeriphStruct.PLL3.PLL3RGE = RCC_PLL3VCIRANGE_0;
	RCC_PeriphStruct.PLL3.PLL3VCOSEL = RCC_PLL3VCOMEDIUM;
	RCC_PeriphStruct.PeriphClockSelection= RCC_PERIPHCLK_SAI1|RCC_PERIPHCLK_LTDC;
	RCC_PeriphStruct.Sai1ClockSelection=RCC_SAI1CLKSOURCE_PLL3;
	HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphStruct);
	
	HAL_SAI_DeInit(&SAI1B_Handler);                          //清除以前的配置
	SAI1B_Handler.Init.DataSize=datasize;                     //设置数据大小
	SAI1B_Handler.Init.AudioFrequency=samplerate;
	HAL_SAI_Init(&SAI1B_Handler);                            //初始化SAI
	SAI1B_Handler.Instance->CR1|=SAI_xCR1_DMAEN;			//DMA使能
	__HAL_SAI_ENABLE(&SAI1B_Handler);                        //使能SAI 
	return true;
}

static void SAI_DMA_Basic_Init(void){
	__HAL_RCC_DMA1_CLK_ENABLE();
	
	SAI1_TXDMA_Handler.Instance=DMA1_Stream0;
	SAI1_TXDMA_Handler.Init.Direction=DMA_MEMORY_TO_PERIPH;
	SAI1_TXDMA_Handler.Init.FIFOMode=DMA_FIFOMODE_DISABLE;
	SAI1_TXDMA_Handler.Init.MemBurst=DMA_MBURST_SINGLE;
	SAI1_TXDMA_Handler.Init.MemDataAlignment=DMA_MDATAALIGN_HALFWORD;
	SAI1_TXDMA_Handler.Init.MemInc=DMA_MINC_ENABLE;
	SAI1_TXDMA_Handler.Init.Mode=DMA_CIRCULAR;
	SAI1_TXDMA_Handler.Init.PeriphBurst=DMA_PBURST_SINGLE;
	SAI1_TXDMA_Handler.Init.PeriphDataAlignment=DMA_PDATAALIGN_HALFWORD;
	SAI1_TXDMA_Handler.Init.PeriphInc=DMA_PINC_DISABLE;
	SAI1_TXDMA_Handler.Init.Priority=DMA_PRIORITY_MEDIUM;
	SAI1_TXDMA_Handler.Init.Request=DMA_REQUEST_SAI1_B;
	HAL_DMA_Init(&SAI1_TXDMA_Handler);
	__HAL_DMA_DISABLE(&SAI1_TXDMA_Handler);
	HAL_NVIC_SetPriority(DMA1_Stream0_IRQn,SAI1_DMA_ISR_PRIO,0);
	HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);
}

static void SAI_TX_DMA_Config(uint32_t m_data_align,uint32_t p_data_align,void* buf0,void* buf1,uint32_t len){
	SAI1_TXDMA_Handler.Init.MemDataAlignment=m_data_align;
	SAI1_TXDMA_Handler.Init.PeriphDataAlignment=p_data_align;
	HAL_DMA_DeInit(&SAI1_TXDMA_Handler);
	HAL_DMA_Init(&SAI1_TXDMA_Handler);
	HAL_DMAEx_MultiBufferStart(&SAI1_TXDMA_Handler,(uint32_t)buf0,(uint32_t)&SAI1B_Handler.Instance->DR,(uint32_t)buf1,len);
	__HAL_DMA_DISABLE(&SAI1_TXDMA_Handler);
	__HAL_DMA_CLEAR_FLAG(&SAI1_TXDMA_Handler,DMA_FLAG_TCIF0_4);
	__HAL_DMA_ENABLE_IT(&SAI1_TXDMA_Handler,DMA_IT_TC);
}

static void SAI_DMA_Start(void){
	__HAL_DMA_ENABLE(&SAI1_TXDMA_Handler);
}

static void SAI_DMA_Stop(void){
	__HAL_DMA_DISABLE(&SAI1_TXDMA_Handler);
}

__WEAK void Music_Buf0_Over(void){
	
}

__WEAK void Music_Buf1_Over(void){
	
}

uint8_t HW_Music_Get_Buf_Index(void){
	return DMA1_Stream0->CR&DMA_SxCR_CT;
}


void DMA1_Stream0_IRQHandler(void){
	if(__HAL_DMA_GET_FLAG(&SAI1_TXDMA_Handler,DMA_FLAG_TCIF0_4)!=RESET){
		__HAL_DMA_CLEAR_FLAG(&SAI1_TXDMA_Handler,DMA_FLAG_TCIF0_4);
		if(DMA1_Stream0->CR&DMA_SxCR_CT)
			Music_Buf0_Over();
		else
			Music_Buf1_Over();
	}
}


bool HW_Music_Init(void){
	SAI1_Basic_Init();
	SAI_DMA_Basic_Init();
	if(WM8978_Init()==false)
		return false;
	WM8978_ADDA_Cfg(1,0);	//开启DAC
	WM8978_Input_Cfg(0,0,0);//关闭输入通道
	WM8978_Output_Cfg(1,0);	//开启DAC输出
	return true;
}

bool HW_Muisc_Config_Check_Param(uint32_t datasize,uint32_t samplerate,void* buf0,void* buf1,uint32_t data_len){
	switch(samplerate){
		case SAI_AUDIO_FREQUENCY_11K:
		case SAI_AUDIO_FREQUENCY_22K:
		case SAI_AUDIO_FREQUENCY_44K:
			break;
		case SAI_AUDIO_FREQUENCY_8K:
		case SAI_AUDIO_FREQUENCY_16K:
		case SAI_AUDIO_FREQUENCY_32K:
		case SAI_AUDIO_FREQUENCY_48K:
		case SAI_AUDIO_FREQUENCY_96K:
		case SAI_AUDIO_FREQUENCY_192K:
			break;
		default:
			printf("采样率%d不支持\r\n",samplerate);
			return false;
	}
	if((((size_t)buf0)&3)||(((size_t)buf1)&3))
		return false;
	if(datasize!=16&&datasize!=24)
		return false;
	return true;
}

bool HW_Music_Config(uint32_t datasize,uint32_t samplerate,void* buf0,void* buf1,uint32_t data_len){
	if(datasize==16){//正常使用，无特殊要求
		WM8978_I2S_Cfg(2,0);
		if(SAI1_Config(samplerate,SAI_DATASIZE_16)==false)
			return false;
		SAI_TX_DMA_Config(DMA_MDATAALIGN_HALFWORD,DMA_PDATAALIGN_HALFWORD,buf0,buf1,data_len/2);
		return true;
	}
	else if(datasize==24){//24bit音频，需要将填充数据扩充为32bit
		WM8978_I2S_Cfg(2,2);
		if(SAI1_Config(samplerate,SAI_DATASIZE_24)==false)
			return false;
		SAI_TX_DMA_Config(DMA_MDATAALIGN_WORD,DMA_PDATAALIGN_WORD,buf0,buf1,data_len/4);
		return true;
	}
	else
		return false;
}

void HW_Music_Start(void){
	SAI_DMA_Start();
}

void HW_Music_Stop(void){
	SAI_DMA_Stop();
}

void HW_Muisc_Set_HP_Vol(uint8_t l_vol,uint8_t r_vol){
	WM8978_HPvol_Set(l_vol,r_vol);
}

void HW_Music_Set_SPK_Vol(uint8_t vol){
	WM8978_SPKvol_Set(vol);
}

