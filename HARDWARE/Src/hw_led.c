#include "hw_led.h"



void HW_LED_Init(void){
	GPIO_InitTypeDef Gpio_Initure;
	
	__HAL_RCC_GPIOH_CLK_ENABLE();

	Gpio_Initure.Mode=GPIO_MODE_OUTPUT_PP;
	Gpio_Initure.Pull=GPIO_PULLUP;
	Gpio_Initure.Speed=GPIO_SPEED_FREQ_LOW;
	Gpio_Initure.Pin=GPIO_PIN_7;
	HAL_GPIO_Init(GPIOH,&Gpio_Initure);
	
	LED0(false);
}

