#include "hw_jpeg.h"
#include "hw_mdma.h"


#define USE_MDMA_CHANNEL_IN			6
#define USE_MDMA_CHANNEL_OUT		7

static JPEG_HandleTypeDef JPEG_Handle;

bool HW_JPEG_Init(void){
	MDMA_InitTypeDef hmdma_init;
	
	__HAL_RCC_JPGDECEN_CLK_ENABLE();
	
	JPEG_Handle.Instance=JPEG;
	
	if(HAL_JPEG_Init(&JPEG_Handle)!=HAL_OK)
		return false;
	
	HAL_NVIC_SetPriority(JPEG_IRQn, JPEG_ISR_PRIO, 0);
	HAL_NVIC_EnableIRQ(JPEG_IRQn);
	
	//输入dma配置
	hmdma_init.Priority				=MDMA_PRIORITY_HIGH;
	hmdma_init.Endianness			=MDMA_LITTLE_ENDIANNESS_PRESERVE;
	hmdma_init.SourceInc			=MDMA_SRC_INC_BYTE;
	hmdma_init.DestinationInc		=MDMA_DEST_INC_DISABLE;
	hmdma_init.SourceDataSize		=MDMA_SRC_DATASIZE_BYTE;
	hmdma_init.DestDataSize       	=MDMA_DEST_DATASIZE_WORD;
	hmdma_init.DataAlignment      	=MDMA_DATAALIGN_PACKENABLE;
	hmdma_init.SourceBurst        	=MDMA_SOURCE_BURST_32BEATS;
	hmdma_init.DestBurst          	=MDMA_DEST_BURST_16BEATS;
	hmdma_init.SourceBlockAddressOffset = 0;
	hmdma_init.DestBlockAddressOffset   = 0;
	hmdma_init.Request 				=MDMA_REQUEST_JPEG_INFIFO_TH;
	hmdma_init.TransferTriggerMode	=MDMA_BUFFER_TRANSFER;
	hmdma_init.BufferTransferLength	=32;
	
	__HAL_LINKDMA(&JPEG_Handle,hdmain,*HW_MDMA_Get_Handle(USE_MDMA_CHANNEL_IN));
	
	HW_MDMA_DeInit(USE_MDMA_CHANNEL_IN);
	if(HW_MDMA_Init(USE_MDMA_CHANNEL_IN,&hmdma_init)==false)
		return false;
	
	hmdma_init.Priority        = MDMA_PRIORITY_VERY_HIGH;         /* 优先级最高 */
	hmdma_init.Endianness      = MDMA_LITTLE_ENDIANNESS_PRESERVE; /* 小端格式 */
	hmdma_init.SourceInc       = MDMA_SRC_INC_DISABLE;            /* 源数据地址禁止自增 */
	hmdma_init.DestinationInc  = MDMA_DEST_INC_BYTE;              /* 目的数据地址字节自增 */
	hmdma_init.SourceDataSize  = MDMA_SRC_DATASIZE_WORD;          /* 源地址数据宽度字 */
	hmdma_init.DestDataSize    = MDMA_DEST_DATASIZE_BYTE;         /* 目的地址数据宽度字节 */
	hmdma_init.DataAlignment   = MDMA_DATAALIGN_PACKENABLE;       /* 小端，右对齐 */  
	hmdma_init.SourceBurst     = MDMA_SOURCE_BURST_32BEATS;       /* 源数据突发传输，32次 */
	hmdma_init.DestBurst       = MDMA_DEST_BURST_32BEATS;         /* 目的数据突发传输，16次 */
	
	hmdma_init.SourceBlockAddressOffset = 0;  /* 用于block传输，buffer传输用不到 */
	hmdma_init.DestBlockAddressOffset   = 0;  /* 用于block传输，buffer传输用不到 */

	hmdma_init.Request              = MDMA_REQUEST_JPEG_OUTFIFO_TH;  /* JPEG的FIFO阀值触发中断 */
	hmdma_init.TransferTriggerMode  = MDMA_BUFFER_TRANSFER;          /* 使用MDMA的buffer传输 */ 
	hmdma_init.BufferTransferLength = 32;
	
	HW_MDMA_DeInit(USE_MDMA_CHANNEL_OUT);
	if(HW_MDMA_Init(USE_MDMA_CHANNEL_OUT,&hmdma_init)==false)
		return false;
	
	__HAL_LINKDMA(&JPEG_Handle,hdmaout,*HW_MDMA_Get_Handle(USE_MDMA_CHANNEL_OUT));
	
	return true;
}

bool HW_JPEG_Decode_DMA(uint8_t* pdata_in,uint32_t InLen, uint8_t* pdata_out,uint32_t OutLen){
	return HAL_JPEG_Decode_DMA(&JPEG_Handle,pdata_in,InLen,pdata_out,OutLen)==HAL_OK;
}

bool HW_JPEG_Get_Info(JPEG_ConfTypeDef* info){
	return HAL_JPEG_GetInfo(&JPEG_Handle,info)==HAL_OK;
}

void JPEG_IRQHandler(void){
	HAL_JPEG_IRQHandler(&JPEG_Handle);   
}

