#include "hw_sdcard.h"

#ifndef SD_WRITE_TIMEOUT
  #define SD_WRITE_TIMEOUT       100U
#endif

#ifndef SD_READ_TIMEOUT
  #define SD_READ_TIMEOUT        100U
#endif

static SD_HandleTypeDef SDCard_Handle;

static void SDCard_GPIO_Init(void){
	GPIO_InitTypeDef gpio_init_structure;
	/* Enable GPIOs clock */

    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

	      /**SDMMC1 GPIO Configuration    
    PC8     ------> SDMMC1_D0
    PC9     ------> SDMMC1_D1
    PC10     ------> SDMMC1_D2
    PC11     ------> SDMMC1_D3
    PC12     ------> SDMMC1_CK
    PD2     ------> SDMMC1_CMD 
    */
    /* Common GPIO configuration */
    gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
    gpio_init_structure.Pull      = GPIO_PULLUP;
    gpio_init_structure.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
    /* D0(PC8), D1(PC9), D2(PC10), D3(PC11), CK(PC12), CMD(PD2) */
    /* Common GPIO configuration */
    gpio_init_structure.Alternate = GPIO_AF12_SDIO1;
    /* GPIOC configuration */
    gpio_init_structure.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
    HAL_GPIO_Init(GPIOC, &gpio_init_structure);
    /* GPIOD configuration */
    gpio_init_structure.Pin = GPIO_PIN_2;
    HAL_GPIO_Init(GPIOD, &gpio_init_structure);
}

bool HW_SDCard_Init(void){
	__HAL_RCC_SDMMC1_CLK_ENABLE();
	SDCard_GPIO_Init();
	
    /* NVIC configuration for SDIO interrupts   */
    HAL_NVIC_SetPriority(SDMMC1_IRQn,SDMMC_ISR_PRIO, 0);
    HAL_NVIC_EnableIRQ(SDMMC1_IRQn);
	
	SDCard_Handle.Instance=SDMMC1;
	SDCard_Handle.Init.ClockEdge=SDMMC_CLOCK_EDGE_RISING;
	SDCard_Handle.Init.ClockPowerSave=SDMMC_CLOCK_POWER_SAVE_DISABLE;
	SDCard_Handle.Init.BusWide=SDMMC_BUS_WIDE_4B;
	SDCard_Handle.Init.HardwareFlowControl=SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
	SDCard_Handle.Init.ClockDiv=0;//内部不分频，时钟为48M
	
	if(HAL_SD_Init(&SDCard_Handle)!=HAL_OK)
		return false;
	else
		return true;
}

//读SD卡
//buf:读数据缓存区
//sector:扇区地址
//cnt:扇区个数	
//返回值:true 成功；false 失败;
bool HW_SDCard_ReadDisk(uint8_t* buf,uint32_t sector,uint32_t cnt){
	return HAL_SD_ReadBlocks(&SDCard_Handle,buf,sector,cnt,SD_READ_TIMEOUT*cnt)==HAL_OK;
}  

//写SD卡
//buf:写数据缓存区
//sector:扇区地址
//cnt:扇区个数	
//返回值:true 成功；false 失败;
bool HW_SDCard_WriteDisk(uint8_t *buf,uint32_t sector,uint32_t cnt){   
	return HAL_SD_WriteBlocks(&SDCard_Handle,buf,sector,cnt,cnt*SD_WRITE_TIMEOUT)==HAL_OK;
}

bool HW_SDCard_ReadDisk_DMA(uint8_t* buf,uint32_t sector,uint32_t cnt){
	return HAL_SD_ReadBlocks_DMA(&SDCard_Handle,buf,sector,cnt)==HAL_OK;
}

bool HW_SDCard_WriteDisk_DMA(uint8_t *buf,uint32_t sector,uint32_t cnt){
	return HAL_SD_WriteBlocks_DMA(&SDCard_Handle,buf,sector,cnt)==HAL_OK;
}

bool HW_SDCard_GetInfo(HAL_SD_CardInfoTypeDef* pinfo){
	return HAL_SD_GetCardInfo(&SDCard_Handle,pinfo)==HAL_OK;
}

bool HW_SDCard_Transe_IsOver(void){
	return HAL_SD_GetCardState(&SDCard_Handle)==HAL_SD_CARD_TRANSFER;
}

void SDMMC1_IRQHandler(void){
	HAL_SD_IRQHandler(&SDCard_Handle);
}
