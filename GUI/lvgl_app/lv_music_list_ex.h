#ifndef LV_MUSIC_LIST_EX_H__
#define LV_MUSIC_LIST_EX_H__

#include "lvgl/lvgl.h"

//音乐列表
#ifdef __cplusplus
extern "C" {
#endif
lv_obj_t* lv_music_list_ex_create(lv_obj_t* parent);
void lv_music_list_ex_link_ctrl_box(lv_obj_t* p_obj,lv_obj_t* p_box);

#ifdef __cplusplus
}
#endif

#endif
