#ifndef LV_MUSIC_SPECTRUM_EX_H__
#define LV_MUSIC_SPECTRUM_EX_H__

#include "lvgl/lvgl.h"

#ifdef __cplusplus
extern "C" {
#endif

lv_obj_t* lv_music_spectrum_ex_create(lv_obj_t* parent);
void lv_music_spectrum_ex_link_box(lv_obj_t* p_spectrum,lv_obj_t* p_box);

#ifdef __cplusplus
}
#endif

#endif
