#ifndef LV_PORT_GPU_H__
#define LV_PORT_GPU_H__

#include "lvgl/lvgl.h"

void lv_port_gpu_init(void);
void lv_port_gpu_driver_init(lv_disp_drv_t * driver);


#endif