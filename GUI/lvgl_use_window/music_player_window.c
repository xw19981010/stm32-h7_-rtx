#include "music_player_window.h"
#include "lvgl_app/lv_music_ctrl_box_ex.h"
#include "lvgl_app/lv_music_list_ex.h"
#include "lvgl_app/lv_music_spectrum_ex.h"
#include "setting_save.h"
#include "hw_rand.h"
#include "music_device.h"


static int get_gain_value(void){
	return Common_Setting.soft_volume_gain;
}

static int get_spk_value(void){
	return Common_Setting.spk_volume;
}

static int get_hp_value(void){
	return Common_Setting.hp_l_volume;
}

static void set_gain_value(int value,lv_obj_t* pheader,lv_obj_t* pendlabel){
	Music_Set_Volume_Gain(value);
	lv_label_set_text_fmt(pendlabel,"%ddB",Common_Setting.soft_volume_gain);
}

static void set_spk_value(int value,lv_obj_t* pheader,lv_obj_t* pendlabel){
	const char* pVoice;
	
	Music_Set_SPK_Vol(value);
	lv_label_set_text_fmt(pendlabel,"%d",Common_Setting.spk_volume);
	if(value<20){
		pVoice=LV_SYMBOL_MUTE;
	}
	else if(value<40){
		pVoice=LV_SYMBOL_VOLUME_MID;
	}
	else{
		pVoice=LV_SYMBOL_VOLUME_MAX;
	}
	lv_label_set_text_fmt(pheader,"%s喇叭:",pVoice);
}

static void set_hp_value(int value,lv_obj_t* pheader,lv_obj_t* pendlabel){
	const char* pVoice;
	Music_Set_HP_Vol(value,value);
	lv_label_set_text_fmt(pendlabel,"%d",Common_Setting.hp_l_volume);
	if(value<20){
		pVoice=LV_SYMBOL_MUTE;
	}
	else if(value<40){
		pVoice=LV_SYMBOL_VOLUME_MID;
	}
	else{
		pVoice=LV_SYMBOL_VOLUME_MAX;
	}
	lv_label_set_text_fmt(pheader,"%s耳机:",pVoice);
}

static void music_slider_change_common(lv_event_t* e){
	lv_obj_t* pslider=lv_event_get_target(e);
	lv_obj_t* pendlabel=lv_event_get_user_data(e);
	lv_obj_t* pheader=lv_obj_get_user_data(pendlabel);
	void(*set_value)(int,lv_obj_t*,lv_obj_t*)=lv_obj_get_user_data(pslider);
	set_value(lv_slider_get_value(pslider),pheader,pendlabel);
}

typedef struct{
	lv_obj_t* parent;
	lv_obj_t* align_to_obj;
	lv_align_t align_param;
	lv_coord_t x_off;
	lv_coord_t y_off;
	lv_coord_t width;
	lv_coord_t height;
	int slider_max;
	int slider_min;
	const char* header;
	int(*get_value)(void);
	void(*set_value)(int,lv_obj_t*,lv_obj_t*);
}Music_Slider_Param;

static void music_slider_create(Music_Slider_Param* p){
	lv_obj_t* pheader_label;
	lv_obj_t* pPrecent;
	lv_obj_t* pSlider;
	
	pheader_label=lv_label_create(p->parent);
	pSlider=lv_slider_create(p->parent);
	pPrecent=lv_label_create(p->parent);
	
	lv_obj_set_style_text_font(pheader_label,&lv_font_hz16_aw,LV_STATE_DEFAULT);
	lv_label_set_text(pheader_label,p->header);
	lv_obj_align_to(pheader_label,p->align_to_obj,p->align_param,p->x_off,p->y_off);
	lv_obj_set_height(pheader_label,p->height);
	
	lv_obj_set_height(pSlider,p->height);
	lv_obj_add_event_cb(pSlider,music_slider_change_common,LV_EVENT_VALUE_CHANGED,pPrecent);
	lv_obj_set_width(pSlider,p->width-lv_obj_get_width(pheader_label));
	lv_obj_set_height(pSlider,5);
	lv_obj_align_to(pSlider,pheader_label,LV_ALIGN_OUT_RIGHT_MID,10,0);
	lv_slider_set_range(pSlider,p->slider_min,p->slider_max);
	lv_obj_set_user_data(pSlider,p->set_value);
	lv_slider_set_value(pSlider,p->get_value(),LV_ANIM_OFF);
	
	lv_obj_set_style_text_font(pPrecent,&lv_font_hz16_aw,LV_STATE_DEFAULT);
	lv_obj_set_height(pPrecent,p->height);
	lv_obj_align_to(pPrecent,pSlider,LV_ALIGN_OUT_RIGHT_MID,10,0);
	lv_obj_set_user_data(pPrecent,pheader_label);
	
	p->set_value(p->get_value(),pheader_label,pPrecent);
}

typedef struct{
	lv_obj_t* parent;
	lv_obj_t* align_obj;
	lv_align_t align_type;
	int xoff;
	int yoff;
	const char* show_txt;
	lv_event_cb_t call;
	void* use_data;
}Small_Btn_Param;

typedef struct{
	lv_obj_t* use_btn;
	lv_obj_t* use_txt;
}Small_Create_Struct;

static Small_Create_Struct small_button_create(Small_Btn_Param* p){
	Small_Create_Struct buf_struct;
	
	buf_struct.use_btn=lv_btn_create(p->parent);
	lv_obj_add_event_cb(buf_struct.use_btn,p->call,LV_EVENT_CLICKED,p->use_data);
	
	lv_obj_set_style_text_font(buf_struct.use_btn,&lv_font_hz16_aw,LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(buf_struct.use_btn,lv_color_white(),LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(buf_struct.use_btn,lv_theme_get_color_primary(p->parent),LV_STATE_PRESSED);
	lv_obj_set_style_text_color(buf_struct.use_btn,lv_theme_get_color_primary(p->parent),LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(buf_struct.use_btn,lv_color_white(),LV_STATE_PRESSED);
	lv_obj_set_size(buf_struct.use_btn,LV_DPX(52),LV_DPX(52));
	lv_obj_set_style_radius(buf_struct.use_btn,LV_RADIUS_CIRCLE,LV_STATE_DEFAULT);
	
	
	buf_struct.use_txt=lv_label_create(buf_struct.use_btn);
	lv_label_set_text(buf_struct.use_txt,p->show_txt);
	lv_obj_set_style_align(buf_struct.use_txt,LV_ALIGN_CENTER,LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(buf_struct.use_txt,LV_TEXT_ALIGN_CENTER,LV_STATE_DEFAULT);
	
	lv_obj_align_to(buf_struct.use_btn,p->align_obj,p->align_type,p->xoff,p->yoff);
	return buf_struct;
}

static void refresh_call_func(lv_event_t* e){
	lv_obj_t* p_box=lv_event_get_user_data(e);
	lv_music_ctrl_box_ex_fresh(p_box);
}

static void fill_decoder_surport(Music_Decoder* p,void* token){
	File_Filter* pfilter=token;
	for(const char*const* support=p->surport_file;*support!=NULL;support++){
		size_t print_len=snprintf(NULL,0,"*.%s",*support);
		char print_buf[print_len+1];
		snprintf(print_buf,print_len+1,"*.%s",*support);
		Dir_File_Filter_Add(pfilter,print_buf);
	}
}

static uint32_t music_play_rand_order(uint32_t priv,uint32_t max_index){
	return RNG_Get_RandomNum()%max_index;
}

static uint32_t music_play_positive_order(uint32_t priv,uint32_t max_index){
	return (priv+1)%max_index;
}

static uint32_t music_play_loop(uint32_t priv,uint32_t max_index){
	return priv;
}

typedef struct{
	const char* txt;
	uint32_t (* order_func)(uint32_t priv,uint32_t max_index);
}Order_Pair;
static const Order_Pair order_func_table[]={
	{
		.order_func=music_play_positive_order,
		.txt=LV_SYMBOL_LIST,
	},
	{
		.order_func=music_play_loop,
		.txt=LV_SYMBOL_LOOP,
	},
	{
		.order_func=music_play_rand_order,
		.txt=LV_SYMBOL_SHUFFLE
	},
};

typedef struct{
	lv_obj_t* p_txt;
	lv_obj_t* p_box;
	uint8_t now_order;
}Order_Change_Param;

static void order_change_call_func(lv_event_t* e){
	Order_Change_Param* p_param=lv_event_get_user_data(e);
	const uint32_t order_func_num=sizeof(order_func_table)/sizeof(order_func_table[0]);
	p_param->now_order=(p_param->now_order+1)%order_func_num;
	lv_music_ctrl_box_ex_set_order(p_param->p_box,order_func_table[p_param->now_order].order_func);
	lv_label_set_text(p_param->p_txt,order_func_table[p_param->now_order].txt);
}

void music_player_create(lv_obj_t* parent){
	lv_obj_t* p_box,*plist,*pchart;
	int music_slider_x,music_slider_y;
	const int slider_space=20;
	const int slider_heignt=25;
	const int slider_width=180;
	Music_Slider_Param buf_param;
	Small_Btn_Param small_param;
	File_Filter buf_filter={.pattern=NULL,.pattern_num=0};
	
	p_box=lv_music_ctrl_box_ex_create(parent);
	
	Music_Decoder_Walk(fill_decoder_surport,&buf_filter);
	lv_music_ctrl_box_ex_set_path(p_box,"0:/music",&buf_filter,DS_NOCASE);
	Dir_File_Filter_Destroy(&buf_filter);
	
	lv_music_ctrl_box_ex_set_station(p_box,parent);
	plist=lv_music_list_ex_create(parent);
	lv_music_list_ex_link_ctrl_box(plist,p_box);
	lv_obj_align_to(plist,p_box,LV_ALIGN_OUT_BOTTOM_LEFT,0,0);
	lv_obj_set_size(plist,LV_PCT(50),lv_obj_get_content_height(parent)-lv_obj_get_x(p_box)-lv_obj_get_height(p_box));
	
	
	buf_param.align_param=LV_ALIGN_OUT_RIGHT_TOP;
	buf_param.align_to_obj=p_box;
	buf_param.height=slider_heignt;
	buf_param.parent=parent;
	buf_param.width=slider_width;
	buf_param.x_off=5;
	
	
	buf_param.y_off=0;
	buf_param.slider_max=MUSIC_GAIN_MAX;
	buf_param.slider_min=MUSIC_GAIN_MIN;
	buf_param.header="增益:  ";
	buf_param.set_value=set_gain_value;
	buf_param.get_value=get_gain_value;
	music_slider_create(&buf_param);
	
	buf_param.y_off+=buf_param.height+slider_space;
	buf_param.slider_max=63;
	buf_param.slider_min=0;
	buf_param.header=LV_SYMBOL_MUTE "耳机:  ";
	buf_param.set_value=set_hp_value;
	buf_param.get_value=get_hp_value;
	
	music_slider_create(&buf_param);
	
	buf_param.y_off+=buf_param.height+slider_space;
	buf_param.slider_max=63;
	buf_param.slider_min=0;
	buf_param.header=LV_SYMBOL_MUTE "喇叭:  ";
	buf_param.set_value=set_spk_value;
	buf_param.get_value=get_spk_value;
	music_slider_create(&buf_param);
	
	pchart=lv_music_spectrum_ex_create(parent);
	lv_music_spectrum_ex_link_box(pchart,p_box);
	lv_obj_set_width(pchart,LV_PCT(40));
	lv_obj_set_height(pchart,lv_obj_get_height(plist));
	lv_obj_align_to(pchart,plist,LV_ALIGN_OUT_RIGHT_TOP,10,0);
	
	
	Small_Create_Struct fresh_handle,order_change;
	small_param.align_obj=pchart;
	small_param.align_type=LV_ALIGN_OUT_RIGHT_TOP;
	small_param.call=refresh_call_func;
	small_param.parent=parent;
	small_param.show_txt=LV_SYMBOL_REFRESH;
	small_param.use_data=p_box;
	small_param.xoff=10;
	small_param.yoff=0;
	
	fresh_handle=small_button_create(&small_param);
	
	static Order_Change_Param order_change_param;
	order_change_param.now_order=0;
	order_change_param.p_box=p_box;
	small_param.align_obj=fresh_handle.use_btn;
	small_param.align_type=LV_ALIGN_OUT_BOTTOM_MID;
	small_param.call=order_change_call_func;
	small_param.parent=parent;
	small_param.show_txt=order_func_table[0].txt;
	small_param.use_data=&order_change_param;
	small_param.xoff=0;
	small_param.yoff=10;
	order_change=small_button_create(&small_param);
	order_change_param.p_txt=order_change.use_txt;
	lv_music_ctrl_box_ex_set_order(p_box,order_func_table[0].order_func);
	
}


