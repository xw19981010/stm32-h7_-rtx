/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/
#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "sdcard_rtos.h"

/* Definitions of physical drive number for each drive */
#define SD_CARD		0	/* Example: Map Ramdisk to physical drive 0 */

#define SD_BLOCKSIZE     BLOCKSIZE 

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	switch(pdrv){
		case SD_CARD:
			switch(SDCard_Get_State()){
				case FATFS_WR_NO_USB:
					return RES_OK;
				case FATFS_R_USB_R:
					return RES_WRPRT;
				case NO_FATFS_USB_WR:
					return RES_NOTRDY;
				default:
					return RES_PARERR;
			}
			break;
		default:
			return RES_PARERR;		
	}
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{	    
	switch(pdrv){
		case SD_CARD:		//SD卡
			while(SDCard_Init()==false){
				printf("SD卡初始化错误！\r\n");
				delay_ms(1000);
			}
  			return RES_OK;
		default:
			return  RES_PARERR;
	}	
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
    if (!count)
		return RES_PARERR;//count不能等于0，否则返回参数错误		 	 
	switch(pdrv){
		case SD_CARD://SD卡
			return portFATFS_SDCard_Read(buff,sector,count)?RES_OK:RES_ERROR;	 
		default:
			return RES_PARERR;
	}   
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{ 
    if (!count)
		return RES_PARERR;//count不能等于0，否则返回参数错误		 	 
	switch(pdrv){
		case SD_CARD://SD卡
			return portFATFS_SDCard_Write((uint8_t*)buff,sector,count)?RES_OK:RES_ERROR;
		default:
			return RES_PARERR;
	}	
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	HAL_SD_CardInfoTypeDef pinfo;
	DRESULT res;
	if(pdrv==SD_CARD)//SD卡
	{
	    switch(cmd)
	    {
		    case CTRL_SYNC:
				res = RES_OK; 
		        break;	 
		    case GET_SECTOR_SIZE:
				HW_SDCard_GetInfo(&pinfo);
				*(DWORD*)buff = pinfo.LogBlockSize; 
		        res = RES_OK;
		        break;	 
		    case GET_BLOCK_SIZE:
				HW_SDCard_GetInfo(&pinfo);
				*(WORD*)buff = pinfo.LogBlockSize/SD_BLOCKSIZE;
		        res = RES_OK;
		        break;	 
		    case GET_SECTOR_COUNT:
				HW_SDCard_GetInfo(&pinfo);
		        *(DWORD*)buff = pinfo.LogBlockNbr;
		        res = RES_OK;
		        break;
		    default:
		        res = RES_PARERR;
		        break;
	    }
	}else res=RES_ERROR;//其他的不支持
    return res;
}